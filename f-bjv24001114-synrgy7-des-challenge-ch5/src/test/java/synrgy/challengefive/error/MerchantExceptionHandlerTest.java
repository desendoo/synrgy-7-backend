package synrgy.challengefive.error;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import synrgy.challengefive.error.exception.InternalServerErrorException;
import synrgy.challengefive.error.handler.MerchantExceptionHandler;
import synrgy.challengefive.model.standard.StandardResponseModel;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class MerchantExceptionHandlerTest {

    @Mock
    private MerchantExceptionHandler merchantExceptionHandler;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Handle generic exception")
    void handleGenericException() {
        Exception exception = new Exception("Test exception");
        when(merchantExceptionHandler.handleException(exception)).thenCallRealMethod();

        ResponseEntity<StandardResponseModel> response = merchantExceptionHandler.handleException(exception);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals("Test exception", response.getBody().getErrors().stream().filter(
                e -> e.containsValue("Test exception")).findFirst().get().get(exception.getClass().getSimpleName()));
        assertTrue(response.getBody().getErrors().stream().filter(
                e -> e.containsKey(exception.getClass().getSimpleName())).findFirst().get().get(exception.getClass().getSimpleName()).toString().contains("Test exception"));
    }

    @Test
    @DisplayName("Handle InternalServerErrorException")
    void handleInternalServerErrorException() {
        InternalServerErrorException exception = new InternalServerErrorException("Test exception");
        when(merchantExceptionHandler.handleInternalServerErrorException(exception)).thenCallRealMethod();

        ResponseEntity<StandardResponseModel> response = merchantExceptionHandler.handleInternalServerErrorException(exception);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertTrue(response.getBody().getErrors().stream().filter(
                e -> e.containsKey(exception.getClass().getSimpleName())).findFirst().get().get(exception.getClass().getSimpleName()).toString().contains("Test exception"));
    }

}
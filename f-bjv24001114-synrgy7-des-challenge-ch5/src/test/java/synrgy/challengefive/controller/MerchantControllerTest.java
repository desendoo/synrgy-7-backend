package synrgy.challengefive.controller;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.*;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import synrgy.challengefive.utils.CustomMapper;
import synrgy.challengefive.model.entity.Merchant;
import synrgy.challengefive.model.request.merchant.MerchantCreateRequestModel;
import synrgy.challengefive.model.standard.StandardResponseModel;
import synrgy.challengefive.service.MerchantService;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

// @TestPropertySource("/application-test.properties")
@WebMvcTest(MerchantController.class)
public class MerchantControllerTest {

    private static List<Merchant> merchants;

    // @InjectMocks
    // private MerchantController merchantController;
    private static Pageable pageable;
    @MockBean
    private MerchantService<Merchant> merchantService;
    @Autowired
    private MockMvc mockMvc;

    @BeforeAll
    static void init() {
    }

    @BeforeEach
    void setUp() {
        // MockitoAnnotations.openMocks(this);
        merchants = setupMerchants();
        pageable = PageRequest.of(0, 5, Sort.by("name"));
    }

    List<Merchant> setupMerchants() {
        return List.of(
                new Merchant(UUID.randomUUID(), "Merchant 1", "Location 1", true),
                new Merchant(UUID.randomUUID(), "Merchant 2", "Location 2", false),
                new Merchant(UUID.randomUUID(), "Merchant 3", "Location 3", true),
                new Merchant(UUID.randomUUID(), "Merchant 4", "Location 4", false),
                new Merchant(UUID.randomUUID(), "Merchant 5", "Location 5", true)
        );
    }

    Page<Merchant> setupMerchantsPaged(List<Merchant> merchantList, Pageable pageable) {
        return new PageImpl<>(merchantList, pageable, merchantList.size());
    }

    @Test
    void listMerchantsSuccessfully() throws Exception {
        Boolean open = true;
        merchants = merchants.stream().filter(merchant -> merchant.getOpen() == open).toList();
        Page<Merchant> merchantsPaged = setupMerchantsPaged(merchants, pageable);
        StandardResponseModel expectedResponse = new StandardResponseModel();
        expectedResponse.putData("merchants",
                merchantsPaged.getContent().stream().filter(Merchant::isOpen).map(CustomMapper::toMerchantDTO).toList());
        expectedResponse.setPageableData(merchantsPaged);
        expectedResponse.success();

        // int expectedMerchantsLength = expectedResponse.toJsonObject().getJSONObject("data").getJSONArray("merchants").length();
        when(merchantService.list(pageable, open)).thenReturn(expectedResponse);

        mockMvc.perform(get("/v1/merchants")
                .param("open", open.toString())
                .param("page", pageable.getPageNumber() + "")
                .param("size", pageable.getPageSize() + ""))
                // .param("sort", "name,asc"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.statusCode").value(200))
                .andExpect(jsonPath("$.data.merchants").isArray())
                .andExpect(jsonPath("$.data.merchants").exists())
                .andExpect(jsonPath("$.data.merchants").value(hasSize(merchants.size())))
                .andExpect(jsonPath("$.errors").isEmpty())
                .andDo(print());

        verify(merchantService, times(1)).list(pageable, true);
        verifyNoMoreInteractions(merchantService);
    }

    @Test
    void saveMerchantSuccessfully() throws Exception {
        MerchantCreateRequestModel request = new MerchantCreateRequestModel();
        request.setName("Merchant 6");
        request.setLocation("Location 6");
        request.setOpen(true);
        StandardResponseModel expectedResponse = new StandardResponseModel();
        expectedResponse.putData("merchant", CustomMapper.toMerchantDTO(new Merchant(UUID.randomUUID(), request.getName(), request.getLocation(), request.getOpen())));
        expectedResponse.success();

        when(merchantService.save(request)).thenReturn(expectedResponse);

        mockMvc.perform(post("/v1/merchants")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"name\": \"Merchant 6\",\n" +
                        "  \"location\": \"Location 6\",\n" +
                        "  \"open\": true\n" +
                        "}"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.statusCode").value(200))
                .andExpect(jsonPath("$.data.merchant").exists())
                .andExpect(jsonPath("$.data.merchant.name").value(request.getName()))
                .andExpect(jsonPath("$.errors").isEmpty())
                .andDo(print());

        verify(merchantService, times(1)).save(request);
        verifyNoMoreInteractions(merchantService);
    }

    /*
    @Test
    @DisplayName("Get merchants returns expected response")
    void getMerchantsReturnsExpectedResponse() {
        StandardResponseModel expectedResponse = new StandardResponseModel();
        when(merchantService.list(anyBoolean(), any(Pageable.class))).thenReturn(expectedResponse);

        ResponseEntity<StandardResponseModel> response = merchantController.getMerchants(true, PageRequest.of(0, 5));

        assertEquals(expectedResponse, response.getBody());
    }

    @Test
    @DisplayName("Get merchant by id returns expected response")
    void getMerchantByIdReturnsExpectedResponse() {
        UUID id = UUID.randomUUID();
        StandardResponseModel expectedResponse = new StandardResponseModel();
        when(merchantService.get(id)).thenReturn(expectedResponse);

        ResponseEntity<StandardResponseModel> response = merchantController.getMerchant(id);

        assertEquals(expectedResponse, response.getBody());
    }

    @Test
    @DisplayName("Save merchant returns expected response")
    void saveMerchantReturnsExpectedResponse() {
        MerchantCreateModel request = new MerchantCreateModel();
        StandardResponseModel expectedResponse = new StandardResponseModel();
        when(merchantService.save(request)).thenReturn(expectedResponse);

        ResponseEntity<StandardResponseModel> response = merchantController.saveMerchant(request);

        assertEquals(expectedResponse, response.getBody());
    }

    @Test
    @DisplayName("Edit merchant returns expected response")
    void editMerchantReturnsExpectedResponse() {
        UUID id = UUID.randomUUID();
        MerchantEditModel request = new MerchantEditModel();
        StandardResponseModel expectedResponse = new StandardResponseModel();
        when(merchantService.edit(id, request)).thenReturn(expectedResponse);

        ResponseEntity<StandardResponseModel> response = merchantController.editMerchant(id, request);

        assertEquals(expectedResponse, response.getBody());
    }

    @Test
    @DisplayName("Delete merchant returns expected response")
    void deleteMerchantReturnsExpectedResponse() {
        UUID id = UUID.randomUUID();
        StandardResponseModel expectedResponse = new StandardResponseModel();
        when(merchantService.delete(id)).thenReturn(expectedResponse);

        ResponseEntity<StandardResponseModel> response = merchantController.deleteMerchant(id);

        assertEquals(expectedResponse, response.getBody());
    }*/

}
package synrgy.challengefive.model.entity;

import synrgy.challengefive.model.entity.Order;
import synrgy.challengefive.model.entity.User;

import java.util.Date;
import java.util.UUID;

public class OrderTest {

    public static Order setupEntity(User user) {
        Order order = new Order();
        order.setOrderTime(new Date());
        order.setDestinationAddress("Example Destination Address");
        order.setCompleted(true);
        order.setIdUser(user);

        return order;
    }

}
package synrgy.challengefive.model.entity;

import org.h2.tools.Server;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import synrgy.challengefive.model.entity.Merchant;
import synrgy.challengefive.repository.MerchantRepository;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

// @DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@SpringBootTest
@TestPropertySource("/application-test.properties")
public class MerchantTest {

    private static Merchant sut;
    // private static Server h2;
    @Autowired
    private MerchantRepository merchantRepository;

    // @BeforeAll
    // public static void initTest() throws SQLException {
    //     h2 = Server.createWebServer("-web", "-webAllowOthers", "-webPort", "8080")
    //             .start();
    // }
    //
    // @AfterAll
    // static void terminate() {
    //     h2.stop();
    // }

    public static Merchant setupEntity() {
        Merchant merchant = new Merchant();
        merchant.setId(UUID.randomUUID());
        merchant.setName("Merchant");
        merchant.setLocation("Yogyakarta");
        merchant.setOpen(true);

        return merchant;
    }

    @BeforeEach
    void setUp() {
        resetRepository();
        sut = setupEntity();
    }

    private void resetRepository() {
        merchantRepository.deleteAll();
    }

    @Transactional
    void update() throws Exception {
        try {
            List<Merchant> all = merchantRepository.findAll();
            Merchant mostRecentCreatedMerchant =
                    merchantRepository.findFirstByOrderByCreatedDateDesc();
            if (mostRecentCreatedMerchant != null) {
                mostRecentCreatedMerchant.setName("MERCHANT NAME UPDATED");
                mostRecentCreatedMerchant.setLocation("LOCATION UPDATED");
            }

            List<Merchant> all1 = merchantRepository.findAll();
            Merchant result = merchantRepository.save(mostRecentCreatedMerchant);
            List<Merchant> all2 = merchantRepository.findAll();
            // saving a null entity and expect triggering a transactional rollback
            Merchant result2 = merchantRepository.save(null);
            List<Merchant> all3 = merchantRepository.findAll();
            System.out.println();
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    // @Disabled
    @Test
    @DisplayName("Update merchant fail expected")
    void updateMerchantsFailExpected() {
        List<Merchant> all = merchantRepository.findAll();
        try {
            update();
        } catch (Exception e) {
            // throw new RuntimeException(e);
            System.out.println("ERROR");
        }
        // assertThrows(Exception.class, this::save);
        List<Merchant> all1 = merchantRepository.findAll();
        try {
            update();
        } catch (Exception e) {
            // throw new RuntimeException(e);
            System.out.println("ERROR");
        }
        List<Merchant> all2 = merchantRepository.findAll();
        System.out.println();
    }

    @Transactional
        // @Transactional(rollbackFor = Exception.class)
    void save() throws Exception {
        try {
            Merchant result = merchantRepository.save(sut);
            List<Merchant> all = merchantRepository.findAll();
            // saving a null entity and expect triggering a transactional rollback
            Merchant result2 = merchantRepository.save(null);
            List<Merchant> all2 = merchantRepository.findAll();
            System.out.println();
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new Exception(e);
        }
    }

    @Disabled
    @Test
    @DisplayName("Save merchant fail expected")
    void saveMerchantsFailExpected() {
        List<Merchant> all = merchantRepository.findAll();
        try {
            save();
        } catch (Exception e) {
            // throw new RuntimeException(e);
            System.out.println("ERROR");
        }
        // assertThrows(Exception.class, this::save);
        List<Merchant> all1 = merchantRepository.findAll();
        try {
            save();
        } catch (Exception e) {
            // throw new RuntimeException(e);
            System.out.println("ERROR");
        }
        List<Merchant> all2 = merchantRepository.findAll();
        System.out.println();
    }

    @Test
    @DisplayName("Save merchant successfully")
    void saveMerchantSuccessfully() {
        Merchant result = merchantRepository.save(sut);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(sut.getName(), result.getName());
        assertEquals(sut.getLocation(), result.getLocation());
        assertEquals(sut.isOpen(), result.isOpen());
    }

    @Test
    @DisplayName("Edit merchant successfully")
    void editMerchantOpenSuccessfully() {
        Merchant result = merchantRepository.save(sut);
        assertTrue(result.isOpen());

        result.setOpen(false);
        Merchant updated = merchantRepository.save(result);

        assertFalse(updated.isOpen());
    }

    @Test
    @DisplayName("List merchants successfully")
    void listMerchantsSuccessfully() {
        merchantRepository.save(sut);
        merchantRepository.save(setupEntity());
        merchantRepository.save(setupEntity());
        merchantRepository.save(setupEntity());

        List<Merchant> merchants = merchantRepository.findAll();

        assertFalse(merchants.isEmpty());
        assertEquals(4, merchants.size());
    }

    @Test
    @DisplayName("Edit merchant open state successfully")
    void editOpenMerchantSuccessfully() {
        Merchant result = merchantRepository.save(sut);
        assertTrue(result.isOpen());

        result.setOpen(false);
        Merchant updated = merchantRepository.save(result);

        assertFalse(updated.isOpen());
    }

}
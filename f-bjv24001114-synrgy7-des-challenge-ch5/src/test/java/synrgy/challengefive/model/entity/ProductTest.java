package synrgy.challengefive.model.entity;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import synrgy.challengefive.repository.ProductRepository;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@TestPropertySource("/application-test.properties")
public class ProductTest {

    @Autowired
    private ProductRepository productRepository;

    private static Merchant merchant;
    private static Product sut;

    @BeforeAll
    static void init() {
        merchant = MerchantTest.setupEntity();
    }

    @BeforeEach
    void setUp() {
        resetRepository();
        merchant = MerchantTest.setupEntity();
        sut = setupEntity(merchant);
    }

    private void resetRepository() {
        productRepository.deleteAll();
    }

    public static Product setupEntity(Merchant merchant) {
        Product product = new Product();
        product.setId(UUID.randomUUID());
        product.setName("Super Product");
        product.setPrice(10000.0);
        product.setIdMerchant(merchant);

        return product;
    }

    @Test
    @DisplayName("Save product successfully")
    void saveProductSuccessfully() {
        Product result = productRepository.save(sut);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(sut.getName(), result.getName());
        assertEquals(sut.getPrice(), result.getPrice());
    }

    @Test
    @DisplayName("List product successfully")
    void listProductSuccessfully() {
        Product result = productRepository.save(sut);
        List<Product> products = productRepository.findAll();

        assertNotNull(products);
        assertEquals(1, products.size());
    }

    @Test
    @DisplayName("Edit product successfully")
    void editProductSuccessfully() {
        String editedProductName = "Super Product 2";
        assertEquals(sut.getName(), "Super Product");

        Product result = productRepository.save(sut);
        result.setName(editedProductName);
        result.setPrice(20000.0);
        result.setIdMerchant(merchant);

        Product updated = productRepository.save(result);

        assertNotNull(updated);
        assertEquals(editedProductName, updated.getName());
        assertEquals(result.getName(), updated.getName());
        assertEquals(result.getPrice(), updated.getPrice());
    }

    @Test
    @DisplayName("Delete product successfully")
    void deleteProductSuccessfully() {
        Product result = productRepository.save(sut);
        productRepository.delete(result);

        List<Product> products = productRepository.findAll();

        assertNotNull(products);
        assertEquals(0, products.size());
    }

}
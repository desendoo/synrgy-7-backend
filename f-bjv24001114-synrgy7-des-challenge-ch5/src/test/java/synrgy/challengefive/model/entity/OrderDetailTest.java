package synrgy.challengefive.model.entity;

import synrgy.challengefive.model.entity.Order;
import synrgy.challengefive.model.entity.OrderDetail;
import synrgy.challengefive.model.entity.Product;

import java.util.UUID;

public class OrderDetailTest {

    public static OrderDetail setupEntity(Order order, Product product) {
        int quantity = 5;
        double totalPrice = product.getPrice() * quantity;

        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setId(UUID.randomUUID());
        orderDetail.setQuantity(quantity);
        orderDetail.setTotalPrice(totalPrice);
        orderDetail.setIdOrder(order);
        orderDetail.setIdProduct(product);

        return orderDetail;
    }

}

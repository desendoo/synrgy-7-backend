package synrgy.challengefive.service.impl;

import jakarta.persistence.criteria.Predicate;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import synrgy.challengefive.error.exception.ExtendedEntityNotFoundException;
import synrgy.challengefive.error.exception.InternalServerErrorException;
import synrgy.challengefive.model.entity.Merchant;
import synrgy.challengefive.model.entity.MerchantTest;
import synrgy.challengefive.model.request.merchant.MerchantCreateRequestModel;
import synrgy.challengefive.model.request.merchant.MerchantEditRequestModel;
import synrgy.challengefive.model.standard.StandardResponseModel;
import synrgy.challengefive.repository.MerchantRepository;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class MerchantServiceImplTest {

    @InjectMocks
    private MerchantServiceImpl merchantService;
    @Mock
    private MerchantRepository merchantRepository;
    @Mock
    private Specification<Merchant> specification;
    @Mock
    private Pageable pageable;
    private Boolean openParameterRequest;
    private Merchant merchant;

    private Specification<Merchant> buildSpecification() {
        return ((root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            predicates.add(criteriaBuilder.equal(root.get("open"), openParameterRequest));

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        });
    }

    public static MerchantCreateRequestModel setupMerchantCreateModel(Merchant merchant) {
        MerchantCreateRequestModel merchantCreateRequestModel = new MerchantCreateRequestModel();
        merchantCreateRequestModel.setName(merchant.getName());
        merchantCreateRequestModel.setLocation(merchant.getLocation());
        merchantCreateRequestModel.setOpen(merchant.getOpen());

        return merchantCreateRequestModel;
    }

    public static MerchantEditRequestModel setupMerchantEditModel(String name, String location, Boolean open) {
        MerchantEditRequestModel merchantCreateModel = new MerchantEditRequestModel();
        merchantCreateModel.setName(name);
        merchantCreateModel.setLocation(location);
        merchantCreateModel.setOpen(open);

        return merchantCreateModel;
    }

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        openParameterRequest = true;
        merchant = MerchantTest.setupEntity();
    }

    @Test
    @DisplayName("List merchants successfully")
    void listMerchantsSuccessfully() {
        Page<Merchant> merchants = new PageImpl<>(Collections.singletonList(merchant));

        // Mock the toPredicate method
        when(specification.toPredicate(any(), any(), any())).thenReturn(mock(Predicate.class));
        when(merchantRepository.findAll(any(Specification.class), any(Pageable.class))).thenReturn(merchants);

        StandardResponseModel response = merchantService.list(pageable, null);
        JSONObject data = response.toJsonObject().getJSONObject("data");
        JSONArray merchantsArray = data.getJSONArray("merchants");

        assertNotNull(response);
        assertEquals(200, response.getStatusCode());
        assertFalse(merchantsArray.isEmpty());
    }

    @Test
    @DisplayName("List merchants throws InternalServerErrorException")
    void listMerchantsThrowsInternalServerErrorException() {
        when(merchantRepository.findAll()).thenThrow(new RuntimeException());

        assertThrows(InternalServerErrorException.class, () -> merchantService.list(pageable, true));
    }

    @Test
    @DisplayName("Get merchant successfully")
    void getMerchantSuccessfully() {
        when(merchantRepository.findById(merchant.getId())).thenReturn(Optional.of(merchant));

        StandardResponseModel response = merchantService.get(merchant.getId());
        JSONObject data = response.toJsonObject().getJSONObject("data");
        JSONObject merchantData = data.getJSONObject("merchant");

        assertNotNull(response);
        assertEquals(200, response.getStatusCode());
        assertEquals(merchant.getId().toString(), merchantData.getString("id"));
    }

    @Test
    @DisplayName("Get merchant throws ExtendedEntityNotFoundException")
    void getMerchantThrowsExtendedEntityNotFoundException() {
        when(merchantRepository.findById(any(UUID.class))).thenReturn(Optional.empty());

        assertThrows(ExtendedEntityNotFoundException.class, () -> merchantService.get(UUID.randomUUID()));
    }

    @Test
    @DisplayName("Save merchant successfully")
    void saveMerchantSuccessfully() {
        MerchantCreateRequestModel requestModel = setupMerchantCreateModel(merchant);

        when(merchantRepository.save(any(Merchant.class))).thenReturn(merchant);
        StandardResponseModel response = merchantService.save(requestModel);

        assertNotNull(response);
        assertEquals(201, response.getStatusCode());
    }

    @Test
    @DisplayName("Save merchant triggers @Transactional rollback")
    void saveMerchantTriggersTransactionalRollback() {
        when(merchantRepository.save(any(Merchant.class))).thenThrow(new RuntimeException());

        assertThrows(InternalServerErrorException.class, () -> merchantService.save(new MerchantCreateRequestModel()));
    }

    @Test
    @DisplayName("Save merchant throws InternalServerErrorException")
    void saveMerchantThrowsInternalServerErrorException() {
        MerchantCreateRequestModel requestModel = setupMerchantCreateModel(merchant);

        when(merchantRepository.save(any(Merchant.class))).thenThrow(new RuntimeException());

        assertThrows(InternalServerErrorException.class, () -> merchantService.save(requestModel));
    }

    @Test
    @DisplayName("Edit merchant successfully")
    void editMerchantSuccessfully() {
        MerchantEditRequestModel requestModel = setupMerchantEditModel(merchant.getName(), merchant.getLocation(),
                false);
        assertTrue(merchant.getOpen());

        when(merchantRepository.findById(merchant.getId())).thenReturn(Optional.of(merchant));
        when(merchantRepository.save(any(Merchant.class))).thenReturn(merchant);

        StandardResponseModel response = merchantService.edit(merchant.getId(), requestModel);
        Boolean merchantOpenState =
                response.toJsonObject().getJSONObject("data").getJSONObject("merchant").getBoolean("open");

        assertNotNull(response);
        assertEquals(200, response.getStatusCode());
        assertFalse(merchantOpenState);
    }

    @Test
    @DisplayName("Edit merchant throws ExtendedEntityNotFoundException")
    void editMerchantThrowsExtendedEntityNotFoundException() {
        MerchantEditRequestModel requestModel = setupMerchantEditModel(merchant.getName(), merchant.getLocation(),
                false);

        when(merchantRepository.findById(merchant.getId())).thenReturn(Optional.empty());

        assertThrows(ExtendedEntityNotFoundException.class, () -> merchantService.edit(merchant.getId(), requestModel));
    }

    @Test
    @DisplayName("Edit merchant throws InternalServerErrorException")
    void editMerchantThrowsInternalServerErrorException() {
        MerchantEditRequestModel requestModel = setupMerchantEditModel(merchant.getName(), merchant.getLocation(),
                false);

        when(merchantRepository.findById(merchant.getId())).thenReturn(Optional.of(merchant));
        when(merchantRepository.save(any(Merchant.class))).thenThrow(new RuntimeException());

        assertThrows(InternalServerErrorException.class, () -> merchantService.edit(merchant.getId(), requestModel));
    }

    @Test
    @DisplayName("Edit merchant open state successfully")
    void editMerchantOpenStateSuccessfully() {
        MerchantCreateRequestModel requestModel = setupMerchantCreateModel(merchant);

        when(merchantRepository.findById(any(UUID.class))).thenReturn(Optional.of(merchant));
        when(merchantRepository.save(any(Merchant.class))).thenReturn(merchant);

        StandardResponseModel response = merchantService.editOpen(merchant.getId(), false);

        assertNotNull(response);
        assertEquals(200, response.getStatusCode());
        assertFalse(response.getMessage().containsValue("true"));
    }

    @Test
    @DisplayName("Edit merchant open state throws ExtendedEntityNotFoundException")
    void editMerchantOpenStateThrowsExtendedEntityNotFoundException() {
        when(merchantRepository.findById(any(UUID.class))).thenReturn(Optional.empty());

        assertThrows(ExtendedEntityNotFoundException.class, () -> merchantService.editOpen(UUID.randomUUID(), true));
    }

    @Test
    @DisplayName("Edit merchant open state throws InternalServerErrorException")
    void editMerchantOpenStateThrowsInternalServerErrorException() {
        when(merchantRepository.findById(any(UUID.class))).thenReturn(Optional.of(new Merchant()));
        when(merchantRepository.save(any(Merchant.class))).thenThrow(new RuntimeException());

        assertThrows(InternalServerErrorException.class, () -> merchantService.editOpen(UUID.randomUUID(), true));
    }

    @Test
    @DisplayName("Delete merchant successfully")
    void deleteMerchantSuccessfully() {
        when(merchantRepository.findById(merchant.getId())).thenReturn(Optional.of(merchant));

        StandardResponseModel response = merchantService.delete(merchant.getId());

        assertNotNull(response);
        assertEquals(200, response.getStatusCode());
    }

}
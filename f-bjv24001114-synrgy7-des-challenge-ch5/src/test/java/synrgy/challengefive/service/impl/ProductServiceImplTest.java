package synrgy.challengefive.service.impl;

import jakarta.persistence.criteria.Predicate;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import synrgy.challengefive.error.exception.ExtendedEntityNotFoundException;
import synrgy.challengefive.error.exception.InternalServerErrorException;
import synrgy.challengefive.model.entity.Merchant;
import synrgy.challengefive.model.entity.MerchantTest;
import synrgy.challengefive.model.entity.Product;
import synrgy.challengefive.model.entity.ProductTest;
import synrgy.challengefive.model.request.product.ProductCreateRequestModel;
import synrgy.challengefive.model.request.product.ProductEditRequestModel;
import synrgy.challengefive.model.standard.StandardResponseModel;
import synrgy.challengefive.repository.MerchantRepository;
import synrgy.challengefive.repository.ProductRepository;

import java.util.Collections;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ProductServiceImplTest {

    @InjectMocks
    private ProductServiceImpl productService;
    @Mock
    private ProductRepository productRepository;
    @Mock
    private MerchantRepository merchantRepository;
    @Mock
    private Specification<Product> specification;
    @Mock
    private Pageable pageable;
    private Product product;

    public static ProductCreateRequestModel setupProductCreateModel(Merchant merchant, String name, Double price) {
        ProductCreateRequestModel productCreateRequestModel = new ProductCreateRequestModel();
        productCreateRequestModel.setIdMerchant(merchant.getId());
        productCreateRequestModel.setName(name);
        productCreateRequestModel.setPrice(price);

        return productCreateRequestModel;
    }

    public static  ProductEditRequestModel setupProductEditModel(String name, Double price) {
        ProductEditRequestModel productEditRequestModel = new ProductEditRequestModel();
        productEditRequestModel.setName(name);
        productEditRequestModel.setPrice(price);

        return productEditRequestModel;
    }

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        product = ProductTest.setupEntity(MerchantTest.setupEntity());
    }

    @Test
    @DisplayName("List products successfully")
    void listProductsSuccessfully() {
        Page<Product> products = new PageImpl<>(Collections.singletonList(product));

        // Mock the toPredicate method
        when(specification.toPredicate(any(), any(), any())).thenReturn(mock(Predicate.class));
        when(productRepository.findAll(any(Specification.class), any(Pageable.class))).thenReturn(products);

        StandardResponseModel response = productService.list(pageable, null);
        JSONObject data = response.toJsonObject().getJSONObject("data");
        JSONArray productsArray = data.getJSONArray("products");

        assertNotNull(response);
        assertEquals(200, response.getStatusCode());
        assertFalse(productsArray.isEmpty());
    }

    @Test
    @DisplayName("Get product successfully")
    void getProductSuccessfully() {
        when(productRepository.findById(product.getId())).thenReturn(java.util.Optional.of(product));

        StandardResponseModel response = productService.get(product.getId());
        JSONObject data = response.toJsonObject().getJSONObject("data");
        JSONObject productData = data.getJSONObject("product");

        assertNotNull(response);
        assertEquals(200, response.getStatusCode());
        assertEquals(product.getId().toString(), productData.getString("id"));
    }

    @Test
    @DisplayName("List products throws InternalServerErrorException")
    void listProductsThrowsInternalServerErrorException() {
        when(productRepository.findAll()).thenThrow(new RuntimeException());

        assertThrows(InternalServerErrorException.class, () -> productService.list(pageable, "product"));
    }

    @Test
    @DisplayName("Get product throws InternalServerErrorException")
    void getProductThrowsInternalServerErrorException() {
        when(productRepository.findById(product.getId())).thenThrow(new RuntimeException());

        assertThrows(InternalServerErrorException.class, () -> productService.get(product.getId()));
    }

    @Test
    @DisplayName("Get product throws ExtendedEntityNotFoundException")
    void getProductThrowsExtendedEntityNotFoundException() {
        when(productRepository.findById(any())).thenReturn(java.util.Optional.empty());

        assertThrows(ExtendedEntityNotFoundException.class, () -> productService.get(product.getId()));
    }

    @Test
    @DisplayName("Save product successfully")
    void saveProductSuccessfully() {
        ProductCreateRequestModel requestModel = setupProductCreateModel(product.getIdMerchant(), product.getName(), product.getPrice());

        when(merchantRepository.existsById(product.getIdMerchant().getId())).thenReturn(true);
        when(productRepository.save(any(Product.class))).thenReturn(product);
        StandardResponseModel response = productService.save(requestModel);
        UUID productIdResponse = UUID.fromString((String) response.toJsonObject()
                .getJSONObject("data")
                .getJSONObject("product")
                .get("id"));

        assertNotNull(response);
        assertEquals(201, response.getStatusCode());
        assertEquals(product.getId(), productIdResponse);
    }

    @Test
    @DisplayName("Save product throws InternalServerErrorException")
    void saveProductThrowsInternalServerErrorException() {
        ProductCreateRequestModel requestModel = setupProductCreateModel(product.getIdMerchant(), product.getName(), product.getPrice());

        when(merchantRepository.existsById(product.getIdMerchant().getId())).thenReturn(true);
        when(productRepository.save(any(Product.class))).thenThrow(new RuntimeException());

        assertThrows(InternalServerErrorException.class, () -> productService.save(requestModel));
    }

    @Test
    @DisplayName("Save product throws ExtendedEntityNotFoundException")
    void saveProductThrowsExtendedEntityNotFoundException() {
        ProductCreateRequestModel requestModel = setupProductCreateModel(product.getIdMerchant(), product.getName(), product.getPrice());

        when(merchantRepository.existsById(product.getIdMerchant().getId())).thenReturn(false);

        assertThrows(ExtendedEntityNotFoundException.class, () -> productService.save(requestModel));
    }

    @Test
    @DisplayName("Edit product successfully")
    void editProductSuccessfully() {
        ProductEditRequestModel requestModel = setupProductEditModel(product.getName(), product.getPrice());

        when(productRepository.findById(product.getId())).thenReturn(java.util.Optional.of(product));
        when(productRepository.save(any(Product.class))).thenReturn(product);

        StandardResponseModel response = productService.edit(product.getId(), requestModel);
        UUID productIdResponse = UUID.fromString((String) response.toJsonObject()
                .getJSONObject("data")
                .getJSONObject("product")
                .get("id"));

        assertNotNull(response);
        assertEquals(200, response.getStatusCode());
        assertEquals(product.getId(), productIdResponse);
    }

    @Test
    @DisplayName("Edit product throws InternalServerErrorException")
    void editProductThrowsInternalServerErrorException() {
        ProductEditRequestModel requestModel = setupProductEditModel(product.getName(), product.getPrice());

        when(productRepository.findById(product.getId())).thenReturn(java.util.Optional.of(product));
        when(productRepository.save(any(Product.class))).thenThrow(new RuntimeException());

        assertThrows(InternalServerErrorException.class, () -> productService.edit(product.getId(), requestModel));
    }

    @Test
    @DisplayName("Edit product throws ExtendedEntityNotFoundException")
    void editProductThrowsExtendedEntityNotFoundException() {
        ProductEditRequestModel requestModel = setupProductEditModel(product.getName(), product.getPrice());

        when(productRepository.findById(product.getId())).thenReturn(java.util.Optional.empty());

        assertThrows(ExtendedEntityNotFoundException.class, () -> productService.edit(product.getId(), requestModel));
    }

    @Test
    @DisplayName("Delete product successfully")
    void deleteProductSuccessfully() {
        when(productRepository.findById(product.getId())).thenReturn(java.util.Optional.of(product));

        StandardResponseModel response = productService.delete(product.getId());
        String messageStatus = response.toJsonObject()
                        .getJSONObject("message")
                                .getString("status");

        assertNotNull(response);
        assertEquals(200, response.getStatusCode());
        assertEquals("Success delete product", messageStatus);
    }

    @Test
    @DisplayName("Delete product throws InternalServerErrorException")
    void deleteProductThrowsInternalServerErrorException() {
        when(productRepository.findById(product.getId())).thenReturn(java.util.Optional.of(product));
        doThrow(new RuntimeException()).when(productRepository).delete(any(Product.class));

        assertThrows(InternalServerErrorException.class, () -> productService.delete(product.getId()));
    }

    @Test
    @DisplayName("Delete product throws ExtendedEntityNotFoundException")
    void deleteProductThrowsExtendedEntityNotFoundException() {
        when(productRepository.findById(product.getId())).thenReturn(java.util.Optional.empty());

        assertThrows(ExtendedEntityNotFoundException.class, () -> productService.delete(product.getId()));
    }

}
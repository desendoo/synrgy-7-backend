package synrgy.challengefive.service.impl;

import jakarta.persistence.criteria.Predicate;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import synrgy.challengefive.model.entity.User;
import synrgy.challengefive.model.entity.UserTest;
import synrgy.challengefive.model.request.user.UserCreateRequestModel;
import synrgy.challengefive.model.request.user.UserEditRequestModel;
import synrgy.challengefive.model.standard.StandardResponseModel;
import synrgy.challengefive.repository.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl userService;
    @Mock
    private UserRepository userRepository;
    @Mock
    private BCryptPasswordEncoder passwordEncoder;
    @Mock
    private Specification<User> specification;
    @Mock
    private Pageable pageable;

    private User user;

    public static UserCreateRequestModel setupUserCreateModel(User user) {
        UserCreateRequestModel userCreateRequestModel = new UserCreateRequestModel();
        userCreateRequestModel.setUsername(user.getUsername());
        userCreateRequestModel.setEmail(user.getEmail());
        userCreateRequestModel.setPassword("password");

        return userCreateRequestModel;
    }

    public static UserEditRequestModel setupUserEditRequestModel(String username, String email, String password) {
        UserEditRequestModel userEditRequestModel = new UserEditRequestModel();
        userEditRequestModel.setUsername(username);
        userEditRequestModel.setEmail(email);
        userEditRequestModel.setPassword(password);

        return userEditRequestModel;
    }

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        user = UserTest.setupEntity();
    }

    @Test
    @DisplayName("List users successfully")
    void listUsersSuccessfully() {
        Page<User> users = new PageImpl<>(List.of(user));

        // Mock the toPredicate method
        when(specification.toPredicate(any(), any(), any())).thenReturn(mock(Predicate.class));
        when(userRepository.findAll(any(Specification.class), any(Pageable.class))).thenReturn(users);

        StandardResponseModel response = userService.list(pageable, "username", "email");
        JSONObject data = response.toJsonObject()
                .getJSONObject("data");
        JSONArray usersArray = data.getJSONArray("users");

        verify(userRepository, times(1)).findAll(any(Specification.class), any(Pageable.class));
        assertNotNull(response);
        assertFalse(usersArray.isEmpty());
    }

    @Test
    @DisplayName("Get user successfully")
    void getUserSuccessfully() {
        when(userRepository.findById(any(UUID.class))).thenReturn(Optional.of(user));

        StandardResponseModel response = userService.get(UUID.randomUUID());
        String emailResponse = response.toJsonObject()
                .getJSONObject("data")
                .getJSONObject("user")
                .getString("email");

        verify(userRepository, times(1)).findById(any(UUID.class));
        assertNotNull(response);
        assertEquals(user.getEmail(), emailResponse);
    }

    @Test
    @DisplayName("Save user successfully")
    void saveUserSuccessfully() {
        UserCreateRequestModel requestModel = setupUserCreateModel(user);

        when(userRepository.save(any(User.class))).thenReturn(user);
        when(passwordEncoder.encode(anyString())).thenReturn("encodedPassword");

        StandardResponseModel response = userService.save(requestModel);
        String emailResponse = response.toJsonObject()
                .getJSONObject("data")
                .getJSONObject("user")
                .getString("email");

        verify(userRepository, times(1)).save(any(User.class));
        assertNotNull(response);
        assertEquals(user.getEmail(), emailResponse);
    }

    @Test
    @DisplayName("Edit user successfully")
    void editUserSuccessfully() {
        UserEditRequestModel requestModel = setupUserEditRequestModel("newUsername", "newEmail@mail.com",
                "password");

        when(userRepository.findById(any(UUID.class))).thenReturn(Optional.of(user));
        when(userRepository.save(any(User.class))).thenReturn(user);
        when(passwordEncoder.encode(anyString())).thenReturn("encodedPassword");

        StandardResponseModel response = userService.edit(UUID.randomUUID(), requestModel);
        String emailResponse = response.toJsonObject()
                .getJSONObject("data")
                .getJSONObject("user")
                .getString("email");

        verify(userRepository, times(1)).save(any(User.class));
        assertEquals("newEmail@mail.com", emailResponse);
    }

    @Test
    @DisplayName("Delete user successfully")
    void deleteUserSuccessfully() {
        when(userRepository.findById(any(UUID.class))).thenReturn(Optional.of(user));

        StandardResponseModel response = userService.delete(UUID.randomUUID());

        verify(userRepository, times(1)).delete(any(User.class));
        assertEquals(204, response.getStatusCode());
    }
}
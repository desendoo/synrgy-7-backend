package synrgy.challengefive.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import synrgy.challengefive.model.entity.MerchantTest;
import synrgy.challengefive.model.entity.ProductTest;
import synrgy.challengefive.model.entity.*;
import synrgy.challengefive.model.request.order.OrderCreateRequestModel;
import synrgy.challengefive.model.request.order.OrderDetailCreateRequestModel;
import synrgy.challengefive.model.standard.StandardResponseModel;
import synrgy.challengefive.repository.OrderDetailRepository;
import synrgy.challengefive.repository.OrderRepository;
import synrgy.challengefive.repository.UserRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class OrderServiceImplTest {

    @Mock
    private OrderRepository orderRepository;
    @Mock
    private UserRepository userRepository;
    @Mock
    private OrderDetailRepository orderDetailRepository;
    @Mock
    private ProductServiceImpl productService;
    @InjectMocks
    private OrderServiceImpl<Order> orderService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    public static OrderCreateRequestModel setupOrderCreateModel(User user, Order order) {
        OrderCreateRequestModel orderCreateRequestModel = new OrderCreateRequestModel();
        orderCreateRequestModel.setIdUser(user.getId());
        orderCreateRequestModel.setDestinationAddress(order.getDestinationAddress());

        return orderCreateRequestModel;
    }

    public static OrderDetailCreateRequestModel setupOrderDetailCreateModel(Order order, Product product,
                                                                            OrderDetail orderDetail) {
        OrderDetailCreateRequestModel orderDetailCreateRequestModel = new OrderDetailCreateRequestModel();
        orderDetailCreateRequestModel.setQuantity(orderDetail.getQuantity());
        orderDetailCreateRequestModel.setIdOrder(order.getId());
        orderDetailCreateRequestModel.setIdProduct(product.getId());

        return orderDetailCreateRequestModel;
    }

    @Test
    @DisplayName("Order created successfully")
    void orderCreatedSuccessfully() {
        User user = UserTest.setupEntity();
        Order order = OrderTest.setupEntity(user);

        OrderCreateRequestModel orderCreateRequestModel = setupOrderCreateModel(user, order);

        // Validate if the user exists
        when(userRepository.existsById(orderCreateRequestModel.getIdUser())).thenReturn(true);
        // Create the order
        when(userRepository.getReferenceById(orderCreateRequestModel.getIdUser())).thenReturn(user);
        // Save the order
        when(orderRepository.save(any(Order.class))).thenReturn(order);
        StandardResponseModel response = orderService.save(orderCreateRequestModel);

        assertNotNull(response);
        assertEquals(HttpStatus.CREATED.value(), response.getStatusCode());
        assertNotNull(response.getData());
        assertNotNull(response.getData().get("order"));
    }

    // @Test
    // @DisplayName("OrderDetail created successfully")
    // void orderDetailCreatedSuccessfully() {
    //     User user = UserTest.setupEntity();
    //     Order order = OrderTest.setupEntity(user);
    //     Merchant merchant = MerchantTest.setupEntity();
    //     Product product = ProductTest.setupEntity(merchant);
    //     OrderDetail orderDetail = OrderDetailTest.setupEntity(order, product);
    //     OrderDetailCreateRequestModel orderDetailCreateRequestModel = setupOrderDetailCreateModel(order, product, orderDetail);
    //
    //     // Validate if the order exists
    //     when(orderRepository.existsById(orderDetailCreateRequestModel.getIdOrder())).thenReturn(true);
    //     // Validate if the product exists
    //     when(productService.fetchProductById(orderDetailCreateRequestModel.getIdProduct())).thenReturn(product);
    //     // Create the order detail
    //     when(orderRepository.getReferenceById(orderDetailCreateRequestModel.getIdOrder())).thenReturn(order);
    //     // Save the order detail
    //     when(orderDetailRepository.save(any(OrderDetail.class))).thenReturn(orderDetail);
    //     StandardResponseModel response = orderService.save(orderDetailCreateRequestModel);
    //
    //     assertNotNull(response);
    //     assertEquals(HttpStatus.CREATED.value(), response.getStatusCode());
    //     assertNotNull(response.getData());
    //     assertNotNull(response.getData().get("orderDetail"));
    // }

}
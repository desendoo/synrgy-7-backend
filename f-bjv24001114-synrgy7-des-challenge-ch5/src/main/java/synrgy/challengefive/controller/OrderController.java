package synrgy.challengefive.controller;


import jakarta.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import synrgy.challengefive.model.entity.Order;
import synrgy.challengefive.model.request.order.OrderCreateRequestModel;
import synrgy.challengefive.model.standard.StandardResponseModel;
import synrgy.challengefive.service.OrderService;

import java.util.UUID;

@RestController
@RequestMapping("/v1")
public class OrderController {

    private final OrderService<Order> orderService;

    public OrderController(OrderService<Order> orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/orders")
    public ResponseEntity<StandardResponseModel> getOrders(
            @RequestParam(required = false, name = "idUser") UUID idUser,
            @PageableDefault(page = 0, size = 5, sort = {"orderTime"}) Pageable pageable
    ) {
        StandardResponseModel response = orderService.list(pageable, idUser);

        return response.toResponseEntity();
    }


    @PostMapping("/orders")
public ResponseEntity<StandardResponseModel> saveOrder(
            @Valid @RequestBody OrderCreateRequestModel request
    ) {
        StandardResponseModel response = orderService.save(request);

        return response.toResponseEntity();
    }

}
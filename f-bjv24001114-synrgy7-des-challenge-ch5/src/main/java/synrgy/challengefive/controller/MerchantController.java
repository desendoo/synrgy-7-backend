package synrgy.challengefive.controller;

import jakarta.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import synrgy.challengefive.model.entity.Merchant;
import synrgy.challengefive.model.request.merchant.MerchantCreateRequestModel;
import synrgy.challengefive.model.request.merchant.MerchantEditRequestModel;
import synrgy.challengefive.model.standard.StandardResponseModel;
import synrgy.challengefive.service.MerchantService;

import java.util.UUID;

@RestController
@RequestMapping("/v1")
public class MerchantController {

    private final MerchantService<Merchant> merchantService;

    public MerchantController(MerchantService<Merchant> merchantService) {
        this.merchantService = merchantService;
    }

    @GetMapping("/merchants")
    public ResponseEntity<StandardResponseModel> getMerchants(
            @RequestParam(required = false, name = "open") Boolean open,
            @PageableDefault(page = 0, size = 5, sort = {"name"}) Pageable pageable
    ) {
        StandardResponseModel response = merchantService.list(pageable, open);

        return response.toResponseEntity();
    }

    @GetMapping("/merchants/{id}")
    public ResponseEntity<StandardResponseModel> getMerchant(
            @PathVariable("id") UUID id
    ) {
        StandardResponseModel response = merchantService.get(id);

        return response.toResponseEntity();
    }

    @PostMapping("/merchants")
    public ResponseEntity<StandardResponseModel> saveMerchant(
            @Valid @RequestBody MerchantCreateRequestModel request
            ) {
        StandardResponseModel response = merchantService.save(request);

        return response.toResponseEntity();
    }

    @PutMapping("/merchants/{id}")
    public ResponseEntity<StandardResponseModel> editMerchant(
            @PathVariable("id") UUID id,
            @Valid @RequestBody MerchantEditRequestModel request
    ) {
        StandardResponseModel response = merchantService.edit(id, request);

        return response.toResponseEntity();
    }

    @DeleteMapping("/merchants/{id}")
    public ResponseEntity<StandardResponseModel> deleteMerchant(
            @PathVariable("id") UUID id
    ) {
        StandardResponseModel response = merchantService.delete(id);

        return response.toResponseEntity();
    }

}

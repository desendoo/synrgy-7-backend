package synrgy.challengefive.controller;

import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import synrgy.challengefive.config.FileStorageConfig;
import synrgy.challengefive.error.exception.FileStorageException;
import synrgy.challengefive.model.response.FileUploadResponse;
import synrgy.challengefive.model.standard.StandardResponseModel;
import synrgy.challengefive.service.FileStorageService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@EnableCaching
@RestController
public class FileController {

    private static final Logger logger = LoggerFactory.getLogger(FileController.class);

    private final FileStorageService fileStorageService;
    private final FileStorageConfig fileStorageConfig;

    public FileController(FileStorageService fileStorageService, FileStorageConfig fileStorageConfig) {
        this.fileStorageService = fileStorageService;
        this.fileStorageConfig = fileStorageConfig;
    }

    @PostMapping(
            value = "/v1/upload",
            consumes = {"multipart/form-data", "application/json"}
    ) public ResponseEntity<StandardResponseModel> uploadFile(
            @RequestParam("file") MultipartFile file
    ) throws IOException {
        StandardResponseModel response = new StandardResponseModel();

        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("ddMyyyyhhmmss");
//        String strDate = formatter.format(date);
        String strDate = String.valueOf(UUID.randomUUID());
        String nameFormat= file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") );
        if (nameFormat.isEmpty()){
            nameFormat = ".png";
        }
        String fileName = fileStorageConfig.getUploadDir() + strDate + nameFormat;


        String fileNameforDOwnload = strDate + nameFormat;
        Path TO = Paths.get(fileName);

//        //validasi hanya boleh PNG
//        if (!file.getContentType().equals("image/png")){
//            return null;// eror
//        }

        try {

            Files.copy(file.getInputStream(), TO); // pengolahan upload disini :

        } catch (Exception e) {
            e.printStackTrace();
            FileUploadResponse fileUploadResponse = new FileUploadResponse(fileNameforDOwnload, null,
                    file.getContentType(), file.getSize(), e.getMessage());
            throw new FileStorageException(fileUploadResponse, e.getMessage());
        }

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/showFile/")
                .path(fileNameforDOwnload)
                .toUriString();


        FileUploadResponse aFalse = new FileUploadResponse(fileNameforDOwnload, fileDownloadUri,
                file.getContentType(), file.getSize(), "false");

        response.putData("file", aFalse);
        response.setStatusCode(HttpStatus.CREATED.value());

        return response.toResponseEntity();
    }

    @GetMapping("/v1/showFile/{fileName:.+}")
    public ResponseEntity<Resource> showFile(
            @PathVariable String fileName,
            HttpServletRequest request
    ) { // Load file as Resource : step 1 load path lokasi name file
        Resource resource = fileStorageService.loadFileAsResource(fileName);
        // Try to determine file's content type
        String contentType = null;
        try {
            // dapatan URL download
//            System.out.println("resource.getFile().getAbsolutePath" + resource.getFile().getAbsolutePath());
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());

        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }
        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";// type .json
        }
//        System.out.println("filename=2=" + HttpHeaders.CONTENT_DISPOSITION);
//        System.out.println("filename=3=" + resource.getFilename());
//        System.out.println("filename=3=" + resource);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                // .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        String.format("attachment; filename=\"%s\"",
                                resource.getFilename()))
                .body(resource);
    }

}

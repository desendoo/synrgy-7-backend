package synrgy.challengefive.controller;

import jakarta.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import synrgy.challengefive.model.entity.User;
import synrgy.challengefive.model.request.user.UserCreateRequestModel;
import synrgy.challengefive.model.request.user.UserEditRequestModel;
import synrgy.challengefive.model.standard.StandardResponseModel;
import synrgy.challengefive.service.UserService;

import java.util.*;

@RestController
@RequestMapping("/v1")
public class UserController {

    // private RestTemplate restTemplate = new RestTemplate();
    private final UserService<User> userService;

    public UserController(UserService<User> userService) {
        this.userService = userService;
    }

    @GetMapping("/users")
    public ResponseEntity<StandardResponseModel> getUsers(
            @RequestParam(required = false, name = "username") String username,
            @RequestParam(required = false, name = "email") String email,
            @PageableDefault(page = 0, size = 5, sort = {"username"}) Pageable pageable
    ) {
        StandardResponseModel response = userService.list(pageable, username, email);

        return response.toResponseEntity();
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<StandardResponseModel> getUser(
            @PathVariable(name = "id") UUID id
    ) {
        StandardResponseModel response = userService.get(id);

        return response.toResponseEntity();
    }

    @PostMapping("/users")
    public ResponseEntity<StandardResponseModel> createUser(
            @Valid @RequestBody UserCreateRequestModel request
    ) {
        StandardResponseModel response = userService.save(request);

        return response.toResponseEntity();
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<StandardResponseModel> editUser(
            @PathVariable(name = "id") UUID id,
            @Valid @RequestBody UserEditRequestModel request
    ) {
        StandardResponseModel response = userService.edit(id, request);

        return response.toResponseEntity();
    }

    /*
    @GetMapping("/v1/objects")
    public ResponseEntity<StandardResponseModel> index() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");

        String url = "https://api.restful-api.dev/objects?id=2";
        // Call with string query parameter
        Object restResponse = restTemplate.exchange(
                url,
                HttpMethod.GET,
                new HttpEntity<>(headers),
                Object.class).getBody();

        JSONArray jsonArray = new JSONArray(restResponse.toString());

        StandardResponseModel response = new StandardResponseModel();
        response.setStatusCode(HttpStatus.OK.value());
        response.putData("data", restResponse);

        return ResponseEntity
                .status(HttpStatus.valueOf(response.getStatusCode()))
                .body(response);
    }

    @GetMapping("/v1/users")
    public ResponseEntity<StandardResponseModel> getUsers() {
        Map<String, String> user = new HashMap<>();
        user.put("name", "John Doe");
        user.put("email", "johndoe@mail.com");
        Map<String, String> user2 = new HashMap<>();
        user2.put("name", "Will Smith");
        user2.put("email", "willsmith@mail.com");

        List<Map<String, String>> users = new ArrayList<>();
        users.add(user);
        users.add(user2);

        StandardResponseModel response = new StandardResponseModel();
        response.setStatusCode(HttpStatus.OK.value());
        response.putData("users", users);

        return ResponseEntity
                .status(HttpStatus.valueOf(response.getStatusCode()))
                .body(response);
    }

    @PostMapping("/v1/users")
    public ResponseEntity<StandardResponseModel> createUser() {
        Map<String, Object> user = new HashMap<>();
        user.put("name", "John Doe");
        user.put("email", "johndoe@mail.com");

        StandardResponseModel response = new StandardResponseModel();
        response.setStatusCode(HttpStatus.CREATED.value());
        response.setData(user);

        return ResponseEntity
                .status(HttpStatus.valueOf(response.getStatusCode()))
                .body(response);
    }
    */

}

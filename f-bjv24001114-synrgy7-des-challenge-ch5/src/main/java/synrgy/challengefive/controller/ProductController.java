package synrgy.challengefive.controller;

import jakarta.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import synrgy.challengefive.model.entity.Product;
import synrgy.challengefive.model.request.product.ProductCreateRequestModel;
import synrgy.challengefive.model.request.product.ProductEditRequestModel;
import synrgy.challengefive.model.standard.StandardResponseModel;
import synrgy.challengefive.service.ProductService;

import java.util.UUID;

@RestController
@RequestMapping("/v1")
public class ProductController {

    private final ProductService<Product> productService;

    public ProductController(ProductService<Product> productService) {
        this.productService = productService;
    }

    @GetMapping("/products")
    public ResponseEntity<StandardResponseModel> getProducts(
            @RequestParam(required = false, name = "name") String name,
            @PageableDefault(page = 0, size = 5, sort = {"name"}) Pageable pageable
            // @RequestParam(value = "page", defaultValue = "0") int page,
            // @RequestParam(value = "size", defaultValue = "5") int size
    ) {
        StandardResponseModel response = productService.list(pageable, name);

        return response.toResponseEntity();
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<StandardResponseModel> getProduct(
            @PathVariable("id") UUID id
    ) {
        StandardResponseModel response = productService.get(id);

        return response.toResponseEntity();
    }

    @PostMapping("/products")
    public ResponseEntity<StandardResponseModel> saveProduct(
            @Valid @RequestBody ProductCreateRequestModel request
    ) {
        StandardResponseModel response = productService.save(request);

        return response.toResponseEntity();
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<StandardResponseModel> editProduct(
            @PathVariable("id") UUID id,
            @Valid @RequestBody ProductEditRequestModel request
    ) {
        StandardResponseModel response = productService.edit(id, request);

        return response.toResponseEntity();
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<StandardResponseModel> deleteProduct(
            @PathVariable("id") UUID id
    ) {
        StandardResponseModel response = productService.delete(id);

        return response.toResponseEntity();
    }

}

package synrgy.challengefive.service.impl;

import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.criteria.Join;
import jakarta.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import synrgy.challengefive.error.exception.ExtendedEntityNotFoundException;
import synrgy.challengefive.error.exception.InternalServerErrorException;
import synrgy.challengefive.model.dto.ExtendedOrderDTO;
import synrgy.challengefive.model.dto.OrderDetailDTO;
import synrgy.challengefive.model.request.product.ProductsOrder;
import synrgy.challengefive.utils.CustomMapper;
import synrgy.challengefive.model.entity.Order;
import synrgy.challengefive.model.entity.OrderDetail;
import synrgy.challengefive.model.entity.Product;
import synrgy.challengefive.model.entity.User;
import synrgy.challengefive.model.request.order.OrderCreateRequestModel;
import synrgy.challengefive.model.request.order.OrderDetailCreateRequestModel;
import synrgy.challengefive.model.standard.StandardResponseModel;
import synrgy.challengefive.repository.OrderDetailRepository;
import synrgy.challengefive.repository.OrderRepository;
import synrgy.challengefive.repository.UserRepository;
import synrgy.challengefive.service.OrderDetailService;
import synrgy.challengefive.service.OrderService;
import synrgy.challengefive.service.ProductService;

import java.util.*;

@Service
public class OrderServiceImpl<T> implements OrderService<T>, OrderDetailService<T> {

    private final OrderRepository orderRepository;
    private final UserRepository userRepository;
    private final OrderDetailRepository orderDetailRepository;
    private final ProductService<Product> productService;

    public OrderServiceImpl(OrderRepository orderRepository, UserRepository userRepository, OrderDetailRepository orderDetailRepository, ProductService<Product> productService) {
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
        this.orderDetailRepository = orderDetailRepository;
        this.productService = productService;
    }

    @Override
    public StandardResponseModel list(Pageable pageable, UUID idUser) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            Specification<Order> specification = (root, query, criteriaBuilder) -> {
                if (idUser != null) {
                    Join<Order, User> userJoin = root.join("idUser");
                    // Get Order by User ID specification
                    return criteriaBuilder.equal(userJoin.get("id"), idUser);
                }
                return null;
            };

            Page<Order> orders = orderRepository.findAll(specification, pageable);
            List<ExtendedOrderDTO> extendedOrderDetailDTOList = new ArrayList<>();
            orders.forEach(order -> {
                List<OrderDetail> fetchedOrderDetail = orderDetailRepository.findByIdOrder(order.getId());
                List<OrderDetailDTO> orderDetailDTOList = new ArrayList<>();
                fetchedOrderDetail.forEach(orderDetail -> {
                    orderDetailDTOList.add(CustomMapper.toOrderDetailDTO(orderDetail));
                });
                extendedOrderDetailDTOList.add(CustomMapper.toExtendedOrderDetailDTO(order, orderDetailDTOList));
            });

            response.putData("orders", extendedOrderDetailDTOList);
            response.setPageableData(orders);
            response.success();

            return response;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    /*
    // Order Body Request
    {
        "idUser": "1",
        "destinationAddress": "Jl. Jendral Sudirman No. 1",
        "products": [
            {
                "id": "1",
                "quantity": 1
            },
            {
                "id": "2",
                "quantity": 3
            },
            {
                "id": "3",
                "quantity": 5
            }
    }
    // Order
    {
        "data": {
            "order": {
                    "id": "1",
                    "orderTime": "2021-08-01T00:00:00.000+00:00",
                    "destinationAddress": "Jl. Jendral Sudirman No. 1",
                    "completed": false,
                    "idUser": "1",
                    "orderDetails": [
                        {
                            "id": "1",
                            "quantity": 1,
                            "totalPrice": 10000,
                            "product": {
                                "id": "1",
                                "name": "Product 1",
                                "price": 10000,
                                "idMerchant": "1"
                            }
                        }
                    ]
                }
            }
        }
    }
    // Order Detail
    {
        "data": {
            "orderDetails": [
                {
                    "id": "1",
                    "idOrder": "1",
                    "idProduct": "1",
                    "quantity": 1,
                    "totalPrice": 10000
                }
            ]
        }
     */
    @Override
    @Transactional
    public StandardResponseModel save(OrderCreateRequestModel request) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            if (!userRepository.existsById(request.getIdUser())) {
                throw new ExtendedEntityNotFoundException(User.class);
            }

            Order order = Order.builder()
                    .orderTime(new Date())
                    .completed(false)
                    .destinationAddress(request.getDestinationAddress())
                    .idUser(userRepository.getReferenceById(request.getIdUser()))
                    .build();

            Order result = orderRepository.save(order);

            List<OrderDetailDTO> orderDetailDTOList;
            try {
                // Create OrderDetail for each Product
                orderDetailDTOList = createOrderDetail(result.getId(), request.getProducts());
            } catch (ExtendedEntityNotFoundException e) {
                throw e;
            } catch (Exception e) {
                throw new InternalServerErrorException(e.getMessage());
            }

            // response.putData("order", CustomMapper.toOrderDTO(result));
            response.putData("order", CustomMapper.toExtendedOrderDetailDTO(result, orderDetailDTOList));
            response.putMessageStatus("Success save order");
            response.created();

            return response;
        } catch (EntityNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @Transactional
    private List<OrderDetailDTO> createOrderDetail(UUID idOrder, List<ProductsOrder> productsOrder) {
        List<OrderDetailDTO> orderDetailDTOList = new ArrayList<>();
        try {
            productsOrder.forEach(productOrder -> {
                // Product fetchedProduct = productService.fetchProductById(productOrder.getId());
                // if (fetchedProduct == null) {
                //     throw new ExtendedEntityNotFoundException(Product.class);
                // }
                Optional<Product> fetchedProduct = fetchProductDetailById(productOrder.getId());
                fetchedProduct.orElseThrow(() -> new ExtendedEntityNotFoundException(Product.class));

                double totalPrice = productOrder.getQuantity() * fetchedProduct.get().getPrice();
                OrderDetail orderDetail = OrderDetail.builder()
                        .idOrder(orderRepository.getReferenceById(idOrder))
                        .quantity(productOrder.getQuantity())
                        .idProduct(fetchedProduct.get())
                        .totalPrice(totalPrice)
                        // .calculateTotalPrice(fetchedProduct.getPrice(), fetchedProduct.getId())
                        .build();
                // orderDetail.calculateTotalPrice(fetchedProduct.getPrice(), fetchedProduct.getId());

                OrderDetail result = orderDetailRepository.save(orderDetail);
                orderDetailDTOList.add(CustomMapper.toOrderDetailDTO(result));
            });
            return orderDetailDTOList;
        } catch (EntityNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    private Optional<Product> fetchProductDetailById(UUID idProduct) {
        return Optional.ofNullable(productService.fetchProductById(idProduct));
    }

    // @Override
    // @Transactional
    // public StandardResponseModel save(OrderDetailCreateRequestModel request) {
    //     StandardResponseModel response = new StandardResponseModel();
    //
    //     try {
    //         if (!orderRepository.existsById(request.getIdOrder())) {
    //             throw new ExtendedEntityNotFoundException(Order.class);
    //         }
    //
    //         Product fetchedProduct = productService.fetchProductById(request.getIdProduct());
    //         if (fetchedProduct == null) {
    //             throw new ExtendedEntityNotFoundException(Product.class);
    //         }
    //
    //         double totalPrice = request.getQuantity() * fetchedProduct.getPrice();
    //         OrderDetail orderDetail = OrderDetail.builder()
    //                 .idOrder(orderRepository.getReferenceById(request.getIdOrder()))
    //                 .idProduct(fetchedProduct)
    //                 .quantity(request.getQuantity())
    //                 .totalPrice(totalPrice)
    //                 .build();
    //
    //         OrderDetail result = orderDetailRepository.save(orderDetail);
    //
    //         response.putData("orderDetail", CustomMapper.toOrderDetailDTO(result));
    //         response.putMessageStatus("Success save order detail");
    //         response.created();
    //
    //         return response;
    //     } catch (EntityNotFoundException e) {
    //         throw e;
    //     } catch (Exception e) {
    //         throw new InternalServerErrorException(e.getMessage());
    //     }
    // }

}
package synrgy.challengefive.service.impl;

import jakarta.persistence.criteria.Predicate;
import jakarta.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import synrgy.challengefive.error.exception.ExtendedEntityNotFoundException;
import synrgy.challengefive.error.exception.InternalServerErrorException;
import synrgy.challengefive.model.entity.User;
import synrgy.challengefive.model.request.user.UserCreateRequestModel;
import synrgy.challengefive.model.request.user.UserEditRequestModel;
import synrgy.challengefive.model.standard.StandardResponseModel;
import synrgy.challengefive.repository.UserRepository;
import synrgy.challengefive.service.UserService;
import synrgy.challengefive.utils.CustomMapper;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService<User> {

    private final UserRepository userRepository;
    // bcrypt
    private final BCryptPasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public StandardResponseModel list(Pageable pageable, String username, String email) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            Specification<User> specification = (root, query, criteriaBuilder) -> {
                Predicate predicate = criteriaBuilder.conjunction();
                if (username != null) {
                    predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(criteriaBuilder.lower(root.get("username")), "%" + username.toLowerCase() + "%"));
                }
                if (email != null) {
                    predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(criteriaBuilder.lower(root.get("email")), "%" + email.toLowerCase() + "%"));
                }
                return predicate;
            };

            Page<User> users = userRepository.findAll(specification, pageable);

            response.putData("users", users.getContent().stream().map(CustomMapper::toUserDTO).toList());
            response.setPageableData(users);
            response.success();

            return response;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @Override
    public StandardResponseModel get(UUID id) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            Optional<User> foundUser = userRepository.findById(id);

            foundUser.ifPresentOrElse(user -> {
                response.putData("user", CustomMapper.toUserDTO(user));
                response.success();
            }, () -> {
                throw new ExtendedEntityNotFoundException(User.class);
            });

            return response;
        } catch (ExtendedEntityNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @Override
    @Transactional
    public StandardResponseModel save(UserCreateRequestModel request) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            User user = User.builder()
                    .username(request.getUsername())
                    .email(request.getEmail())
                    .password(passwordEncoder.encode(request.getPassword()))
                    .build();

            User result = userRepository.save(user);

            response.putData("user", CustomMapper.toUserDTO(result));
            response.created();

            return response;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @Override
    public StandardResponseModel edit(UUID id, UserEditRequestModel request) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            Optional<User> foundUser = userRepository.findById(id);

            foundUser.ifPresentOrElse(user -> {
                user.setUsername(request.getUsername());
                user.setEmail(request.getEmail());
                user.setPassword(passwordEncoder.encode(request.getPassword()));

                User result = userRepository.save(user);

                response.putData("user", CustomMapper.toUserDTO(result));
                response.success();
            }, () -> {
                throw new ExtendedEntityNotFoundException(User.class);
            });

            return response;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @Override
    @Transactional
    public StandardResponseModel delete(UUID idUser) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            userRepository.findById(idUser).ifPresentOrElse(user -> {
                userRepository.delete(user);

                response.successNoContent();
                response.putMessageStatus("Success delete user");
            }, () -> {
                throw new ExtendedEntityNotFoundException(User.class);
            });

            return response;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }
}
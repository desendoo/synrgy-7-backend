package synrgy.challengefive.service.impl;

import jakarta.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import synrgy.challengefive.error.exception.ExtendedEntityNotFoundException;
import synrgy.challengefive.error.exception.InternalServerErrorException;
import synrgy.challengefive.utils.CustomMapper;
import synrgy.challengefive.model.entity.Merchant;
import synrgy.challengefive.model.request.merchant.MerchantCreateRequestModel;
import synrgy.challengefive.model.request.merchant.MerchantEditRequestModel;
import synrgy.challengefive.model.standard.StandardResponseModel;
import synrgy.challengefive.repository.MerchantRepository;
import synrgy.challengefive.service.MerchantService;

import java.util.Optional;
import java.util.UUID;

@Service
public class MerchantServiceImpl implements MerchantService<Merchant> {

    private final MerchantRepository merchantRepository;

    public MerchantServiceImpl(MerchantRepository merchantRepository) {
        this.merchantRepository = merchantRepository;
    }

    @Override
    public StandardResponseModel list(Pageable pageable, Boolean open) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            Specification<Merchant> specification = (root, query, criteriaBuilder) -> {
                if (open != null) {
                    return criteriaBuilder.equal(root.get("open"), open);
                }
                return null;
            };

            Page<Merchant> merchants = merchantRepository.findAll(specification, pageable);

            response.putData("merchants", merchants.getContent().stream().map(CustomMapper::toMerchantDTO).toList());
            response.setPageableData(merchants);
            response.success();

            return response;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @Override
    public StandardResponseModel get(UUID id) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            Optional<Merchant> foundMerchant = merchantRepository.findById(id);

            foundMerchant.ifPresentOrElse(merchant -> {
                response.putData("merchant", CustomMapper.toMerchantDTO(merchant));
                response.success();
            }, () -> {
                throw new ExtendedEntityNotFoundException(Merchant.class);
            });

            return response;
        } catch (ExtendedEntityNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @Override
    @Transactional
    public StandardResponseModel save(MerchantCreateRequestModel request) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            Merchant merchant = Merchant.builder()
                    .name(request.getName())
                    .location(request.getLocation())
                    .open(request.getOpen())
                    .build();

            Merchant result = merchantRepository.save(merchant);

            response.putData("merchant", CustomMapper.toMerchantDTO(result));
            response.success(HttpStatus.CREATED.value());
            response.putMessageStatus("Success save merchant");

            return response;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @Override
    @Transactional
    public StandardResponseModel edit(UUID id, MerchantEditRequestModel request) {
        StandardResponseModel response = new StandardResponseModel();
        try {
            Optional<Merchant> foundMerchant = merchantRepository.findById(id);

            foundMerchant.ifPresentOrElse(merchant -> {
                merchant.setName(request.getName());
                merchant.setLocation(request.getLocation());
                merchant.setOpen(request.getOpen());
                Merchant result = merchantRepository.save(merchant);

                response.putData("merchant", CustomMapper.toMerchantDTO(result));
                response.success();
                response.putMessageStatus("Success edit merchant");
            }, () -> {
                throw new ExtendedEntityNotFoundException(Merchant.class);
            });

            return response;
        } catch (ExtendedEntityNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @Override
    @Transactional
    public StandardResponseModel delete(UUID id) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            Optional<Merchant> foundMerchant = merchantRepository.findById(id);

            foundMerchant.ifPresentOrElse(merchant -> {
                merchantRepository.delete(merchant);
                response.success(HttpStatus.OK.value());
                response.putMessageStatus("Success delete merchant");
            }, () -> {
                throw new ExtendedEntityNotFoundException(Merchant.class);
            });

            return response;
        } catch (ExtendedEntityNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @Override
    @Transactional
    public StandardResponseModel editOpen(UUID idMerchant, boolean openState) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            Optional<Merchant> foundMerchant = merchantRepository.findById(idMerchant);

            foundMerchant.ifPresentOrElse(merchant -> {
                merchant.setOpen(openState);
                Merchant result = merchantRepository.save(merchant);

                response.putData("merchant", CustomMapper.toMerchantDTO(result));
                response.success(HttpStatus.OK.value());
                response.putMessageStatus("Success edit merchant open state");
            }, () -> {
                throw new ExtendedEntityNotFoundException(Merchant.class);
            });

            return response;
        } catch (ExtendedEntityNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

}
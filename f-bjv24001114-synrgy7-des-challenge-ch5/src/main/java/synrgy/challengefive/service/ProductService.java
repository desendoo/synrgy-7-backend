package synrgy.challengefive.service;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import synrgy.challengefive.model.entity.Product;
import synrgy.challengefive.model.request.product.ProductCreateRequestModel;
import synrgy.challengefive.model.request.product.ProductEditRequestModel;
import synrgy.challengefive.model.standard.StandardResponseModel;

import java.util.UUID;

@Component
public interface ProductService<T> {

    StandardResponseModel list(Pageable pageable, String name);
    StandardResponseModel get(UUID id);
    StandardResponseModel save(ProductCreateRequestModel request);
    StandardResponseModel edit(UUID id, ProductEditRequestModel request);
    StandardResponseModel delete(UUID id);

    Product fetchProductById(UUID idProduct);

}

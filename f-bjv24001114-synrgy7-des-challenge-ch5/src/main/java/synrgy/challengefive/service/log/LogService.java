package synrgy.challengefive.service.log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import synrgy.challengefive.model.entity.Log;
import synrgy.challengefive.repository.LogRepository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
@ConditionalOnProperty(name = "backup.logging.enable", havingValue = "true", matchIfMissing = true)
public class LogService {
    private static final String LOG_DIRECTORY = "./logs";
    private static final DateTimeFormatter LOG_DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

    @Autowired
    private LogRepository logRepository;

    // @Scheduled(cron = "0 0 0 * * MON") // runs every Monday at 00:00
    // @Scheduled(cron = "0 0 0 * * ?") // runs every day at 00:00
    // @Scheduled(cron = "0 7 21 * * *") // runs at 09:05 PM
    @Scheduled(cron = "${cron.expression:-}") // runs every minute
    public void backupLogs() {
        LocalDateTime oneWeekAgo = LocalDateTime.now().minusWeeks(1);

        try (BufferedReader reader = new BufferedReader(new FileReader(LOG_DIRECTORY + "/logfile-" + oneWeekAgo.format(DateTimeFormatter.ISO_LOCAL_DATE) + ".log"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split("]\\[", 5);
                if (parts.length < 5) {
                    Log log = new Log();
                    log.setMessage(line);

                    logRepository.save(log);
                } else {

                    Log log = new Log();
                    log.setTimestamp(LocalDateTime.parse(parts[0].substring(1), LOG_DATE_FORMAT));
                    log.setLevel(parts[1]);
                    log.setClassName(parts[2]);
                    log.setThread(parts[3]);
                    log.setMessage(parts[4].substring(0, parts[4].length() - 1)); // remove trailing ']'

                    logRepository.save(log);
                }
            }
        } catch (IOException e) {
            System.out.println("Error reading log file");
            // e.printStackTrace();
        }
    }
}
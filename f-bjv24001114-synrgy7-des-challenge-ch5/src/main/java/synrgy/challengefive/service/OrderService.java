package synrgy.challengefive.service;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import synrgy.challengefive.model.request.order.OrderCreateRequestModel;
import synrgy.challengefive.model.standard.StandardResponseModel;

import java.util.UUID;

@Component
public interface OrderService<T> {

    StandardResponseModel list(Pageable pageable, UUID idUser);
    StandardResponseModel save(OrderCreateRequestModel request);

}
package synrgy.challengefive.service;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import synrgy.challengefive.model.request.merchant.MerchantCreateRequestModel;
import synrgy.challengefive.model.request.merchant.MerchantEditRequestModel;
import synrgy.challengefive.model.standard.StandardResponseModel;

import java.util.UUID;

@Component
public interface MerchantService<T> {

     StandardResponseModel list(Pageable pageable, Boolean open);
     StandardResponseModel get(UUID id);
     StandardResponseModel save(MerchantCreateRequestModel request);
     StandardResponseModel edit(UUID id, MerchantEditRequestModel request);
     StandardResponseModel delete(UUID id);
     StandardResponseModel editOpen(UUID idMerchant, boolean openState);

}

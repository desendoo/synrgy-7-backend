package synrgy.challengefive.service;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import synrgy.challengefive.model.request.user.UserCreateRequestModel;
import synrgy.challengefive.model.request.user.UserEditRequestModel;
import synrgy.challengefive.model.standard.StandardResponseModel;

import java.util.UUID;

@Component
public interface UserService<T> {

    StandardResponseModel list(Pageable pageable, String username, String email);
    StandardResponseModel get(UUID id);
    StandardResponseModel save(UserCreateRequestModel request);
    StandardResponseModel edit(UUID id, UserEditRequestModel request);
    StandardResponseModel delete(UUID idUser);

}
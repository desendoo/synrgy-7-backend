package synrgy.challengefive;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@OpenAPIDefinition
@SpringBootApplication
@EnableScheduling
public class ChallengefiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChallengefiveApplication.class, args);
	}

}

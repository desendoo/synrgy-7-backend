package synrgy.challengefive.utils;

import org.springframework.data.domain.Page;
import synrgy.challengefive.model.dto.*;
import synrgy.challengefive.model.entity.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class CustomMapper {

    public static ExtendedOrderDTO toExtendedOrderDetailDTO(Order order, List<OrderDetailDTO> orderDetailDTOList) {
        return ExtendedOrderDTO.extendedOrderDTOBuilder()
                .id(order.getId())
                .orderTime(order.getOrderTime())
                .destinationAddress(order.getDestinationAddress())
                .completed(order.getCompleted())
                .idUser(order.getIdUser().getId())
                .orderDetails(orderDetailDTOList)
                .build();
    }

    public static OrderDetailDTO toOrderDetailDTO(OrderDetail orderDetail) {
        return OrderDetailDTO.builder()
                .id(orderDetail.getId())
                .quantity(orderDetail.getQuantity())
                .totalPrice(orderDetail.getTotalPrice())
                // .order(toOrderDTO(orderDetail.getIdOrder()))
                .product(toProductDTO(orderDetail.getIdProduct()))
                .build();
    }

    public static OrderDTO toOrderDTO(Order order) {
        return OrderDTO.builder()
                .id(order.getId())
                .orderTime(order.getOrderTime())
                .destinationAddress(order.getDestinationAddress())
                .completed(order.getCompleted())
                .idUser(order.getIdUser().getId())
                .build();
    }

    public static UserDTO toUserDTO(User user) {
        return UserDTO.builder()
                .id(user.getId())
                .username(user.getUsername())
                .email(user.getEmail())
                .build();
    }

    public static ProductDTO toProductDTO(Product product) {
        return ProductDTO.builder()
                .id(product.getId())
                .name(product.getName())
                .price(product.getPrice())
                .idMerchant(product.getIdMerchant().getId())
                .build();
    }

    // public static ProductDTO toProductDTO(Product product, UUID idMerchant) {
    //     return ProductDTO.builder()
    //             .id(product.getId())
    //             .name(product.getName())
    //             .price(product.getPrice())
    //             .idMerchant(idMerchant)
    //             .build();
    // }

    public static MerchantDTO toMerchantDTO(Merchant merchant) {
        return MerchantDTO.builder()
                .id(merchant.getId())
                .name(merchant.getName())
                .location(merchant.getLocation())
                .open(merchant.getOpen())
                .build();
    }

    public static Map<String, Object> pageableToMap(Page<?> page) {
        Map<String, Object> pageableMap = new HashMap<>();
        // pageableMap.put("pageable", page.getPageable());
        pageableMap.put("totalPages", page.getTotalPages());
        pageableMap.put("totalElements", page.getTotalElements());
        pageableMap.put("last", page.isLast());
        pageableMap.put("size", page.getSize());
        pageableMap.put("number", page.getNumber());
        pageableMap.put("sort", page.getSort());
        pageableMap.put("numberOfElements", page.getNumberOfElements());
        pageableMap.put("first", page.isFirst());
        pageableMap.put("empty", page.isEmpty());

        return pageableMap;
    }
}
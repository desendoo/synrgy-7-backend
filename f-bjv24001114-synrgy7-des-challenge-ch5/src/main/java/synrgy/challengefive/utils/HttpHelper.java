package synrgy.challengefive.utils;
//
// public class HttpHelper {
//
//     public static WebClient protectedResourceWebClientBuilder(String url, String accessToken) {
//         WebClient webClient = WebClient.builder()
//                 .baseUrl(url)
//                 .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
//                 .defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
//                 .build();
//
//         return webClient;
//     }
//
//     public static WebClient unprotectedResourceWebClientBuilder(String url) {
//         WebClient webClient = WebClient.builder()
//                 .baseUrl(url)
//                 .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
//                 .build();
//
//         return webClient;
//     }
//
//     private static Mono<? extends Throwable> handleError(
//             ClientResponse clientResponse
//     ) {
//         return clientResponse.bodyToMono(String.class).flatMap(errorBody -> {
//             return Mono.error(new RuntimeException("HTTP " + clientResponse.rawStatusCode() + " | " + errorBody));
//         });
//     }
//
//     private static RequestHeadersSpec<?> configurePostRequestBody(
//             WebClient webClient,
//             BodyInserter<MultiValueMap<String, String>, ClientHttpRequest> bodyInserterForm,
//             BodyInserter<Map<String, Object>, ReactiveHttpOutputMessage> bodyInserterBodyObject,
//             BodyInserter<List<Object>, ReactiveHttpOutputMessage> bodyInserterBodyArray
//     ) {
//         if (bodyInserterForm != null) {
//             return webClient.post().body(bodyInserterForm);
//         } else if (bodyInserterBodyObject != null) {
//             return webClient.post().body(bodyInserterBodyObject);
//         } else if (bodyInserterBodyArray != null) {
//             return webClient.post().body(bodyInserterBodyArray);
//         }
//
//         return webClient.post();
//     }
//
//     private static RequestHeadersSpec<?> configurePutRequestBody(
//             WebClient webClient,
//             BodyInserter<MultiValueMap<String, String>, ClientHttpRequest> bodyInserterForm,
//             BodyInserter<Map<String, Object>, ReactiveHttpOutputMessage> bodyInserterBodyObject
//     ) {
//         if (bodyInserterForm != null) {
//             return webClient.put().body(bodyInserterForm);
//         } else if (bodyInserterBodyObject != null) {
//             return webClient.put().body(bodyInserterBodyObject);
//         }
//
//         return webClient.put();
//     }
//
//     private static RequestHeadersSpec<?> configureGetRequest(
//             WebClient webClient
//     ) {
//         return webClient.get();
//     }
//
//     private static RequestHeadersSpec<?> configureGetRequestQueryParameter(
//             WebClient webClient,
//             UriComponents uriComponents
//     ) {
//         return webClient.get().uri(uriComponents.toString());
//     }
//
//     public static ResponseEntity<String> configureHttpRequest(
//             RequestHeadersSpec<?> webClient
//     ) {
//         return webClient
//                 .retrieve()
//                 .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
//                     return handleError(clientResponse);
//                 })
//                 .toEntity(String.class)
//                 .doOnSuccess(success -> {
//                     int responseStatusCode = success.getStatusCode().value();
//                     System.out.println("HTTP Response Code: " + responseStatusCode);
//                 })
//                 .doOnError(error -> {
//                     System.err.println("HTTP Error: ");
//                     error.printStackTrace();
//                     // TODO: Create an Exception specifically to catch error from HTTP request
//                 })
//                 .block();
//     }
//
//     public static ResponseEntity<String> makeHttpPostRequest(
//             WebClient webClient
//     ) {
//         return configureHttpRequest(configurePostRequestBody(webClient, null, null, null));
//     }
//
//     public static ResponseEntity<String> makeHttpPostRequest(
//             WebClient webClient,
//             JSONObject requestBody
//     ) {
//         BodyInserter<Map<String, Object>, ReactiveHttpOutputMessage> bodyInserterBodyObject = BodyInserters.fromValue(requestBody.toMap());
//
//         return configureHttpRequest(configurePostRequestBody(webClient, null, bodyInserterBodyObject, null));
//     }
//
//     public static ResponseEntity<String> makeHttpPostRequest(
//             WebClient webClient,
//             JSONArray requestBody
//     ) {
//         BodyInserter<List<Object>, ReactiveHttpOutputMessage> bodyInserterBodyArray = BodyInserters.fromValue(requestBody.toList());
//
//         return configureHttpRequest(configurePostRequestBody(webClient, null, null, bodyInserterBodyArray));
//     }
//
//     public static ResponseEntity<String> makeHttpPostRequest(
//             WebClient webClient,
//             MultiValueMap<String, String> formParams
//     ) {
//         // MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();
//         // formParams.forEach(multiValueMap::add);
//
//         // BodyInserter<MultiValueMap<String, String>, ClientHttpRequest> bodyInserterForm = BodyInserters.fromFormData(multiValueMap);
//         BodyInserter<MultiValueMap<String, String>, ClientHttpRequest> bodyInserterForm = BodyInserters.fromFormData(formParams);
//
//         return configureHttpRequest(configurePostRequestBody(webClient, bodyInserterForm, null, null));
//     }
//
//     public static ResponseEntity<String> makeHttpPutRequest(
//             WebClient webClient,
//             JSONObject requestBody
//     ) {
//         BodyInserter<Map<String, Object>, ReactiveHttpOutputMessage> bodyInserterBodyObject = BodyInserters.fromValue(requestBody.toMap());
//
//         return configureHttpRequest(configurePutRequestBody(webClient, null, bodyInserterBodyObject));
//     }
//
//     public static ResponseEntity<String> makeHttpGetRequest(
//             WebClient webClient,
//             UriComponents uriComponents
//     ) {
//         return configureHttpRequest(configureGetRequestQueryParameter(webClient, uriComponents));
//     }
//
//     public static ResponseEntity<String> makeHttpGetRequest(
//             WebClient webClient
//     ) {
//         return configureHttpRequest(configureGetRequest(webClient));
//     }
//
//     // ===== ===== ===== ===== ===== ===== ===== ===== =====
//     // ===== ===== =====     Fetchers      ===== ===== =====
//     // ===== ===== ===== ===== ===== ===== ===== ===== =====
//
//     public static String fetchAccessToken(ResponseEntity<String> httpResponse) {
//         return fetchAttributeFromHttpResponseBodyObject(httpResponse, "access_token");
//     }
//
//     public static String fetchIdUserKeycloak(ResponseEntity<String> httpResponse) {
//         return fetchAttributeFromHttpResponseBodyArray(httpResponse, "id", 0);
//     }
//
//     public String fetchRefreshTokenFromHttpResponseBody(ResponseEntity<String> httpResponse) {
//         return fetchAttributeFromHttpResponseBodyObject(httpResponse, "refresh_token");
//     }
//
//     public static String fetchAttributeFromHttpResponseBodyArray(ResponseEntity<String> httpResponse, String key, Integer index) {
//         JSONArray listOfObjects = new JSONArray(httpResponse.getBody());
//         System.out.println(listOfObjects.toString(4));
//
//         String fetchedAttribute = listOfObjects
//                 .getJSONObject(index)
//                 .getString(key);
//
//         return fetchedAttribute;
//     }
//
//     public static String fetchAttributeFromHttpResponseBodyObject(ResponseEntity<String> httpResponse, String key) {
//         JSONObject object = new JSONObject(httpResponse.getBody());
//         System.out.println(object.toString(4));
//
//         return object.getString(key);
//     }
// }
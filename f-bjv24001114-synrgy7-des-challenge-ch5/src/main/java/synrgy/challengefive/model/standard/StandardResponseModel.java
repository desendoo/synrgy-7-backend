package synrgy.challengefive.model.standard;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import synrgy.challengefive.utils.CustomMapper;

import java.util.*;

@Data
public class StandardResponseModel implements StandardData, StandardMessage, StandardError {

    /*
     * Code status request
     */
    @NotNull
    private Integer statusCode;

    /*
     * Data
     */
    @NotNull
    private Map<String, Object> data;

    /*
     * Message
     */
    @NotNull
    private Map<String, Object> message;

    /*
     * Errors
     */
    @NotNull
    private List<Map<String, Object>> errors;

    /* ===== END - PRIVATE FIELDS ===== */

    public StandardResponseModel() {
        this.data = new HashMap<>();
        this.message = new HashMap<>();
        this.errors = new ArrayList<>();
    }

    public ResponseEntity<StandardResponseModel> toResponseEntity() {
        ResponseEntity<StandardResponseModel> body = ResponseEntity.status(this.statusCode).body(this);
        return body;
    }

    public JSONObject toJsonObject() {
        ObjectMapper mapper = new ObjectMapper();
        String responseInString = null;

        try {
            responseInString = mapper.writeValueAsString(this);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        return new JSONObject(responseInString);
    }

    public void  setPageableData(Page<?> page) {
        this.putData("pageable", CustomMapper.pageableToMap(page));
    }

    public void success() {
        this.statusCode = HttpStatus.OK.value();
    }

    public void successNoContent() {
        this.statusCode = HttpStatus.NO_CONTENT.value();
    }

    public void success(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public void success(Integer statusCode, Map<String, Object> data) {
        this.statusCode = statusCode;
        this.data = data;
    }

    public void created() {
        this.statusCode = HttpStatus.CREATED.value();
    }

    public void error(HttpMessage e) {
        setStatusCode(e.getHttpStatusCode());
        putError(e.name(),  e.getExceptionMessage());
    }

    public StandardResponseModel error(Integer statusCode) {
        setStatusCode(statusCode);
        return this;
    }

    public StandardResponseModel error(Integer statusCode, Exception e) {
        setStatusCode(statusCode);
        putError(e.getClass().getSimpleName(),  e.getMessage());
        return this;
    }

    @Override
    public void putData(String key, Object value) {
        this.data.put(key, value);
    }

    @Override
    public void putMessage(String key, Object value) {
        this.message.put(key, value);
    }

    @Override
    public void putError(String key, Object value) {
        Map<String, Object> error = new HashMap<>();
        error.put(key, value);
        this.errors.add(error);
    }

    @Override
    public void appendMessages(Map<String, Object> messages) {
        this.message.putAll(messages);
    }

    @Override
    public void putMessageStatus(String messageStatus) {
        this.putMessage("status", messageStatus);
    }

    @Override
    public void appendData(Map<String, Object> data) {
        this.data.putAll(data);
    }

    @Override
    public void appendErrorFromMap(Map<String, Object> error) {
        error.forEach((k, v) -> {
            Map<String, Object> er = new HashMap<>();
            er.put(k, v);
            this.errors.add(er);
        });
    }

    @Override
    public void appendErrors(ArrayList<Map<String, Object>> errors) {
        this.errors.addAll(errors);
    }

    /* ===== END - PRIVATE METHODS ===== */

}
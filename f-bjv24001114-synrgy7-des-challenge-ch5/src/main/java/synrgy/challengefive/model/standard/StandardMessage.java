package synrgy.challengefive.model.standard;

import java.util.Map;
import java.util.Optional;

public interface StandardMessage {

    void putMessage(String key, Object value);
    void appendMessages(Map<String, Object> message);
    void putMessageStatus(String messageStatus);

}

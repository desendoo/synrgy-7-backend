package synrgy.challengefive.model.standard;

import java.util.ArrayList;
import java.util.Map;

public interface StandardError {

    void putError(String key, Object value);
    void appendErrorFromMap(Map<String, Object> error);
    void appendErrors(ArrayList<Map<String, Object>> errors);

}

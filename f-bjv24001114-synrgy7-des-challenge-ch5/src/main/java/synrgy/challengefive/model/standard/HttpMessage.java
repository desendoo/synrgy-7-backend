package synrgy.challengefive.model.standard;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public enum HttpMessage {

    // 404
    ENTITY_IS_NOT_FOUND("The entity is not found or not registered.", HttpStatus.NOT_FOUND.value()),
    // 409
    USER_ALREADY_REGISTERED("The user is already registered.", HttpStatus.CONFLICT.value()),
    // 500
    INTERNAL_SERVER_ERROR("Internal server error.", HttpStatus.INTERNAL_SERVER_ERROR.value()),
    FILE_STORAGE_ERROR("File storage error.", HttpStatus.INTERNAL_SERVER_ERROR.value())
    ;

    private final String exceptionMessage;
    private final int httpStatusCode;

    HttpMessage(String exceptionMessage, int httpStatusCode) {
        this.exceptionMessage = exceptionMessage;
        this.httpStatusCode = httpStatusCode;
    }
}
package synrgy.challengefive.model.standard;

import java.util.Map;

public interface StandardData {

    void putData(String key, Object value);
    void appendData(Map<String, Object> data);

}

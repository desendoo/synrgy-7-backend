package synrgy.challengefive.model.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.SQLRestriction;
import org.hibernate.annotations.UuidGenerator;
import synrgy.challengefive.model.AbstractDate;

import java.util.UUID;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "order_detail")
@SQLDelete(sql = "UPDATE order_detail SET deleted_date = CURRENT_TIMESTAMP WHERE id = ?")
@SQLRestriction("deleted_date is null")
public class OrderDetail extends AbstractDate {

    @Id
    @UuidGenerator
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "total_price")
    private Double totalPrice;

    @ManyToOne
    @JoinColumn(name = "id_order")
    private Order idOrder;

    @ManyToOne
    @JoinColumn(name = "id_product")
    private Product idProduct;

    /*void calculateTotalPrice(Double productPrice, UUID idProduct) {
        if (this.idProduct.getId().equals(idProduct)) {
            this.totalPrice = productPrice * this.quantity;
        }
    }

    // Builder to encapsulate the total price calculation
    public static class Builder extends OrderDetailBuilder {
        private Product idProduct;
        public Builder idProduct(UUID idProduct) {
            this.idProduct = Product.builder().id(idProduct).build();
            return this;
        }
        public Builder calculateTotalPrice(Double productPrice, UUID idProduct) {
            if (this.idProduct.getId().equals(idProduct)) {
                this.calculateTotalPrice(productPrice, idProduct);
            }
            return this;
        }
    }*/

}

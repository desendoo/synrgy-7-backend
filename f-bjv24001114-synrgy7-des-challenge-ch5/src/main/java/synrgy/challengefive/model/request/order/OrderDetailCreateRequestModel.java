package synrgy.challengefive.model.request.order;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.UUID;

@Data
public class OrderDetailCreateRequestModel {

    @NotNull(message = "Quantity cannot be null")
    private Integer quantity;

    @NotBlank(message = "User cannot be blank")
    @NotNull(message = "User cannot be null")
    @NotEmpty(message = "User cannot be empty")
    private UUID idOrder;

    @NotBlank(message = "User cannot be blank")
    @NotNull(message = "User cannot be null")
    @NotEmpty(message = "User cannot be empty")
    private UUID idProduct;

}
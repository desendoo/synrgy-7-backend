package synrgy.challengefive.model.request.user;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.UUID;

@Data
public class UserEditRequestModel {

    @NotBlank(message = "Username cannot be blank")
    @NotNull(message = "Username cannot be null")
    @NotEmpty(message = "Username cannot be empty")
    @Size(min = 3, max = 255, message = "Username must be between 3 and 255 characters")
    private String username;

    @NotBlank(message = "Email address cannot be blank")
    @NotNull(message = "Email address cannot be null")
    @NotEmpty(message = "Email address cannot be empty")
    @Size(min = 3, max = 255, message = "Email address must be between 3 and 255 characters")
    private String email;

    @NotBlank(message = "Password cannot be blank")
    @NotNull(message = "Password cannot be null")
    @NotEmpty(message = "Password cannot be empty")
    @Size(min = 8, max = 255, message = "Password must be between 8 and 255 characters")
    private String password;

    // @NotBlank(message = "User cannot be blank")
    // @NotNull(message = "User cannot be null")
    // @NotEmpty(message = "User cannot be empty")
    // private UUID idUser;

}
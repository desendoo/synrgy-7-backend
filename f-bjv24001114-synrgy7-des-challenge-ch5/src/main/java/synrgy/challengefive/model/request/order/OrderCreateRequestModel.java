package synrgy.challengefive.model.request.order;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import synrgy.challengefive.model.request.product.ProductsOrder;

import java.util.List;
import java.util.UUID;

@Data
public class OrderCreateRequestModel {

    @NotBlank(message = "Destination address cannot be blank")
    @NotNull(message = "Destination address cannot be null")
    @NotEmpty(message = "Destination address cannot be empty")
    @Size(min = 3, max = 255, message = "Destination address must be between 3 and 255 characters")
    private String destinationAddress;

    @Valid
    @NotEmpty(message = "Products list cannot be empty")
    private List<ProductsOrder> products;

    @NotNull(message = "User cannot be null")
    private UUID idUser;

}
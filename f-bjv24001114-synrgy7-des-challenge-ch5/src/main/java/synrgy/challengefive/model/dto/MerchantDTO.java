package synrgy.challengefive.model.dto;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class MerchantDTO {

    private UUID id;
    private String name;
    private String location;
    private Boolean open;

}
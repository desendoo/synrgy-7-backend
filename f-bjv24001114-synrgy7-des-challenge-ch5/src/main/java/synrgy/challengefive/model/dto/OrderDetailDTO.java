package synrgy.challengefive.model.dto;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class OrderDetailDTO {

    private UUID id;
    private Integer quantity;
    private Double totalPrice;
    // private OrderDTO order;
    private ProductDTO product;

}
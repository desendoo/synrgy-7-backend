package synrgy.challengefive.model.dto;

import lombok.*;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Setter
@Getter
// @Data
// @Builder(toBuilder = true)
public class ExtendedOrderDTO extends OrderDTO {

    private List<OrderDetailDTO> orderDetails;

    @Builder(builderMethodName = "extendedOrderDTOBuilder")
    public ExtendedOrderDTO(UUID id, Date orderTime, String destinationAddress, Boolean completed, UUID idUser, List<OrderDetailDTO> orderDetails) {
        super(id, orderTime, destinationAddress, completed, idUser);
        this.orderDetails = orderDetails;
    }

    // public static class ExtendedOrderDTOBuilder extends OrderDTOBuilder {
    //     private List<OrderDetailDTO> orderDetails;
    //
    //     public ExtendedOrderDTOBuilder id(UUID id) {
    //         super.id(id);
    //         return this;
    //     }
    //
    //     public ExtendedOrderDTOBuilder orderTime(Date orderTime) {
    //         super.orderTime(orderTime);
    //         return this;
    //     }
    //
    //     public ExtendedOrderDTOBuilder destinationAddress(String destinationAddress) {
    //         super.destinationAddress(destinationAddress);
    //         return this;
    //     }
    //
    //     public ExtendedOrderDTOBuilder completed(Boolean completed) {
    //         super.completed(completed);
    //         return this;
    //     }
    //
    //     public ExtendedOrderDTOBuilder idUser(UUID idUser) {
    //         super.idUser(idUser);
    //         return this;
    //     }
    //
    //     public ExtendedOrderDTOBuilder orderDetails(List<OrderDetailDTO> orderDetails) {
    //         this.orderDetails = orderDetails;
    //         return this;
    //     }
    //
    //     public ExtendedOrderDTO build() {
    //         super.build();
    //         return new ExtendedOrderDTO(orderDetails);
    //     }
    // }
}
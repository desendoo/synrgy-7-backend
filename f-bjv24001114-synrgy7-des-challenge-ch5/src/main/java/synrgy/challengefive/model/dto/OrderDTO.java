package synrgy.challengefive.model.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
@Builder
public class OrderDTO {

    private UUID id;
    private Date orderTime;
    private String destinationAddress;
    private Boolean completed;
    private UUID idUser;

}
package synrgy.challengefive.error.exception;

import lombok.Getter;
import lombok.Setter;
import synrgy.challengefive.error.ExtendedRuntimeException;
import synrgy.challengefive.model.response.FileUploadResponse;
import synrgy.challengefive.model.standard.HttpMessage;

@Setter
@Getter
public class FileStorageException extends ExtendedRuntimeException {

    FileUploadResponse fileUploadResponse;

    public FileStorageException() {
        super(HttpMessage.FILE_STORAGE_ERROR);
    }

    public FileStorageException(FileUploadResponse fileUploadResponse, String message) {
        super(HttpMessage.FILE_STORAGE_ERROR, message);
        this.fileUploadResponse = fileUploadResponse;
    }

    public FileStorageException(String message) {
        super(HttpMessage.FILE_STORAGE_ERROR, message);
    }
    public FileStorageException(Throwable cause) {
        super(HttpMessage.FILE_STORAGE_ERROR);
    }

    public FileStorageException(String message, Throwable cause) {
        super(HttpMessage.FILE_STORAGE_ERROR, message);
    }

}
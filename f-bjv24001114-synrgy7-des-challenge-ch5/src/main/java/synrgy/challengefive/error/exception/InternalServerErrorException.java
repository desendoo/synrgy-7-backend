package synrgy.challengefive.error.exception;

import synrgy.challengefive.error.ExtendedRuntimeException;
import synrgy.challengefive.model.standard.HttpMessage;

public class InternalServerErrorException extends ExtendedRuntimeException {

    public InternalServerErrorException() {
        super(HttpMessage.INTERNAL_SERVER_ERROR);
    }

    public InternalServerErrorException(String message) {
        super(HttpMessage.INTERNAL_SERVER_ERROR, message);
    }

}
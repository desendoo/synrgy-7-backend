package synrgy.challengefive.error.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import synrgy.challengefive.error.exception.InternalServerErrorException;
import synrgy.challengefive.model.standard.StandardResponseModel;

@RestControllerAdvice
public class MerchantExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(MerchantExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    public ResponseEntity<StandardResponseModel> handleException(Exception e) {
        StandardResponseModel response = new StandardResponseModel();
        response.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), e);
        logger.error(e.getMessage(), e);

        return response.toResponseEntity();
    }
    @ExceptionHandler(InternalServerErrorException.class)
    public ResponseEntity<StandardResponseModel> handleInternalServerErrorException(InternalServerErrorException e) {
        StandardResponseModel response = new StandardResponseModel();
        response.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), e);
        logger.error(e.getMessage(), e);

        return response.toResponseEntity();
    }


}

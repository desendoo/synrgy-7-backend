package synrgy.challengefive.error.handler;

import jakarta.validation.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import synrgy.challengefive.error.exception.InternalServerErrorException;
import synrgy.challengefive.model.standard.StandardResponseModel;

@ControllerAdvice
// @RestControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<StandardResponseModel> handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
        StandardResponseModel response = new StandardResponseModel();
        response.error(HttpStatus.BAD_REQUEST.value(), e);

        return response.toResponseEntity();
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<StandardResponseModel> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e) {
        StandardResponseModel response = new StandardResponseModel();
        response.error(HttpStatus.BAD_REQUEST.value(), e);

        return response.toResponseEntity();
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<StandardResponseModel> ConstraintViolationException(ConstraintViolationException e) {
        StandardResponseModel response = new StandardResponseModel();
        // extract the violations
        e.getConstraintViolations().forEach(violation -> {
            response.putError(violation.getPropertyPath().toString(), violation.getMessage());
        });
        response.setStatusCode(HttpStatus.BAD_REQUEST.value());

        return response.toResponseEntity();
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<StandardResponseModel> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        StandardResponseModel response = new StandardResponseModel();

        e.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();

            response.putError(fieldName, errorMessage);
        });

        response.setStatusCode(400);
        response.setStatusCode(HttpStatus.BAD_REQUEST.value());

        return response.toResponseEntity();
    }

    @ExceptionHandler(InternalServerErrorException.class)
    public ResponseEntity<StandardResponseModel> handleInternalServerErrorException(InternalServerErrorException e) {
        StandardResponseModel response = new StandardResponseModel();
        response.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), e);

        return response.toResponseEntity();
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<StandardResponseModel> handleRuntimeException(RuntimeException e) {
        StandardResponseModel response = new StandardResponseModel();
        response.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), e);

        return response.toResponseEntity();
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<StandardResponseModel> handleException(Exception e) {
        StandardResponseModel response = new StandardResponseModel();
        response.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), e);

        return response.toResponseEntity();
    }

}
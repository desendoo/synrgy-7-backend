package synrgy.challengefive.error.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import synrgy.challengefive.error.exception.FileStorageException;
import synrgy.challengefive.model.standard.HttpMessage;
import synrgy.challengefive.model.standard.StandardResponseModel;

@RestControllerAdvice
public class FileExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(FileExceptionHandler.class);

    @ExceptionHandler(FileStorageException.class)
    public ResponseEntity<StandardResponseModel> handleFileStorageException(FileStorageException e) {
        StandardResponseModel response = new StandardResponseModel();
        response.error(HttpMessage.FILE_STORAGE_ERROR.getHttpStatusCode(), e);
        response.putError("FILE_STORAGE_ERROR", e.getFileUploadResponse());

        logger.error(e.getMessage(), e);

        return response.toResponseEntity();
    }

}
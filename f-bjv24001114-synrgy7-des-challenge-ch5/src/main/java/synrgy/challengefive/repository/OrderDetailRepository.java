package synrgy.challengefive.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import synrgy.challengefive.model.entity.OrderDetail;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetail, UUID>, JpaSpecificationExecutor<OrderDetail> {

    @Query(
            value = "SELECT * FROM order_detail WHERE id_order = ?1",
            nativeQuery = true
    )
    List<OrderDetail> findByIdOrder(UUID idOrder);

    /*
    // function to get order detail by id with the following data structure as response
                {
                    "id": "ec0200e8-0832-4e3e-91c5-949a12cc7510",
                    "quantity": 5,
                    "totalPrice": 5,
                    "product": {
                        "id": "551d28ed-fec9-4be0-a9c6-b38ebd85c582",
                        "name": "Test Food",
                        "price": 1,
                        "idMerchant": "2d654209-086e-4697-81a1-ae00d23734fd"
                    }
                }
    * */
    // @Query(value = "SELECT new map(od.id as id, od.quantity as quantity, od.totalPrice as totalPrice, " +
    //         "new map(p.id as id, p.name as name, p.price as price, p.idMerchant.id as idMerchant) as product) " +
    //         "FROM OrderDetail od JOIN od.idProduct p WHERE od.id = ?1")
    // Object findOrderDetailWithProductById(UUID id);

}
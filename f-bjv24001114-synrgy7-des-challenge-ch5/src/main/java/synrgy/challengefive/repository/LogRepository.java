package synrgy.challengefive.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import synrgy.challengefive.model.entity.Log;

public interface LogRepository extends JpaRepository<Log, Long> {
}
package synrgy.challengefive.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import synrgy.challengefive.model.entity.Merchant;

import java.util.UUID;

@Repository
public interface MerchantRepository extends JpaRepository<Merchant, UUID>, JpaSpecificationExecutor<Merchant> {
    // Merchant findFirstByOrderByCreatedDateDesc();
    // native query to find the first merchant by created date
    @Query(value = "SELECT * FROM merchant ORDER BY created_date DESC LIMIT 1", nativeQuery = true)
    Merchant findFirstByOrderByCreatedDateDesc();
}

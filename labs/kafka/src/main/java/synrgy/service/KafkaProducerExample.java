package synrgy.service;

// import org.apache.kafka.clients.producer.KafkaProducer;
// import org.apache.kafka.clients.producer.ProducerRecord;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.kafka.core.KafkaTemplate;
// import org.springframework.stereotype.Service;
//
// import java.util.Properties;
//
// @Service
// public class KafkaProducerExample {
//
//     private static final String TOPIC = "my_topic";
//     String topic = "my-topic";
//     String message = "Hello, Kafka!";
//
//     @Autowired
//     private KafkaTemplate<String, String> kafkaTemplate;
//
//     public KafkaProducerExample() {
//     }
//
//     public KafkaProducerExample(KafkaTemplate<String, String> kafkaTemplate) {
//         this.kafkaTemplate = kafkaTemplate;
//     }
//
//     public KafkaProducerExample(String topic, String message) {
//         this.topic = topic;
//         this.message = message;
//     }
//
//     public void writeMessage(String msg) {
//         this.kafkaTemplate.send(TOPIC, msg);
//     }
//
//     public void send() {
//         Properties props = new Properties();
//         props.put("bootstrap.servers", "localhost:9092,localhost:9093,localhost:9094");
//         props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
//         props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
//         KafkaProducer<String, String> producer = new KafkaProducer<>(props);
//
//         ProducerRecord<String, String> record = new ProducerRecord<>(topic, message);
//
//         producer.send(record);
//         System.out.println("topik saya : " + topic + "\npesan saya : " +  message);
//     }
//
// }

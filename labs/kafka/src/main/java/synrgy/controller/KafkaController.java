package synrgy.controller;

// import org.apache.kafka.clients.producer.KafkaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
// import synrgy.service.KafkaProducerExample;
import synrgy.service.KafkaProducerService;

import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/kafka")
public class KafkaController {

    // private final KafkaProducerExample producer;
    //
    // public KafkaController(KafkaProducerExample producer) {
    //     this.producer = producer;
    // }

    @Autowired
    private KafkaProducerService kafkaProducerService;

    // POST /kafka/createTopic
    // Parameters:
    // - topicName: my_topic
    // - partitions: 3
    // - replicationFactor: 1
    @PostMapping("/createTopic")
    public String createTopic(@RequestParam String topicName, @RequestParam int partitions, @RequestParam short replicationFactor) {
        kafkaProducerService.createTopic(topicName, partitions, replicationFactor);
        return "Topic created successfully with name: " + topicName + ", partitions: " + partitions + ", replication factor: " + replicationFactor;
    }

    // POST /kafka/sendMessage
    // Parameters:
    // - topic: my_topic
    // - message: Hello Kafka
    @PostMapping("/sendMessage")
    public String sendMessage(@RequestParam String topic, @RequestParam String message) {
        kafkaProducerService.sendMessage(topic, message);
        return "Message sent successfully to topic: " + topic + ", message: " + message;
    }

    // @PostMapping("/publish")
    // public void writeMessageToTopic(@RequestParam("message") String message){
    //     // ProducerRecord<String, String> record = new ProducerRecord<>("your-topic-name", message);
    //     this.producer.writeMessage(message);
    // }
    //
    // @PostMapping("/publish/dynamic")
    // public void writeMessageToTopic2(@RequestParam("topic") String topic, @RequestParam("message") String message){
    //     KafkaProducerExample prod = new KafkaProducerExample(topic, message);
    //     prod.send();
    // }


}

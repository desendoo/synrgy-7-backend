package synrgy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SynrgyLabsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SynrgyLabsApplication.class, args);
	}

}

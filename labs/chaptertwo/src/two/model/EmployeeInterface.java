package two.model;

public interface EmployeeInterface {

    public void updateName(String name);

    public void updateIdNumber(String idNumber);

    public void updateUserRole(USER_ROLE userRole);

    public void updateEmployeeType(EMPLOYEE_TYPE employeeType);

}

package two.model;

import lombok.Data;

@Data
public abstract class EmployeeAbstract implements EmployeeInterface {

    private String name;
    private String idNumber;
    private USER_ROLE userRole;
    private EMPLOYEE_TYPE employeeType;

    public EmployeeAbstract() {
    }

    public EmployeeAbstract(String name, String idNumber) {
        this.name = name;
        this.idNumber = idNumber;
    }

    public EmployeeAbstract(String name, String idNumber, USER_ROLE userRole) {
        this.name = name;
        this.idNumber = idNumber;
        this.userRole = userRole;
    }

    public EmployeeAbstract(String name, String idNumber, USER_ROLE userRole, EMPLOYEE_TYPE employeeType) {
        this.name = name;
        this.idNumber = idNumber;
        this.userRole = userRole;
        this.employeeType = employeeType;
    }

    public abstract double calculateEarnings();

    @Override
    public void updateName(String name) {
        this.name = name;
    }

    @Override
    public void updateIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    @Override
    public void updateUserRole(USER_ROLE userRole) {
        this.userRole = userRole;
    }

    @Override
    public void updateEmployeeType(EMPLOYEE_TYPE employeeType) {
        this.employeeType = employeeType;
    }

}



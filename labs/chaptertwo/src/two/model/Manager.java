package two.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class Manager extends Employee implements ManagerInterface {

    private List<Employee> managedEmployees;
    private static final float BONUS = 0.1f;

    public Manager(String name, String idNumber, USER_ROLE userRole, EMPLOYEE_TYPE employeeType, double hoursWorked, double hourlyRate) {
        super(name, idNumber, userRole, employeeType, hoursWorked, hourlyRate);
    }

    @Override
    public void addManagedEmployee(Employee employee) {
        if (managedEmployees == null || managedEmployees.isEmpty()) {
            managedEmployees = new ArrayList<>();
        }
        this.managedEmployees.add(employee);
    }

    @Override
    public double calculateEarnings() {
        double baseEarnings = super.calculateEarnings();
        double bonusEarnings = baseEarnings * BONUS * managedEmployees.size();

        return baseEarnings + bonusEarnings;
    }

}

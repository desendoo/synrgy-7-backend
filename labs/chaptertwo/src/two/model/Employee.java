package two.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class Employee extends EmployeeAbstract {

    private double hoursWorked;
    private double hourlyRate;

    public Employee() {
        super();
    }

    public Employee(String name, String idNumber) {
        super(name, idNumber);
    }

    public Employee(String name, String idNumber, USER_ROLE userRole, EMPLOYEE_TYPE employeeType, double hoursWorked, double hourlyRate) {
        super(name, idNumber, userRole, employeeType);
        this.hoursWorked = hoursWorked;
        this.hourlyRate = hourlyRate;
    }

    @Override
    public double calculateEarnings() {
        return this.hoursWorked * this.hourlyRate;
    }

}

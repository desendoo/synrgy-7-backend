package two.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class ManagerRepository {

    private List<Manager> managers;
    private Map<String, List<String>> managedEmployees;

    public ManagerRepository() {
        this.managers = new ArrayList<>();
        this.managedEmployees = new HashMap<>();
    }

    public void addManagedEmployee(String managerId, String employeeId) {
        if (managedEmployees.get(managerId) == null || managedEmployees.get(managerId).isEmpty()) {
            List<String> employeeIds = new ArrayList<>();
            employeeIds.add(employeeId);

            managedEmployees.put(managerId, employeeIds);
        } else if (managedEmployees.get(managerId) != null || !managedEmployees.get(managerId).isEmpty()){
            managedEmployees.get(managerId).add(employeeId);
        }
    }

    public List<String> getManagedEmployeesByManagerId(String managerId) {
        return managedEmployees.get(managerId);
    }

    public void addManager(Manager manager) {
        this.managers.add(manager);
    }

    public Manager getManagerById(String id) {
        for (Manager manager : managers) {
            if (manager.getIdNumber().equals(id)) {
                return manager;
            }
        }
        return null;
    }

    public void deleteManagerById(String id) {
        for (Manager manager : managers) {
            if (manager.getIdNumber().equals(id)) {
                managers.remove(manager);
                return;
            }
        }
    }

    public void updateManagerById(Manager manager, String id) {
        for (Manager mng : managers) {
            if (mng.getIdNumber().equals(id)) {
                mng.setName(manager.getName());
                mng.setIdNumber(manager.getIdNumber());
                mng.setUserRole(manager.getUserRole());
                mng.setEmployeeType(manager.getEmployeeType());
                mng.setHoursWorked(manager.getHoursWorked());
                mng.setHourlyRate(manager.getHourlyRate());
                break;
            }
        }
    }

}

package two.model;

public enum EMPLOYEE_TYPE {
    SOFTWARE_ENGINEER,
    DATA_SCIENTIST,
    MANAGER,
    PRODUCT_MANAGER,
    DESIGNER,
    SALES_BUSINESS_DEVELOPMENT,
    MARKETING_COMMUNICATIONS,
    OPERATIONS_HR,
    FINANCE_ACCOUNTING,
    CUSTOMER_SUPPORT_SUCCESS,
    LEGAL_COMPLIANCE
}
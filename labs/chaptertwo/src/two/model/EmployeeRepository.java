package two.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class EmployeeRepository {

    private List<Employee> employees;

    public EmployeeRepository() {
        this.employees = new ArrayList<>();
    }

    public void addEmployee(Employee employee) {
        this.employees.add(employee);
    }

    public Employee getEmployeeById(String id) {
        for (Employee employee : employees) {
            if (employee.getIdNumber().equals(id)) {
                return employee;
            }
        }
        return null;
    }

    public void deleteEmployeeById(String id) {
        for (Employee employee : employees) {
            if (employee.getIdNumber().equals(id)) {
                employees.remove(employee);
                return;
            }
        }
    }

    public void updateEmployeeById(Employee employee, String id) {
        for (Employee emp : employees) {
            if (emp.getIdNumber().equals(id)) {
                emp.setName(employee.getName());
                emp.setIdNumber(employee.getIdNumber());
                emp.setUserRole(employee.getUserRole());
                emp.setEmployeeType(employee.getEmployeeType());
                emp.setHoursWorked(employee.getHoursWorked());
                emp.setHourlyRate(employee.getHourlyRate());
                break;
            }
        }
    }

}

package two.controller;

import two.model.Employee;
import two.model.EmployeeRepository;
import two.view.EmployeeView;

import java.util.List;

public class EmployeeController {

    private EmployeeRepository employeeRepository;
    private EmployeeView employeeView;

    public EmployeeController(EmployeeRepository employeeRepository, EmployeeView employeeView) {
        this.employeeRepository = employeeRepository;
        this.employeeView = employeeView;
    }

    public void displayEmployees() {
        List<Employee> employees = employeeRepository.getEmployees();
        for (Employee employee : employees) {
            employeeView.printDetails(employee);
        }
    }

    public void addEmployee(Employee employee) {
        employeeRepository.addEmployee(employee);
    }

    public List<Employee> getEmployees() {
        return employeeRepository.getEmployees();
    }

    public Employee getEmployeeById(String id) {
        return employeeRepository.getEmployeeById(id);
    }

    public void deleteEmployeeById(String id) {
        employeeRepository.deleteEmployeeById(id);
    }

    public void updateEmployeeById(Employee employee, String id) {
        employeeRepository.updateEmployeeById(employee, id);
    }

}

package two.controller;

import two.model.ManagerRepository;
import two.view.ManagerView;

import java.util.List;

public class ManagerController {

    private ManagerRepository managerRepository;
    private ManagerView managerView;

    public ManagerController(ManagerRepository managerRepository, ManagerView managerView) {
        this.managerRepository = managerRepository;
        this.managerView = managerView;
    }

    public void addManagedEmployee(String managerId, String employeeId) {
        managerRepository.addManagedEmployee(managerId, employeeId);
    }

    public List<String> getManagedEmployeesByManagerId(String managerId) {
        return managerRepository.getManagedEmployeesByManagerId(managerId);
    }

}

package two.view;

import two.model.Employee;
import two.model.Manager;

public class ManagerView {

    private static final EmployeeView employeeView = new EmployeeView();

    public void printDetails(Manager manager) {
        System.out.printf("[ID: %s][%s] Name: %s, Earnings: %.2f%n", manager.getIdNumber(), manager.getEmployeeType(), manager.getName(), manager.calculateEarnings());
        for (Employee employee : manager.getManagedEmployees()) {
            System.out.print("=> Managed Employee - ");
            employeeView.printDetails(employee);
        }
    }

}

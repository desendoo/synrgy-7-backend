package two.view;

import two.model.Employee;

public class EmployeeView {

    public void printDetails(Employee employee) {
        System.out.printf("[ID: %s][%s] Name: %s, Earnings: %.2f%n", employee.getIdNumber(), employee.getEmployeeType(), employee.getName(), employee.calculateEarnings());
    }

}

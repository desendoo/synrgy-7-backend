package two;

import lombok.Data;

@Data
public abstract class Vehicle {

    private String brand;
    private Integer year;

    // Constructor
    public Vehicle(String brand, int year) {
        this.brand = brand;
        this.year = year;
    }

    // Abstract method untuk menghitung biaya servis kendaraan
    abstract double calculateServiceCost();

}

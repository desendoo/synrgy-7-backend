import five.TrowsWithObjectResponse;
import one.Car;
import two.controller.EmployeeController;
import two.model.*;
import two.view.EmployeeView;
import two.view.ManagerView;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.*;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {

    public static void main(String[] args) {
//        test1();
//        test2();
//        test3();
//        test4();
//        testExceptionThrowed();
//        try {
//            testExceptionThrows();
//        } catch (Exception e) {
//            printErrorDetails(e);
//        }
        employeeManagement();
    }

    public static void employeeManagement() {
        EmployeeRepository employeeRepository = new EmployeeRepository();
        EmployeeView employeeView = new EmployeeView();
        ManagerView managerView = new ManagerView();
        EmployeeController employeeController = new EmployeeController(employeeRepository, employeeView);

        employeeController.addEmployee(new Employee("John Doe", "123456", USER_ROLE.USER, EMPLOYEE_TYPE.SOFTWARE_ENGINEER, 48, 150));
        employeeController.addEmployee(new Employee("Will Smith", "312321", USER_ROLE.USER, EMPLOYEE_TYPE.DATA_SCIENTIST, 40, 120));

        employeeController.displayEmployees();

        Manager manager = new Manager("Iron Man", "654321", USER_ROLE.ADMIN, EMPLOYEE_TYPE.MANAGER, 48, 200);

        employeeController.getEmployees().forEach(employee -> manager.addManagedEmployee(employee));
        managerView.printDetails(manager);
    }

    static void testThrowWithObjectResponse() {
        TrowsWithObjectResponse processor = new TrowsWithObjectResponse();
        try {
            processor.processPayment(-100); // Ini akan melemparkan PaymentException
            System.out.println("Pembayaran berhasil diproses");
        } catch (TrowsWithObjectResponse.PaymentException e) {
            System.out.println("Terjadi kesalahan dalam pemrosesan pembayaran: " + e.getMessage());
            TrowsWithObjectResponse.PaymentResponse response = e.getResponse();
            System.out.println("Status pembayaran: " + (response.isSuccess() ? "Berhasil" : "Gagal"));
            System.out.println("Pesan kesalahan: " + response.getErrorMessage());
            // Lakukan penanganan yang sesuai, misalnya memberikan pesan kesalahan kepada pengguna
        }
    }

    static void testExceptionThrows() throws Exception {
        BufferedWriter bw = new BufferedWriter(new FileWriter("myFile.txt"));
        bw.write("Test");
        bw.close();
    }

    static void testExceptionThrowed() {
        try {
//            double x = 3/0;
            System.out.println("EXECUTED");
            throw new ArithmeticException();
        } catch (ArithmeticException e) {
            printErrorDetails(e);
        }
    }

    static void test4() {
        // NullPointerException example
        String text = null;
        try {
            System.out.println(text.length());
        } catch (NullPointerException e) {
            printErrorDetails(e);
        }

        // ArrayIndexOutOfBoundsException example
        int[] numbers = {1, 2, 3};
        try {
            System.out.println(numbers[3]);
        } catch (ArrayIndexOutOfBoundsException e) {
            printErrorDetails(e);
        }

        // Scanner stream open and close example in try-catch-finally
        String predefinedInput = "123\n"; // Predefined number
        InputStream in = new ByteArrayInputStream(predefinedInput.getBytes());
        System.setIn(in);
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.print("Enter a number: ");
            int number = scanner.nextInt();
            System.out.println("You entered: " + number);
        } catch (InputMismatchException e) {
            printErrorDetails(e);
        } finally {
            scanner.close();
        }
    }

    static void printErrorDetails(Exception e) {
        System.out.println("[" + e.getClass().getCanonicalName() + "] occurred because of \"" + e.getMessage() + "\"");
    }

    static void test3() {
        EmployeeView employeeView = new EmployeeView();
        ManagerView managerView = new ManagerView();

        Employee employeeDev = new Employee("John Doe", "123456", USER_ROLE.USER, EMPLOYEE_TYPE.SOFTWARE_ENGINEER, 48, 150);
        Employee employeeData = new Employee("Will Smith", "312321", USER_ROLE.USER, EMPLOYEE_TYPE.DATA_SCIENTIST, 40, 120);

        employeeView.printDetails(employeeDev);
        employeeView.printDetails(employeeData);

        Manager manager = new Manager("Iron Man", "654321", USER_ROLE.ADMIN, EMPLOYEE_TYPE.MANAGER, 48, 200);

        manager.addManagedEmployee(employeeDev);
        manager.addManagedEmployee(employeeData);
        managerView.printDetails(manager);
    }

    static void test2() {
        Map<Integer, String> menuSatu = new HashMap<>();
        Map<Integer, String> menuDua = new HashMap<>();
        menuSatu.put(1, "nasi goreng");
        menuDua.put(2, "bakmi goreng");
        Map<Integer, String> daftarMenu = new HashMap<>();
        daftarMenu.put(1, "nasi goreng");
        daftarMenu.put(2, "bakmi goreng");

        Set<Map<Integer, String>> setMenu = new HashSet<>();

        daftarMenu.forEach((key, value) -> {
            System.out.println(key + ". " + value);
        });
        System.out.println("=====");

//        setMenu.f
    }

    static void test1() {
        Map<Integer, Car> test = new HashMap<>();
        Car corolla = new Car(1, "Toyota", "Corolla", 1966);
        Set<Car> cars = new HashSet<>();
        cars.add(corolla);
        cars.add(new Car(2, "Honda", "Civic", 2000));
        cars.add(new Car(3, "Ford", "Mustang", 2015));
        cars.add(new Car(4, "BMW", "3 Series", 2020));
        cars.add(new Car(5, "Audi", "A4", 2018));

        for (Car car : cars) {
            System.out.println(car.getIdCar() + ". " + car.getBrand() + " | " + car.getModel() + " | " + car.getYear());
        }
        System.out.println("=====\n");

        corolla.setModel("Corolla Reborn");

        for (Car car : cars) {
            System.out.println(car.getIdCar() + ". " + car.getBrand() + " | " + car.getModel() + " | " + car.getYear());
        }
        System.out.println("=====\n");

        Car mobilSatu = new Car(1, "Toyota", "Corolla", 1966);
        cars.add(mobilSatu);
        cars.add(corolla);
        cars.add(corolla);
        cars.add(corolla);

        for (Car car : cars) {
            System.out.println(car.getIdCar() + ". " + car.getBrand() + " | " + car.getModel() + " | " + car.getYear());
        }
        System.out.println("=====\n");

        corolla.setYear(2024);
//        cars.add(corolla);
        cars.remove(corolla);

        for (Car car : cars) {
            System.out.println(car.getIdCar() + ". " + car.getBrand() + " | " + car.getModel() + " | " + car.getYear());
        }
        System.out.println("=====\n");

//        mobilSatu.startEngine();
//        mobilSatu.stopEngine();
//
//        mobilSatu.setDistance(200.0);
//        mobilSatu.setTime(20);
//
//        System.out.println(mobilSatu.getBrand());
//        System.out.println(mobilSatu.speed());
    }

}
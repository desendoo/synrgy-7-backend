package one;

import lombok.Data;

@Data
public class Car {

    private int idCar;
    private String brand;
    private String model;
    private int year;
    private Double distance;
    private Integer time;

    public Car(int idCar, String brand, String model, int year) {
        this.idCar = idCar;
        this.brand = brand;
        this.model = model;
        this.year = year;
    }

    public Integer speed() {
        return (int) (this.distance/this.time);
    }

    public void startEngine() {
        System.out.println(brand + " " + model  + " " +  String.valueOf(year) + " Engine started");
    }

    public void stopEngine() {
        System.out.println(brand + " " + model + " " + String.valueOf(year) + " Engine stopped");
    }

}
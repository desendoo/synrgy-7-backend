package synrgy.springsecurity;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
// @SpringBootTest
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("/application-test.properties")
class TestingLoginController {

    @LocalServerPort
    private int PORT;
    private static TestRestTemplate restTemplate;
    @Value("${baseurl:}") // FILE_SHOW_RUL
    private String BASEURL;
    @Value("${baseurltest:}") // FILE_SHOW_RUL
    private String BASEURLTEST;

    @BeforeAll
    public static void setUp() {
        restTemplate = new TestRestTemplate();
    }

    @Test
    void restTemplateLogin() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("username", "admin@mail.com");
        map.add("password", "password");
        map.add("grant_type", "password");
        map.add("client_id", "my-client-web");
        map.add("client_secret", "password");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

        ResponseEntity<String> response = restTemplate.postForEntity(BASEURLTEST + ":" + PORT + "/api/oauth/token",
        // ResponseEntity<String> response = restTemplate.postForEntity(BASEURL + "/api/oauth/token",
                request,
                String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        System.out.println("response  =" + response.getBody());
        System.out.println("");
    }

    // @Test
    // void restTemplateLoginPlainUsernamePassword() throws Exception {
    //     HttpHeaders headers = new HttpHeaders();
    //     headers.set("Accept", "*/*");
    //     headers.set("Content-Type", "application/json");
    //     String bodyTesting = "{\n" +
    //             "    \"username\":\"user@mail.com\",\n" +
    //             "    \"password\":\"password\"\n" +
    //             "}";
    //     HttpEntity<String> entity = new HttpEntity<String>(bodyTesting, headers);
    //
    //     ResponseEntity<String> exchange = restTemplate.exchange(BASEURLTEST + ":" + PORT + "/api/v1/user-login/login",
    //     // ResponseEntity<String> exchange = restTemplate.exchange(BASEURL + "/api/v1/user-login/login",
    //             HttpMethod.POST,
    //             entity,
    //             String.class);
    //
    //     assertEquals(HttpStatus.OK, exchange.getStatusCode());
    //     System.out.println("response  =" + exchange.getBody());
    //     System.out.println("");
    // }


}
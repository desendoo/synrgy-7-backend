package synrgy.springsecurity.repository.oauth;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import synrgy.springsecurity.model.entity.oauth.UserOauth;

public interface UserOauthRepository extends JpaRepository<UserOauth, Long>, PagingAndSortingRepository<UserOauth,
        Long> {

    @Query("FROM UserOauth u WHERE LOWER(u.username) = LOWER(?1)")
    UserOauth findOneByUsername(String username);

    @Query("FROM UserOauth u WHERE u.otp = ?1")
    UserOauth findOneByOTP(String otp);

    @Query("FROM UserOauth u WHERE LOWER(u.username) = LOWER(:username)")
    UserOauth checkExistingEmail(String username);

}
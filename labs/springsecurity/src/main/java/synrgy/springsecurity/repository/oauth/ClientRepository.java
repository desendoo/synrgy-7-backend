package synrgy.springsecurity.repository.oauth;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import synrgy.springsecurity.model.entity.oauth.Client;

public interface ClientRepository extends JpaRepository<Client, Long>, PagingAndSortingRepository<Client, Long> {

    Client findOneByClientId(String clientId);

}
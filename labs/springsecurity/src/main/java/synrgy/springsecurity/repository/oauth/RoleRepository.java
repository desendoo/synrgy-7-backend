package synrgy.springsecurity.repository.oauth;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import synrgy.springsecurity.model.entity.oauth.Role;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Long>, PagingAndSortingRepository<Role, Long> {

    Role findOneByName(String name);

    List<Role> findByNameIn(String[] names);

}
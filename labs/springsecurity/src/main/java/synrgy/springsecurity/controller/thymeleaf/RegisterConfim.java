package synrgy.springsecurity.controller.thymeleaf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import synrgy.springsecurity.model.entity.oauth.UserOauth;
import synrgy.springsecurity.repository.oauth.UserOauthRepository;
import synrgy.springsecurity.security.Config;

import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/v1/user-register/web/")
public class RegisterConfim {

    @Autowired
    public UserOauthRepository userRepo;

    Config config = new Config();

    @RequestMapping(method = RequestMethod.GET, value = {"/index/{tokenotp}"})
    public String index(Model model, @PathVariable String tokenotp) {
        UserOauth user = userRepo.findOneByOTP(tokenotp);
        if (user == null) {
            System.out.println("user null: tidak ditemukan");
            model.addAttribute("errordesc", "User not found for code " + tokenotp);
            model.addAttribute("title", "");
            return "register";
        }
        if (user.isEnabled()) {
            model.addAttribute("errordesc", "Akun Anda sudah aktif, Silahkan melakukan login ");
            model.addAttribute("title", "");
            return "register";
        }

        String today = convertDateToString(new Date());

        String dateToken = config.convertDateToString(user.getOtpExpiredDate());
        if (Long.parseLong(today) > Long.parseLong(dateToken)) {
            model.addAttribute("errordesc", "Your token is expired. Please Get token again.");
            model.addAttribute("title", "");
            return "register";
        }
        user.setEnabled(true);
        userRepo.save(user);
        model.addAttribute("title", "Congratulations, " + user.getUsername() + ", you have successfully registered.");
        model.addAttribute("errordesc", "");
        return "register";
    }

    public String convertDateToString(Date date) {

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String strDate = dateFormat.format(date);
        // System.out.println("Date: " + strDate);
        return strDate;
    }
}

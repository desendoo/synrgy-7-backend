package synrgy.springsecurity.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import synrgy.springsecurity.model.response.TemplateResponse;
import synrgy.springsecurity.service.oauth.UserOauthService;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/v1/user")
public class UserController {

    @Autowired
    public TemplateResponse templateCRUD;

    @Autowired
    public UserOauthService userOauthService;

    // @RequestMapping(method = RequestMethod.GET, value = "/user/public")
    @GetMapping("/public")
    public ResponseEntity<Map> index() {
        Map<String, Object> content = new HashMap<>();
        content.put("user", "Example User");

        return ResponseEntity
                .status(200)
                .body(content);
    }

    // @RequestMapping(method = RequestMethod.GET, value = "/user/protected")
    @GetMapping("/protected")
    public ResponseEntity<Map> user(Principal principal) {
        Map success = userOauthService.getDetailProfile(principal);

        return ResponseEntity
                .status(200)
                .body(success);
    }

}

package synrgy.springsecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import synrgy.springsecurity.model.entity.oauth.UserOauth;
import synrgy.springsecurity.model.request.ResetPasswordModel;
import synrgy.springsecurity.model.response.TemplateResponse;
import synrgy.springsecurity.repository.oauth.UserOauthRepository;
import synrgy.springsecurity.security.Config;
import synrgy.springsecurity.service.oauth.UserOauthService;
import synrgy.springsecurity.utils.EmailSender;
import synrgy.springsecurity.utils.EmailTemplate;
import synrgy.springsecurity.utils.SimpleStringUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/v1/forget-password/")
public class ForgetPasswordController {

    @Autowired
    public UserOauthService serviceReq;
    @Autowired
    public TemplateResponse templateCRUD;
    @Autowired
    public EmailTemplate emailTemplate;
    @Autowired
    public EmailSender emailSender;
    Config config = new Config();
    @Autowired
    private UserOauthRepository userRepository;
    @Value("${expired.token.password.minute:}")// FILE_SHOW_RUL
    private int expiredToken;
    @Autowired
    private PasswordEncoder passwordEncoder;

    // Step 1 : Send OTP
    @PostMapping("/send")// send OTP//send OTP
    public Map sendEmailPassword(@RequestBody ResetPasswordModel user) {
        String message = "Thanks, please check your email";

        if (StringUtils.isEmpty(user.getEmail())) return templateCRUD.error("No email provided", 400);
        UserOauth found = userRepository.findOneByUsername(user.getEmail());
        if (found == null) return templateCRUD.error("Email not found", 400); // throw new BadRequest("Email not
        // found");

        String template = emailTemplate.getResetPassword();
        if (StringUtils.isEmpty(found.getOtp())) {
            UserOauth search;
            String otp;
            do {
                otp = SimpleStringUtils.randomString(6, true);
                search = userRepository.findOneByOTP(otp);
            } while (search != null);
            Date dateNow = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateNow);
            calendar.add(Calendar.MINUTE, expiredToken);
            Date expirationDate = calendar.getTime();

            found.setOtp(otp);
            found.setOtpExpiredDate(expirationDate);
            template = template.replaceAll("\\{\\{PASS_TOKEN}}", otp);
            template = template.replaceAll("\\{\\{USERNAME}}", (found.getUsername() == null ? "" +
                    "@UserName"
                    :
                    "@" + found.getUsername()));

            userRepository.save(found);
        } else {
            template = template.replaceAll("\\{\\{USERNAME}}", (found.getUsername() == null ? "" +
                    "@UserName"
                    :
                    "@" + found.getUsername()));
            template = template.replaceAll("\\{\\{PASS_TOKEN}}", found.getOtp());
        }
        emailSender.sendAsync(found.getUsername(), "Chute - Forget Password", template);


        return templateCRUD.success("success");

    }

    // Step 2 : CHek TOKEN OTP EMAIL
    @PostMapping("/validate")
    public Map cheKTOkenValid(@RequestBody ResetPasswordModel model) {
        if (model.getOtp() == null) return templateCRUD.error("Token is required", 400);

        UserOauth user = userRepository.findOneByOTP(model.getOtp());
        if (user == null) {
            return templateCRUD.error("Token not valid", 400);
        }

        return templateCRUD.success("Success");
    }

    // Step 3 : lakukan reset password baru
    @PostMapping("/change-password")
    public Map resetPassword(@RequestBody ResetPasswordModel model) {
        if (model.getOtp() == null) return templateCRUD.error("Token is required", 400);
        if (model.getNewPassword() == null) return templateCRUD.error("New Password is required", 400);
        UserOauth user = userRepository.findOneByOTP(model.getOtp());
        String success;
        if (user == null) return templateCRUD.error("Token not valid", 400);

        user.setPassword(passwordEncoder.encode(model.getNewPassword().replaceAll("\\s+", "")));
        user.setOtpExpiredDate(null);
        user.setOtp(null);

        try {
            userRepository.save(user);
            success = "success";
        } catch (Exception e) {
            return templateCRUD.error("Gagal simpan user", 400);
        }
        return templateCRUD.success(success);
    }

}
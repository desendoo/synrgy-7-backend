package synrgy.springsecurity.model.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class UserUpdateModel {
    private String username;
    private String password;
    private String fullname;
    private String email;
}

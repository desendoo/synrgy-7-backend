// package synrgy.springsecurity.error;
//
// import org.springframework.http.HttpStatus;
// import org.springframework.http.ResponseEntity;
// import org.springframework.validation.FieldError;
// import org.springframework.web.bind.MethodArgumentNotValidException;
// import org.springframework.web.bind.annotation.ExceptionHandler;
//
// import java.util.Map;
//
// public class GlobalExceptionHandler {
//
//     @ExceptionHandler(MethodArgumentNotValidException.class)
//     public ResponseEntity<Map> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
//         StandardResponseModel response = new StandardResponseModel();
//         e.getBindingResult().getAllErrors().forEach(error -> {
//             String fieldName = ((FieldError) error).getField();
//             String errorMessage = error.getDefaultMessage();
//
//             response.putError(fieldName, errorMessage);
//         });
//         logger.error(e.getMessage(), e);
//
//         return ResponseEntity
//                 .status(response.getStatusCode())
//                 .body(response);
//     }
//
// }

package synrgy.springsecurity.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity(securedEnabled = true) //secure definition
public class Oauth2ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    /**
     * Manage resource server.
     */
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        super.configure(resources);
    }
//    private static final String SECURED_PATTERN = "/api/**";
    /**
     * Manage endpoints.
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.cors()
                .and()
                .csrf()
                .disable()
                .antMatcher("/**")
                .authorizeRequests()
                .antMatchers(
                        "/",
                        "/api/auth/**",
                        "/actuator/**",
                        "/showFile/**",
                        "/swagger-ui/**",
                        "/swagger-ui.html",
                        "/v1/showFile/**",
                        "/v1/upload",
                        "/v1/user/public",
                        "/v1/user-register/**",
                        "/v1/user-login/**",
                        "/v1/forget-password/**",
                        "/v3/api-docs/**",
                        "/forget-password/**",
                        "/oauth/authorize**",
                        "/login**",
                        "/error**")
                .permitAll()
                .antMatchers("/v1/role-test-global/list-barang").hasAnyAuthority("ROLE_READ")
                .antMatchers("/v1/role-test-global/post-barang").hasAnyAuthority("ROLE_WRITE")
                .antMatchers("/v1/role-test-global/post-barang-user").hasAnyAuthority("ROLE_USER")
                .antMatchers("/v1/role-test-global/post-barang-admin").hasAnyAuthority("ROLE_ADMIN")
                .and()
                .authorizeRequests()
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .permitAll()
        ;
    }
}


// @Configuration
// @EnableMethodSecurity(securedEnabled = true)
// public class Oauth2ResourceServerConfiguration {
//
//     @Bean
//     public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
//         http.cors(cors -> cors.configure(http))
//                 .csrf(csrf -> csrf.disable())
//                 .authorizeHttpRequests(authorize -> authorize
//                         .requestMatchers("/**").permitAll()
//                         .requestMatchers("/", "/showFile/**", "/v1/showFile/**", "/v1/upload", "/user-register/**", "/swagger-ui/**", "/swagger-ui.html", "/v3/api-docs/**", "/user-login/**",
//                                 "/forget-password/**", "/oauth/authorize**", "/login**", "/error**")
//                         .permitAll()
//                         .requestMatchers("/v1/role-test-global/list-barang").hasAnyAuthority("ROLE_READ")
//                         .requestMatchers("/v1/role-test-global/post-barang").hasAnyAuthority("ROLE_WRITE")
//                         .requestMatchers("/v1/role-test-global/post-barang-user").hasAnyAuthority("ROLE_USER")
//                         .requestMatchers("/v1/role-test-global/post-barang-admin").hasAnyAuthority("ROLE_ADMIN")
//                         .anyRequest().authenticated()
//                 )
//                 .formLogin(formLogin -> formLogin.permitAll());
//         return http.build();
//     }
// }

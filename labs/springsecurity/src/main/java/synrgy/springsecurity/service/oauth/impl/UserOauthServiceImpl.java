package synrgy.springsecurity.service.oauth.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import synrgy.springsecurity.model.entity.oauth.Role;
import synrgy.springsecurity.model.entity.oauth.UserOauth;
import synrgy.springsecurity.model.request.RegisterModel;
import synrgy.springsecurity.model.response.TemplateResponse;
import synrgy.springsecurity.repository.oauth.RoleRepository;
import synrgy.springsecurity.repository.oauth.UserOauthRepository;
import synrgy.springsecurity.security.Config;
import synrgy.springsecurity.service.oauth.Oauth2UserDetailsService;
import synrgy.springsecurity.service.oauth.UserOauthService;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserOauthServiceImpl implements UserOauthService {

    Config config = new Config();
    private static final Logger logger = LoggerFactory.getLogger(UserOauthServiceImpl.class);
    @Autowired
    RoleRepository repoRole;

    @Autowired
    UserOauthRepository repoUser;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    public TemplateResponse templateResponse;

    @Autowired
    private Oauth2UserDetailsService oauth2UserDetailsService;

    @Override
    public Map registerManual(RegisterModel objModel) {
        Map map = new HashMap();
        try {
            String[] roleNames = {"ROLE_USER", "ROLE_READ", "ROLE_WRITE"}; // admin
            UserOauth user = new UserOauth();
            user.setUsername(objModel.getEmail().toLowerCase());
            user.setFullname(objModel.getFullname());

            //step 1 :
//            user.setEnabled(false); // matikan user

            String password = encoder.encode(objModel.getPassword().replaceAll("\\s+", ""));
            List<Role> r = repoRole.findByNameIn(roleNames);

            user.setRoles(r);
            user.setPassword(password);
            UserOauth obj = repoUser.save(user);

            return templateResponse.success(obj);

        } catch (Exception e) {
            logger.error("Error registerManual=", e);
            return templateResponse.error("error:"+e, 500);
        }

    }

    @Override
    public Map registerByGoogle(RegisterModel objModel) {
        Map map = new HashMap();
        try {
            String[] roleNames = {"ROLE_USER", "ROLE_READ", "ROLE_WRITE"}; // ROLE DEFAULE
            UserOauth user = new UserOauth();
            user.setUsername(objModel.getUsername().toLowerCase());
            user.setFullname(objModel.getFullname());
            //step 1 :
            user.setEnabled(false); // matikan user
            String password = encoder.encode(objModel.getPassword().replaceAll("\\s+", ""));
            List<Role> r = repoRole.findByNameIn(roleNames);
            user.setRoles(r);
            user.setPassword(password);
            UserOauth obj = repoUser.save(user);
            return templateResponse.success(obj);

        } catch (Exception e) {
            logger.error("Error registerManual=", e);
            return templateResponse.error("error:"+e, 500);
        }

    }

    @Override
    public Map getDetailProfile(Principal principal) {
        UserOauth idUser = getUserIdToken(principal, oauth2UserDetailsService);
        try {
           // User obj = userRepository.save(idUser);
            return templateResponse.success(idUser);
        } catch (Exception e){
            return templateResponse.error(e,"500");
        }
    }

    private UserOauth getUserIdToken(Principal principal, Oauth2UserDetailsService userDetailsService) {
        UserDetails user = null;
        String username = principal.getName();
        if (!StringUtils.isEmpty(username)) {
            user = userDetailsService.loadUserByUsername(username);
        }

        if (null == user) {
            throw new UsernameNotFoundException("User not found");
        }
        UserOauth idUser = repoUser.findOneByUsername(user.getUsername());
        if (null == idUser) {
            throw new UsernameNotFoundException("User name not found");
        }
        return idUser;
    }

}

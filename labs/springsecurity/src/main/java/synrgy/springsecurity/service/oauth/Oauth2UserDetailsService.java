package synrgy.springsecurity.service.oauth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import synrgy.springsecurity.model.entity.oauth.UserOauth;
import synrgy.springsecurity.repository.oauth.UserOauthRepository;

@Service
public class Oauth2UserDetailsService implements UserDetailsService {

    @Autowired
    private UserOauthRepository userOauthRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserOauth userOauth = userOauthRepository.findOneByUsername(s);
        if (null == userOauth) {
            throw new UsernameNotFoundException(String.format("Username %s is not found", s));
        }

        return userOauth;
    }

    @CacheEvict("oauth_username")
    public void clearCache(String s) {
    }
    
}
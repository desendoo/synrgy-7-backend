package synrgy.springsecurity.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import synrgy.springsecurity.model.entity.oauth.Role;
import synrgy.springsecurity.model.entity.oauth.UserOauth;
import synrgy.springsecurity.model.request.LoginModel;
import synrgy.springsecurity.model.response.TemplateResponse;
import synrgy.springsecurity.repository.oauth.UserOauthRepository;
import synrgy.springsecurity.service.UserService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Autowired
    private UserOauthRepository userRepository;
    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    public TemplateResponse templateResponse;

    @Value("${baseurl:}") // FILE_SHOW_RUL
    private String BASEURL;

    @Override
    public Map login(LoginModel loginModel) {
        /**
         * business logic for login here
         * **/
        try {
            Map<String, Object> map = new HashMap<>();

            UserOauth checkUser = userRepository.findOneByUsername(loginModel.getUsername());

            if ((checkUser != null) && (encoder.matches(loginModel.getPassword(), checkUser.getPassword()))) {
                if (!checkUser.isEnabled()) {
                    map.put("is_enabled", checkUser.isEnabled());
                    return templateResponse.error(map, 400);
                }
            }
            if (checkUser == null) {
                return templateResponse.error("user not found", 404);
            }
            if (!(encoder.matches(loginModel.getPassword(), checkUser.getPassword()))) {
                return templateResponse.error("wrong password", 400);
            }
            String url = BASEURL + "/oauth/token?username=" + loginModel.getUsername() +
                    "&password=" + loginModel.getPassword() +
                    "&grant_type=password" +
                    "&client_id=my-client-web" +
                    "&client_secret=password";
            ResponseEntity<Map> response = restTemplateBuilder.build().exchange(url, HttpMethod.POST, null, new
                    ParameterizedTypeReference<Map>() {
                    });

            if (response.getStatusCode() == HttpStatus.OK) {
                UserOauth user = userRepository.findOneByUsername(loginModel.getUsername());
                List<String> roles = new ArrayList<>();

                for (Role role : user.getRoles()) {
                    roles.add(role.getName());
                }
                //save token
//                checkUser.setAccessToken(response.getBody().get("access_token").toString());
//                checkUser.setRefreshToken(response.getBody().get("refresh_token").toString());
//                userRepository.save(checkUser);

                map.put("access_token", response.getBody().get("access_token"));
                map.put("token_type", response.getBody().get("token_type"));
                map.put("refresh_token", response.getBody().get("refresh_token"));
                map.put("expires_in", response.getBody().get("expires_in"));
                map.put("scope", response.getBody().get("scope"));
                map.put("jti", response.getBody().get("jti"));

                return map;
            } else {
                return templateResponse.error("user not found", 404);
            }
        } catch (HttpStatusCodeException e) {
            e.printStackTrace();
            if (e.getStatusCode() == HttpStatus.BAD_REQUEST) {
                return templateResponse.error("invalid login", 400);
            }
            return templateResponse.error(e, 400);
        } catch (Exception e) {
            e.printStackTrace();

            return templateResponse.error(e, 400);
        }
    }


}
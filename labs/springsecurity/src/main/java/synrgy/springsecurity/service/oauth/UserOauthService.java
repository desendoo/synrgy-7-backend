package synrgy.springsecurity.service.oauth;

import synrgy.springsecurity.model.request.RegisterModel;

import java.security.Principal;
import java.util.Map;

public interface UserOauthService {
    Map registerManual(RegisterModel objModel) ;
    Map registerByGoogle(RegisterModel objModel) ;
    Map getDetailProfile(Principal principal);
}

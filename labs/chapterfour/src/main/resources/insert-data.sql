INSERT INTO employee (name, address, dob, status) VALUES
('John Doe', '123 Main St', '1980-01-01', 'Active'),
('Jane Doe', '456 Maple Ave', '1985-02-02', 'Inactive'),
('Jim Smith', '789 Oak Dr', '1990-03-03', 'Active'),
('Jill Johnson', '321 Pine Ln', '1995-04-04', 'Active'),
('Joe Williams', '654 Elm St', '2000-05-05', 'Inactive');

INSERT INTO employee_detail (nik, npwp, id_employee) VALUES
('1234567890', 'AB12345', 1),
('2345678901', 'BC23456', 2),
('3456789012', 'CD34567', 3),
('4567890123', 'DE45678', 4),
('5678901234', 'EF56789', 5);
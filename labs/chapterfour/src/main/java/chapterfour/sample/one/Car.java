package chapterfour.sample.one;

import lombok.Data;

@Data
public class Car {

    private Integer idCar;
    private String brand;
    private String model;
    private Integer year;
    private Double distance;
    private Integer time;

    public Car(Integer idCar, String brand, String model, Integer year) {
        this.idCar = idCar;
        this.brand = brand;
        this.model = model;
        this.year = year;
    }

    public Integer speed() {
        return (int) (this.distance/this.time);
    }

    public void startEngine() {
        System.out.println(brand + " " + model  + " " +  String.valueOf(year) + " Engine started");
    }

    public void stopEngine() {
        System.out.println(brand + " " + model + " " + String.valueOf(year) + " Engine stopped");
    }

}
package chapterfour.sample.storeprocedure.controller;

import chapterfour.sample.storeprocedure.repository.EmployeeSPRepository;
import chapterfour.sample.storeprocedure.service.ServiceEmployeeSP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/v1/employee/sp")
public class EmployeeControllerSP {
    @Autowired
    public ServiceEmployeeSP serviceEmployee;
    @Autowired
    private DataSource dataSource;
    @Autowired
    public EmployeeSPRepository employeeSPRepository;

    @GetMapping(value = {"", "/"})
    public ResponseEntity<Map> getById() {
        Map map = new HashMap();
        // map.put("list", employeeRepository.getListSP());
        map.put("list", employeeSPRepository.findAll());
        return new ResponseEntity<Map>(map, HttpStatus.OK);
    }
}

package chapterfour.sample.storeprocedure.repository;

import chapterfour.sample.storeprocedure.model.entity.EmployeeSP;
// import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
//public interface EmployeeRepository extends PagingAndSortingRepository<Employee,Long> {
public interface EmployeeSPRepository extends JpaRepository<EmployeeSP, Long>, JpaSpecificationExecutor<EmployeeSP> {
    //Store prosedure
    @Query(value = "select * from getemployee()",nativeQuery = true)
    public List<Object> getListSP();
    // public List<Employee> findAll();

    @Transactional(readOnly = false)
    @Procedure("insert")
    void saveEmployeeSP(@Param("resid") Long resid, @Param("reqnama") String reqnama);

    @Procedure("update_employee")
    void updateEmployeeSP(@Param("resid") Long resid,@Param("reqnama") String reqnama);

    @Procedure("deleted_employee")
    void deleted_employee(@Param("resid") Long resid);

    @Procedure("getemployeebyid")
    Object getemployeebyid(@Param("reqid") Long reqid);

}

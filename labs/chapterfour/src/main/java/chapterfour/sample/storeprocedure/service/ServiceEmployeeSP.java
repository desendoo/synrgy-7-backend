package chapterfour.sample.storeprocedure.service;

import chapterfour.sample.storeprocedure.model.entity.EmployeeSP;

import java.util.Map;

public interface ServiceEmployeeSP {
    Map save(EmployeeSP request);
    Map edit(EmployeeSP request);
    Map delete(EmployeeSP request);
    Map list();

}

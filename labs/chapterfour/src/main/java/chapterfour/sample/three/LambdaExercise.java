package chapterfour.sample.three;

import java.util.function.BiConsumer;

public class LambdaExercise {

    public static void printMyFullName(BiConsumer<String, String> biConsumer) {
        biConsumer.accept("Budi", "Santoso");
    }

}

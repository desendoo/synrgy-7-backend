package chapterfour.sample.three;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import chapterfour.sample.one.Car;

public class CarDealer {
    // private Map<Integer, Car> cars;

    // processCars method that implements Consumer functional interface as its parameter
    // this method will iterate over the cars map and mark each car as sold
    public static <T> Map<Car, Boolean> processCars(List<Car> cars, Consumer<Car> action) {
        // One line of code that transforms the cars map into a map of cars and booleans
        // where the boolean value indicates if the car is sold or not
        return cars.stream()
                .collect(Collectors.toMap(car -> car, car -> {
                    action.accept(null);
                    return true;
                }));
    }

}

package chapterfour.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
public class ExampleController {

    private static final Logger logger = LoggerFactory.getLogger(ExampleController.class);

    @GetMapping("/hello")
    public String hello() {
        return "Hello, World!";
    }

    @GetMapping("/{yourName}")
    public String helloName(@PathVariable String yourName) {
        return "Hello, " + yourName + "!";
    }

    @GetMapping("/logging/{payload}")
    public String testLogging(@PathVariable String payload) {
        try {
            throw new Exception("This is a test exception with payload: " + payload);
        } catch (Exception e) {
            logger.error("An error occurred: " + e.getMessage());
        }
        return "Hello, " + payload + "!";
    }

}

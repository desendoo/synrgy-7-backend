package chapterfour;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChapterfourApplication {

	private static final Logger logger = LoggerFactory.getLogger(ChapterfourApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ChapterfourApplication.class, args);
		logger.info("info logging level");
		logger.error("eror logging level");
		logger.warn("warning logging level");
		logger.debug("debug logging level");
		logger.trace("trace logging level");
	}

}

package chapterfour.storeprocedure;

import org.springframework.stereotype.Component;

@Component
public class QuerySPEmployee {

    public String getData ="CREATE OR REPLACE FUNCTION public.getemployee()\n" +
            " RETURNS TABLE(resid bigint, resname varchar, resaddress text)\n" +
            " LANGUAGE plpgsql\n" +
            "AS $function$\n" +
            "\tbegin\n" +
            "\tRETURN QUERY\n" +
            "      select id bigint, name varchar, address from employee e;\n" +
            "\tEND;\n" +
            "$function$\n" +
            ";\n";

    public String getDataEmployeeLikeName ="CREATE OR REPLACE FUNCTION public.listemployee(reqnama varchar)\n" +
            " RETURNS TABLE(resid bigint, resname varchar, resaddress varchar)\n" +
            " LANGUAGE plpgsql\n" +
            "AS $function$\n" +
            "           DECLARE \n" +
            "               var_r record;\n" +
            "           BEGIN\n" +
            "               FOR var_r IN(SELECT \n" +
            "                           id,\n" +
            "                           name,\n" +
            "                           address \n" +
            "                           FROM employee\n" +
            "                           WHERE name ILIKE  reqnama)  \n" +
            "               LOOP\n" +
            "                   resid :=var_r.id  ;\n" +
            "                   resname :=var_r.name;\n" +
            "                   resaddress :=var_r.address;\n" +
            "                   RETURN NEXT;\n" +
            "               END LOOP;\n" +
            "           end $function$\n" +
            ";\n";

    public String insertEmployee ="CREATE OR REPLACE PROCEDURE public.insert(inout resid bigint,inout reqnama character varying)\n" +
            "             LANGUAGE plpgsql\n" +
            "            AS $procedure$\n" +
            "            begin\n" +
            "            if reqnama is null Then \n" +
            "              raise notice 'nama kosong';\n" +
            "             reqnama = 'nama wajib diisi';\n" +
            "             return;\n" +
            "            else \n" +
            "             raise notice 'nama  ada';\n" +
            "            end if;  \n" +
            "                 INSERT into public.employee\n" +
            "                 (id, created_date, updated_date, address, dob, \"name\", status) \n" +
            "                 SELECT nextval('employee_id_seq'),\n" +
            "                       now(),\n" +
            "                       now(),\n" +
            "                      'jakarta',\n" +
            "                      '1970-01-15',\n" +
            "                       reqnama,\n" +
            "                       'active'\n" +
            "                RETURNING id INTO resid; \n" +
            "                 commit;\n" +
            "            END;\n" +
            "            $procedure$;\n";
    public String updateEmployee = "CREATE OR REPLACE PROCEDURE public.update_employee(INOUT resid bigint, INOUT reqnama varchar)\n" +
            " LANGUAGE plpgsql\n" +
            "AS $procedure$\n" +
            "            begin\n" +
            "            if reqnama is null Then \n" +
            "              raise notice 'nama kosong';\n" +
            "             reqnama = 'nama wajib diisi';\n" +
            "             return;\n" +
            "            else \n" +
            "             raise notice 'nama  ada';\n" +
            "            end if;  \n" +
            "                 update employee\n" +
            "                 set name=reqnama\n" +
            "                 where id = resid;\n" +
            "               reqnama=reqnama;\n" +
//            "                 commit;\n" +  -- Tidak perlu COMMIT di sini, biarkan aplikasi Anda yang mengelola transaksi.
            "            END;\n" +
            "            $procedure$\n" +
            ";\n";

    public String deletedEmployee ="CREATE OR REPLACE PROCEDURE public.deleted_employee(INOUT resid bigint)\n" +
            " LANGUAGE plpgsql\n" +
            "AS $procedure$\n" +
            "            begin \n" +
            "                 update employee\n" +
            "                 set created_date =now()\n" +
            "                 where id = resid;\n" +
//            "                 commit;\n" +  -- Tidak perlu COMMIT di sini, biarkan aplikasi Anda yang mengelola transaksi.
            "            END;\n" +
            "            $procedure$\n" +
            ";\n";
    // public  String getByID = "CREATE OR REPLACE FUNCTION public.getemployeebyid(reqid bigint)\n" +
    //         " RETURNS TABLE(resid bigint, resname character varying, resaddress text)\n" +
    //         " LANGUAGE plpgsql\n" +
    //         "AS $function$\n" +
    //         "\tbegin\n" +
    //         "\t\treturn QUERY\n" +
    //         "\t\tselect id, name, address from employee where id =reqid;\n" +
    //         "\tEND;\n" +
    //         "$function$\n" +
    //         ";\n";
    public  String getByID = """
            CREATE OR REPLACE FUNCTION public.getemployeebyid(reqid bigint)
            RETURNS TABLE(resid bigint, resname character varying, resaddress text)
            LANGUAGE plpgsql
            AS $function$
            BEGIN
             RETURN QUERY
                  select id AS resid, name AS resname, address AS resaddress from employee e where id = reqid;
            END;
            $function$
            ;
            """;

}

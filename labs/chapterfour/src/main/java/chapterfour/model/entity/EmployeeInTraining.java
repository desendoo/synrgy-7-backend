package chapterfour.model.entity;

import lombok.Data;
import org.hibernate.annotations.Where;

import jakarta.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "employee_in_training")
@Where(clause = "deleted_date is null")
public class EmployeeInTraining extends AbstractDate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column
    private Date date;

    // Foreign key from training table
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_training")
    private Training training;

    // Foreign key from employee table
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_employee")
    private Employee employee;

}

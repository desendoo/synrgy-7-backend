package chapterfour.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.Where;

import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "employee")
@Where(clause = "deleted_date is null")
public class Employee extends AbstractDate implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    public String name;

    @Column(name = "address", columnDefinition = "TEXT")
    public String address;

    // 2016-01-01
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dob;

    public  String status;

    // FetchType.EAGER used to load the EmployeeDetail entity when the Employee entity is loaded
    @OneToOne(targetEntity = EmployeeDetail.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name="id_employee_detail", referencedColumnName = "id")
    @PrimaryKeyJoinColumn
    private EmployeeDetail employeeDetail;

}
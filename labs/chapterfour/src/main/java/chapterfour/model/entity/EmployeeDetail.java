package chapterfour.model.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.Where;

import java.io.Serializable;

@Data
@Entity
@Table(name = "employee_detail")
@Where(clause = "deleted_date is null")
public class EmployeeDetail extends AbstractDate implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nik", length = 45)
    private String nik;

    @Column(name = "npwp", length = 10)
    private String npwp;

    @JsonIgnore
    @OneToOne (targetEntity = Employee.class, cascade = CascadeType.ALL)
    @JoinColumn(name="id_employee", referencedColumnName = "id")
    private Employee employee;

}
package chapterfour.model.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.Where;

import java.io.Serializable;

@Data
@Entity
@Table(name = "account_card")
@Where(clause = "deleted_date is null")
public class AccountCard extends AbstractDate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "kind")
    private String kind;

    @Column(length = 100)
    private String accountNumber;

    //    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employee employee;

}

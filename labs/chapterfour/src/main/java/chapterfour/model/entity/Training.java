package chapterfour.model.entity;

import lombok.Data;
import org.hibernate.annotations.Where;

import jakarta.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "training")
@Where(clause = "deleted_date is null")
public class Training extends AbstractDate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "trainer")
    private String trainer;

    @Column(name = "theme")
    private String theme;

}

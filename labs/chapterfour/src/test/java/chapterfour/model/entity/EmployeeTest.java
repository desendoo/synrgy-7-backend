package chapterfour.model.entity;

import chapterfour.repository.EmployeeRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EmployeeTest {

    @Autowired
    private EmployeeRepository employeeRepository;

    private EmployeeDetail employeeDetail;

    @BeforeAll
    public void setUp() {
        employeeDetail = new EmployeeDetail();
        employeeDetail.setNik("1234567890");
        employeeDetail.setNpwp("0987654321");
    }

    @Test
    public void createEmployee() {
        Date dob = new Date();
        Employee employee = new Employee();

        employee.setName("Rizki");
        employee.setAddress("Jakarta");
        employee.setDob(dob);
        employee.setStatus("ACTIVE");
        employee.setEmployeeDetail(employeeDetail);

        employeeRepository.save(employee);
        assertEquals("Rizki", employee.getName());
    }

    @Test
    public  void getEmployee() {
        Employee employee = employeeRepository.getById(1L);
        assertEquals("Rizki", employee.getName());
    }

}

package chapterfour.sample.three;

import chapterfour.sample.three.CarDealer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import chapterfour.sample.one.Car;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class LambdaExerciseTest {

    private static Map<String, String> names;

    @BeforeAll
    static void setUp() {
        names = new HashMap<>();
        names.put("Budi", "Santoso");
        names.put("Joko", "Widodo");
        names.put("Ahmad", "Yani");
    }

    // Test CarDealer.processCars method
    @Test
    void processCars() {
        List<Car> cars = Arrays.asList(
                new Car(1, "Toyota", "Corolla", 2019),
                new Car(2, "Honda", "Civic", 2020),
                new Car(3, "Suzuki", "Ertiga", 2021)
        );


        CarDealer.processCars(cars, car -> {
            System.out.println("Car " + car.getBrand() + " " + car.getModel()  + " " +  String.valueOf(car.getYear()) +  " is sold");
        });
    }

    @Test
    void methodReference() {
        List<Car> cars = Arrays.asList(
                new Car(1, "Toyota", "Corolla", 2019),
                new Car(2, "Honda", "Civic", 2020),
                new Car(3, "Suzuki", "Ertiga", 2021)
        );
        cars.forEach(Car::startEngine);

        List<String> carBrands = new ArrayList<>(cars.stream()
                .map(Car::getBrand)
                .toList());
        carBrands.forEach(System.out::println);
        carBrands.removeIf(brand -> brand.equals("Toyota"));
        carBrands.forEach(System.out::println);
    }

    @Test
    void lambdaNoParameter() {
        Runnable runnable = () -> System.out.println("Hello World");
    }

    @Test
    void lambdaOneParameterConsumer() {
        Consumer<String> consumer = (String myName) -> {
            System.out.println("Hello " + myName);
        };
        consumer.accept("Budi");
    }

    @Test
    void lambdaTwoParameterBiConsumer() {
        BiConsumer<String, String> biConsumer = (String firstName, String lastName) -> {
            System.out.println("Hello " + firstName + " " + lastName);
        };
        biConsumer.accept("Budi", "Santoso");

        // names.forEach(
        // LambdaExercise.printMyFullName(biConsumer);
    }

    @Test
    void lambdaNoParameterSupplier() {
        Supplier<String> supplier = () -> {
            return "Hello World";
        };
        System.out.println(supplier.get());
    }

}

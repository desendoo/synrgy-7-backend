package chapterfour.sample.storeprocedure;

import chapterfour.sample.storeprocedure.model.entity.EmployeeSP;
import chapterfour.sample.storeprocedure.repository.EmployeeSPRepository;
import chapterfour.storeprocedure.QuerySPEmployee;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TestingSp {
    @Autowired
    private DataSource dataSource;
    @Autowired
    public EmployeeSPRepository employeeSPRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public QuerySPEmployee querySPEmployee;
    @Before
    public void init() {
        try {
            jdbcTemplate.execute(querySPEmployee.getData);
            jdbcTemplate.execute(querySPEmployee.getDataEmployeeLikeName);
            jdbcTemplate.execute(querySPEmployee.insertEmployee);
            jdbcTemplate.execute(querySPEmployee.updateEmployee);
            jdbcTemplate.execute(querySPEmployee.deletedEmployee);
        } finally {
//            session.close();
        }
    }

    @Test
    public void listSP(){
        // List<Object> obj =  employeeRepository.getListSP();
        List<EmployeeSP> obj =  employeeSPRepository.findAll();
        System.out.println(obj);
    }

    @Test
    public void getIdSp(){
        Object obj =  employeeSPRepository.getemployeebyid(2L);
        System.out.println(obj);
    }

    @Test
    public void saveSp(){
        employeeSPRepository.saveEmployeeSP(null, "spring boot1");
    }

    @Test
    public void udpateSP(){
        employeeSPRepository.updateEmployeeSP(6L, "spring boot1");
    }
    @Test
    public void deletedSp(){
        employeeSPRepository.deleted_employee(8L);
    }

}

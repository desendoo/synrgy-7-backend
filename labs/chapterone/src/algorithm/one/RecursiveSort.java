package algorithm.one;

public class RecursiveSort {
    // Method untuk menemukan indeks elemen terkecil dalam subarray
    static int findMinIndex(int[] arr, int start, int end) {
        int minIndex = start;
        for (int i = start + 1; i <= end; i++) {
            if (arr[i] < arr[minIndex]) {
                minIndex = i;
            }
        }
        return minIndex;
    }

    // Method untuk melakukan selection sort secara rekursif
    static void recursiveSelectionSort(int[] arr, int start, int end) {
        if (start >= end) {
            return;
        }
        int minIndex = findMinIndex(arr, start, end);
        if (minIndex != start) {
            // Jika elemen terkecil bukan di indeks awal, maka tukar elemen di awal dengan elemen terkecil
            int temp = arr[minIndex];
            arr[minIndex] = arr[start];
            arr[start] = temp;
        }
        // Panggil rekursif untuk subarray selanjutnya
        recursiveSelectionSort(arr, start + 1, end);
    }

    // Method untuk mencetak array
    static void printArray(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n; ++i)
            System.out.print(arr[i] + " ");
        System.out.println();
    }

    public static void main(String[] args) {
        int[] arr = {64, 25, 12, 22, 11};
        int n = arr.length;
        System.out.println("Array sebelum sorting:");
        printArray(arr);
        recursiveSelectionSort(arr, 0, n - 1);
        System.out.println("Array setelah sorting:");
        printArray(arr);
    }
}

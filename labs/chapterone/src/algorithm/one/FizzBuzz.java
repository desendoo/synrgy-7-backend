package algorithm.one;

import java.util.Scanner;

public class FizzBuzz {
    public static void main(String[] args) {
        fizzBuzz();
    }
    public static void fizzBuzz() {
        /* FizzBuzz Algorithm
        DEFINE batas bawah <= 1
        INPUT batas atas
        LOOP counter <= batas bawah
            IF counter merupakan kelipatan 3 DAN 5
                PRINT "FizzBuzz "
            ELSE IF counter merupakan kelipatan 3
                PRINT "Fizz "
            ELSE IF counter merupakan kelipatan 5
                PRINT "Buzz "
            ELSE
                PRINT counter + " "
        UNTIL hingga batas atas
         */
        Scanner input = new Scanner(System.in);
        int batasBawah = 1;
        int batasAtas = input.nextInt();
        int counter = batasBawah;
        while (counter <= batasAtas) {
            if (counter % 3 == 0 &&
                counter % 5 == 0) {
                System.out.print("FizzBuzz ");
            } else if (counter % 3 == 0) {
                System.out.print("Fizz ");
            } else if (counter % 5 == 0) {
                System.out.print("Buzz ");
            } else {
                System.out.print(counter + " ");
            }
            counter++;
        }
    }
}

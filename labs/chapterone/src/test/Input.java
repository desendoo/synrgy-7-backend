package test;

import java.util.Scanner;

public class Input {

    public static void main(String[] args) {
//        inputTest1();
//        inputTest2();
       chapterThree();
//        chapterFour();
        // chapterFive();
        // System.out.println("");
        // fizzBuzz(1, 20);
    }

    private static void fizzBuzz(int start, int end) {
        for (int i = start; i <= end; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.print("FizzBuzz ");
            } else if (i % 3 == 0) {
                System.out.print("Fizz ");
            } else if (i % 5 == 0) {
                System.out.print("Buzz ");
            } else {
                System.out.print(i + " ");
            }
        }
    }

    public static void chapterFive() {
        /* FizzBuzz Algorithm
        DEFINE batas bawah <= 1
        INPUT batas atas
        LOOP counter <= batas bawah
            IF counter merupakan kelipatan 3 DAN 5
                PRINT "FizzBuzz "
            ELSE IF counter merupakan kelipatan 3
                PRINT "Fizz "
            ELSE IF counter merupakan kelipatan 5
                PRINT "Buzz "
            ELSE
                PRINT counter + " "
        UNTIL hingga batas atas
         */
        Scanner input = new Scanner(System.in);
        int batasBawah = 1;
        int batasAtas = input.nextInt();
        int counter = batasBawah;
        while (counter <= batasAtas) {
            if (counter % 3 == 0 &&
                counter % 5 == 0) {
                System.out.print("FizzBuzz ");
            } else if (counter % 3 == 0) {
                System.out.print("Fizz ");
            } else if (counter % 5 == 0) {
                System.out.print("Buzz ");
            } else {
                System.out.print(counter + " ");
            }
            counter++;
        }
    }

    public static void chapterFour() {
    }

    public static void chapterThree() {
        Scanner input = new Scanner(System.in);

        int ayamGoreng = 15000;
        int nasiGoreng = 18000;
        int nasiGorengSpesial = 22000;
        int bakmiGoreng = 20000;
        int bakmiGorengSpesial = 24000;
        String menuAyamGoreng = "Ayam Goreng";
        String menuNasiGoreng = "Nasi Goreng";
        String menuNasiGorengSpesial = "Nasi Goreng Spesial";
        String menuBakmiGoreng = "Bakmi Goreng";
        String menuBakmiGorengSpesial = "Bakmi Goreng Spesial";

        System.out.println("Selamat datang di Rumah Sendiri");
        System.out.println("Daftar menu:");
        System.out.println("1. " + menuAyamGoreng + "\t\t" + ayamGoreng);
        System.out.println("2. " + menuNasiGoreng + "\t\t" + nasiGoreng);
        System.out.println("3. Nasi " + menuNasiGorengSpesial + "\t\t" + nasiGorengSpesial);
        System.out.println("4. " + menuBakmiGoreng + "\t\t" + bakmiGoreng);
        System.out.println("5. Bakmi " + menuBakmiGorengSpesial + "\t\t" + bakmiGorengSpesial);

        System.out.print("Silakan pilih menu: ");
        int pilihanMenu = input.nextInt();

        System.out.print("Silakan tentukan jumlah pesanan: ");
        int jumlahPesanan = input.nextInt();

        String menuPilihan = "";
        int totalHarga = 0;
        if (pilihanMenu == 1) {
            totalHarga = ayamGoreng * jumlahPesanan;
            menuPilihan = menuAyamGoreng;
        } else if (pilihanMenu == 2) {
            totalHarga = nasiGoreng * jumlahPesanan;
            menuPilihan = menuAyamGoreng;
        } else if (pilihanMenu == 3) {
            totalHarga = nasiGorengSpesial * jumlahPesanan;
            menuPilihan = menuNasiGorengSpesial;
        } else if (pilihanMenu == 4) {
            totalHarga = bakmiGoreng * jumlahPesanan;
            menuPilihan = menuBakmiGoreng;
        } else if (pilihanMenu == 5) {
            totalHarga = bakmiGorengSpesial * jumlahPesanan;
            menuPilihan = menuBakmiGorengSpesial;
        }
        System.out.println("\n===== Nota Pembayaran =====");
        System.out.println("Menu Pilihan : " + menuPilihan);
        System.out.println("Jumlah Pesanan : " + jumlahPesanan);
        System.out.println("Total Harga : " + totalHarga);

        input.close();
    }

    public static void inputTest2() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("input nama, qty,total =");

        String nama = scanner.next();
        Long qty = scanner.nextLong();
        Integer total = scanner.nextInt();

        System.out.println("nama ="+nama);
        System.out.println("total ="+total);
        System.out.println("qty ="+qty);

        scanner.close();
    }

    public static void inputTest1() {
        Scanner input = new Scanner(System.in);

        System.out.print("Enter an integer: ");
//        int number = input.nextInt();
        String text = input.next();
//        System.out.println("You entered " + number);
        System.out.println("You entered " + text);

        input.close();
    }

}

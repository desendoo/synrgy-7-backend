package chapterthree.one;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CarTest {

    private static Car car;

    @BeforeAll
    static void setup() {
        car = new Car(1, "Toyota", "Corolla", 2019);
        car.setDistance(100.0);
        car.setTime(10);
        System.out.println("Car object is created: " + car);
    }

    @Test
    void speedTest() {
        Integer speed = car.speed();
        assertEquals(10, speed.intValue());
        System.out.println("Car speed is: " + speed);
    }

    @AfterAll
    static void tearDown() {
        car = null;
        System.out.println("Car object is destroyed: " + car);
    }

}
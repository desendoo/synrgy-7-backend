package chapterthree.four;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class StreamPracticeTest {

    @Test
    void streamCollectorTest() {}
    @Test
    void streamCollectorTest() {
        List<Person> people = List.of(
                new Person("Alice", 30, "New York"),
                new Person("Bob", 25, "London"),
                new Person("Charlie", 35, "Paris"),
                new Person("David", 40, "New York")
        );

        // Group people by city
        Map<String, List<Person>> peopleByCity = people.stream()
                .collect(Collectors.groupingBy(Person::getCity));
        System.out.println("People grouped by city: " + peopleByCity);

        // Find the average age of people in each city
        Map<String, Double> averageAgeByCity = people.stream()
                .collect(Collectors.groupingBy(
                        Person::getCity,
                        Collectors.averagingInt(Person::getAge)
                ));
        System.out.println("Average age of people by city: " + averageAgeByCity);

        // Partition people into adults and minors
        Map<Boolean, List<Person>> adultsAndMinors = people.stream()
                .collect(Collectors.partitioningBy(
                        person -> person.getAge() >= 18
                ));
        System.out.println("Adults and minors: " + adultsAndMinors);
    }
    void streamMapTest() {
        List<FoodOrder> foodOrders = Arrays.asList(
                new FoodOrder("Nasi Goreng", 15000, 2),
                new FoodOrder("Mie Goreng", 12000, 3),
                new FoodOrder("Sate Ayam", 20000, 1),
                new FoodOrder("Bakmi Goreng", 18000, 2)
        );

        List<String> listOfFoodName = foodOrders.stream()
                .map(FoodOrder::getFoodName)
                .toList();
        System.out.println("List of food names: ");
        listOfFoodName.forEach(System.out::println);

        List<Double> listOfFoodPrice = foodOrders.stream()
                .map(FoodOrder::getPrice)
                .toList();
        System.out.println("\nList of food prices: ");
        listOfFoodPrice.forEach(System.out::println);

        List<String> allFoodNames = foodOrders.stream()
                .flatMap(order -> {
                    String[] foodNamesArray = new String[order.getQuantity()];
                    Arrays.fill(foodNamesArray, order.getFoodName());
                    return Arrays.stream(foodNamesArray);
                })
                .toList();
        System.out.println("\n");
        System.out.println("All food names: " + allFoodNames);

        List<String> sortedFoodNames = foodOrders.stream()
                .map(FoodOrder::getFoodName)
                .sorted()
                .toList();
        sortedFoodNames.forEach(System.out::println);
        System.out.println("\n");

        List<String> sortedFoodNameByPrice = foodOrders.stream()
                .sorted(Comparator.comparing(FoodOrder::getPrice))
                .map(FoodOrder::getFoodName)
                .toList();
        sortedFoodNameByPrice.forEach(System.out::println);
        System.out.println("\n");

        // Reduce function example
        Double totalPrice = foodOrders.stream()
                .map(order -> order.getPrice() * order.getQuantity())
                .reduce(0.0, Double::sum);
        System.out.println("\nTotal price: " + totalPrice);

        String cheapestFood = foodOrders.stream()
                .min(Comparator.comparing(FoodOrder::getPrice))
                .map(FoodOrder::getFoodName)
                .orElse("No food found");
        System.out.println("\nCheapest food: " + cheapestFood);

        String mostExpensiveFood = foodOrders.stream()
                .max(Comparator.comparing(FoodOrder::getPrice))
                .map(FoodOrder::getFoodName)
                .orElse("No food found");
        System.out.println("\nMost expensive food: " + mostExpensiveFood);

        String mostExpensiveFood2 = foodOrders.stream()
                .max((order1, order2) -> order1.getPrice() > order2.getPrice() ? 1 : -1)
                .map(FoodOrder::getFoodName)
                .orElse("No food found");
        System.out.println("\nMost expensive food 2: " + mostExpensiveFood2);

        List<FoodOrder> allFoodAbove15000 = foodOrders.stream()
                .filter(order -> order.getPrice() > 15000)
                .sorted(Comparator.comparingDouble(FoodOrder::getPrice).reversed())
                .toList();
        System.out.println("\nAll food above 15000: " + allFoodAbove15000);

        // Collectors example
        List<String> foodNames = foodOrders.stream()
                .map(FoodOrder::getFoodName)
                .collect(Collectors.toList());
        System.out.println("\nFood names: " + foodNames);
    }

    @Test
    void streamTest() {
        Stream<String> stream1 = Stream.of("Java", "Kotlin", "Scala", "Groovy");
        Stream<String> stream2 = Stream.empty();

        String data = null;
        Stream<String> stream3 = Stream.ofNullable(data);
        Stream<String> stream4 = Stream.of("Java", "Kotlin", "Scala", "Groovy");

        Stream<String> streamOfArrayOfList = Arrays.stream(new String[]{"Java", "Kotlin", "Scala", "Groovy"});

    }

}
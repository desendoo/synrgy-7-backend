package chapterthree.two;

import org.junit.jupiter.api.Test;

import java.util.Optional;

public class OptionalExcerciseTest {

    @Test
    public void testOptional() {
        // 3. Membuat Objek kategori
        Category category = new Category("Iphone");
        // 2 .Membuat objek detail produk dengan kategori
        DetailProduct detailProduct = new DetailProduct("Smartphone", Optional.of(category));
        // 1 :Membuat objek produk dengan detail produk
        Product product = new Product("iPhone", Optional.of(detailProduct));

        // OrElse String
        Optional<String> optionalValue = Optional.ofNullable(null);
        String value = optionalValue.orElse("Default Value");
        System.out.println("or else : "+value);

        // OrElse Object ?
        Optional<DetailProduct> detailProduct1 = Optional.ofNullable(null);
        DetailProduct detailProductNew = new DetailProduct("VIVO", Optional.of(category));

        DetailProduct value1 = detailProduct1.orElse(detailProductNew);
        System.out.println("or else di Objek :"+value1);

        // OrElseGet
        Optional<String> optionalValueGet = Optional.ofNullable(null);
        String value3 = optionalValue.orElseGet(() -> "Default Value");
        System.out.println("or else get: " + value3);

        // orelseGet versi 2
        String value4 = optionalValue.orElseGet(() -> {
            // tulis logic yang dibutuhkan
            return "Default Value dari value 4";
        });
        System.out.println("or else get value4 : " + value4);

        // orelseGet Object 3 tampa logic
        DetailProduct value5 = detailProduct1.orElseGet(() -> detailProductNew);
        System.out.println("or else get object: " + value5);

        // orelseGet Object dengan logic
        DetailProduct value6 = detailProduct1.orElseGet(() -> {
            // menuliskan logic
            return detailProductNew;
        });
        System.out.println("or else get object 2: " + value6);

        // name of DetailProduct
        Optional<String> nameOfDetailProduct = Optional.of(product.getDetailProduct()
                .map(DetailProduct::getDescription)
                .orElse("No Detail Product"));
        System.out.println("Name of Detail Product : " + nameOfDetailProduct);
        // name of Category
        Optional<String> nameOfCategory = Optional.of(product.getDetailProduct()
                .flatMap(DetailProduct::getCategory)
                .map(Category::getName)
                .orElse("No Category"));
        System.out.println("Name of Category : " + nameOfCategory);
    }

    @Test
    public void testOptionalOne() {
        // Membuat objek kategori
        Category category = new Category("Electronics");
        // Membuat objek detail produk dengan kategori
        DetailProduct detailProduct = new DetailProduct("Smartphone", Optional.of(category));
        // Membuat objek produk dengan detail produk
        Product product = new Product("iPhone", Optional.of(detailProduct));

        // OrElse String example
        Optional<String> optionalValue = Optional.of("Hello");
        String optionalString = optionalValue.orElse("No Value");
        System.out.println("orElse: " + optionalString);

        // OrElse Object example
        Optional<Category> optionalCategory = Optional.ofNullable(null);
        Category categoryValue = optionalCategory.orElse(new Category("No Category"));

        // OrElseGet example
        Optional<String> optionalValue1 = Optional.ofNullable(null);
        String valueInLambda = optionalValue1.orElseGet(() -> "Default Value lambda");
        String valueInMethodReference = optionalValue1.orElseGet(() -> {
            return "Default Value method reference";
        });
        String valueInFunction = optionalValue1.orElseGet(() -> {
            return "Default Value function";
        });
        Category categoryInFunction = optionalCategory.orElseGet(() -> {
            return new Category("No Category");
        });
        System.out.println("orElseGet in Lambda: " + valueInLambda);
        System.out.println("orElseGet in Method Reference: " + valueInMethodReference);
        System.out.println("orElseGet in Function: " + valueInFunction);
        System.out.println("orElseGet Object in Function: " + categoryInFunction);
    }

    @Test
    public void testOptionalTwo() {
        // Membuat objek kategori
        Category category = new Category("Electronics");
        // Membuat objek detail produk dengan kategori
        DetailProduct detailProduct = new DetailProduct("Smartphone", Optional.of(category));
        // Membuat objek produk dengan detail produk
        Product product = new Product("iPhone", Optional.of(detailProduct));
        // Product product = new Product();
        // Mengambil nama produk
        String productName = product.getName();
        // Mengambil detail produk
        Optional<DetailProduct> detailProductOptional = product.getDetailProduct();
        // Mengambil deskripsi produk
        String description = detailProductOptional.map(DetailProduct::getDescription).orElse("No Description");
        // Mengambil kategori produk
        Optional<Category> categoryOptional = detailProductOptional.flatMap(DetailProduct::getCategory);
        // Mengambil nama kategori
        String categoryName = categoryOptional.map(Category::getName).orElse("No Category");
    }

    @Test
    public void testOptionalMapOne() {
        // Membuat objek kategori
        Category category = new Category("Electronics");
        // Membuat objek detail produk dengan kategori
        DetailProduct detailProduct = new DetailProduct("Smartphone", Optional.of(category));
        // Membuat objek produk dengan detail produk
        Product product = new Product("iPhone", Optional.of(detailProduct));

        // Mengambil nama produk
        String productName = product.getName();
        System.out.println("productName :" + productName);
        // Mengambil detail produk
        Optional<DetailProduct> detailProductOptional = product.getDetailProduct();
        System.out.println("detailProductOptional :" + detailProductOptional);
        // Mengambil deskripsi produk
        String description = detailProductOptional.map(DetailProduct::getDescription).orElse("No Description");
        System.out.println("description :" + description);
        // Mengambil kategori produk
        Optional<Category> categoryOptional = detailProductOptional.flatMap(DetailProduct::getCategory);
        System.out.println("categoryOptional :" + categoryOptional);
        // Mengambil nama kategori
        String categoryName = categoryOptional.map(Category::getName).orElse("No Category");
        System.out.println("categoryName :" + categoryName);

        Optional<Integer> categoryNameLength = categoryOptional.map(Category::getName).map(String::length);
        System.out.println("categoryNameLength :" + categoryNameLength);
        Optional<DetailProduct> detailProductDescription = detailProductOptional.map(detailProduct1 -> {
            detailProduct1.setDescription("New Description");
            return detailProduct1;
        });
    }

    @Test
    public void optionalFlatMapExcercise() {
        // Example 1: Using map
        Optional<String> name = Optional.of("John");
        Optional<String> upperCaseName = name.map(String::toUpperCase);
        upperCaseName.ifPresent(System.out::println); // Output: JOHN

        // Example 2: Using flatMap
        Optional<String> flatName = Optional.of("Jane");
        Optional<String> flatUpperCaseName = flatName.flatMap(s -> Optional.of(s.toUpperCase()));
        flatUpperCaseName.ifPresent(System.out::println); // Output: JANE

        // Example 3: Using map
        Optional<String> nullName = Optional.ofNullable(
            "Desendo Imanuel");
            //     null);
        Optional<Integer> nullUpperCaseName = nullName.map(s -> s.length());
        Optional<Integer> nullUpperCaseNameInitial = nullName.map((s) -> {
            return s.split(" ")[0].length();
        });
        System.out.println("nullUpperCaseName: " + nullUpperCaseName.orElse(Integer.valueOf("123")));
        System.out.println("nullUpperCaseNameInitial: " + nullUpperCaseNameInitial.orElse(Integer.valueOf("123")));

        // Example 4: Using flatMap
        Optional<String> flatNullName = Optional.ofNullable(
                "Desendo Imanuel");
                // null);
        Optional<String> flatNullUpperCaseName = flatNullName.flatMap(s -> Optional.ofNullable(s).map(String::toUpperCase));
        System.out.println("flatNullUpperCaseName: " + flatNullUpperCaseName.orElse("Name is null"));

        // List<String> strings = Arrays.asList("apple", "banana", "orange");
        // List<String> upperCaseStrings = strings.stream()
        //         .map(String::toUpperCase)
        //         .collect(Collectors.toList());
    }

}

package chapterthree;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChapterthreeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChapterthreeApplication.class, args);
	}

}

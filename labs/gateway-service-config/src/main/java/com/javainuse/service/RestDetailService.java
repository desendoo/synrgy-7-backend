// package com.javainuse.service;
//
// import com.javainuse.data.RestDetail;
// import com.javainuse.respository.RestDetailRepository;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Service;
//
// import java.util.List;
//
// @Service
// public class RestDetailService {
//
//     @Autowired
//     private RestDetailRepository restDetailRepository;
//
//     // This method should interact with the database to retrieve the REST details
//     public List<RestDetail> getAllRestDetails() {
//         // implementation depends on your database and ORM
//         return restDetailRepository.findAll();
//     }
// }
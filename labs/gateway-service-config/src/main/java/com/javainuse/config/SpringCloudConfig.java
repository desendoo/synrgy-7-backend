package com.javainuse.config;

// import com.javainuse.service.RestDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringCloudConfig {

    // @Autowired
    // RestDetailService restDetailService;

//    referensi https://spring.io/projects/spring-cloud-gateway
    @Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
        // RouteLocatorBuilder.Builder routes = builder.routes();
        // restDetailService.getAllRestDetails().forEach(restDetail -> {
        //     routes.route(r -> r.path(restDetail.getPath())
        //             .uri(restDetail.getUri())
        //             .id(restDetail.getId()));
        // });
        // return routes.build();
        return builder.routes()
                .route(r -> r.path("/kafka/**")// localhost:8080/employee
                        .uri("http://localhost:8080/")// mengarah
                        .id("kafkaModule"))

                .route(r -> r.path("/api/v1/**")// localhost:8080/employee
                        .uri("http://localhost:8081/")// mengarah
                        .id("employeeModule"))

                .route(r -> r.path("/api/v2/**")//localhost:8080/consumer
                        .uri("http://localhost:8082/")
                        .id("consumerModule"))
                .route(r -> r.path("/api/v3/**")//localhost:8080/consumer
                        .uri("http://localhost:8083/")
                        .id("reportModule"))
                .route(r -> r.path("/api/v3/**")//localhost:8080/consumer
                        .uri("http://localhost:8083/")
                        .id("masterModule"))
                .build();
    }

}
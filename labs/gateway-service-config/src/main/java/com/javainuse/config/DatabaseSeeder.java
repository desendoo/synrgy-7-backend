// package com.javainuse.config;
//
//
// import com.javainuse.data.RestDetail;
// import com.javainuse.respository.RestDetailRepository;
// import org.springframework.boot.ApplicationArguments;
// import org.springframework.boot.ApplicationRunner;
// import org.springframework.stereotype.Component;
// import org.springframework.stereotype.Service;
//
// @Component
// @Service
// public class DatabaseSeeder implements ApplicationRunner {
//
//     private final RestDetailRepository restDetailRepository;
//
//     public DatabaseSeeder(RestDetailRepository restDetailRepository) {
//         this.restDetailRepository = restDetailRepository;
//     }
//
//     @Override
//     public void run(ApplicationArguments args) throws Exception {
//         //         .route(r -> r.path("/kafka/**")// localhost:8080/employee
//         //                 .uri("http://localhost:8080/")// mengarah
//         //                 .id("kafkaModule"))
//         RestDetail restDetail = new RestDetail("kafkaModule", "/kafka/**", "http://localhost:8080/");
//         restDetailRepository.save(restDetail);
//         restDetailRepository.findAll().forEach(System.out::println);
//         System.out.println("DatabaseSeeder.run");
//     }
//
// }
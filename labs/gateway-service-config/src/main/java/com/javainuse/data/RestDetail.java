package com.javainuse.data;

import lombok.AllArgsConstructor;
import lombok.Data;

// import javax.persistence.*;

@Data
// @Entity
@AllArgsConstructor
public class RestDetail {

    // @Id
    private String id;

    // @Column
    private String path;

    // @Column
    private String uri;

    // getters and setters
}
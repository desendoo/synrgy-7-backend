package synrgy.challengethree.view;

import synrgy.challengethree.model.*;

import java.util.Set;

public class MenuView {

    public static void printMenus(Set<MenuAbstract> menus) {
        System.out.println("Menu List:");
        System.out.println("-------------------------------------------------------");
        System.out.printf("%-5s %-30s %-10s", "ID", "Menu", "Price (IDR)\n");
        System.out.println("-------------------------------------------------------");

        menus.forEach(MenuView::printMenuDetail);

        System.out.println("-------------------------------------------------------");
    }

    public static void printMenuDetail(MenuAbstract menu) {
        System.out.printf("%-5s %-30s %-10.2f", menu.getId(), menu.getName(), menu.getPrice());

        if (menu instanceof Food) {
            System.out.println(", Food Type: " + ((Food) menu).getType());
        } else if (menu instanceof Drink) {
            System.out.println(", Drink Size: " + ((Drink) menu).getSize());
        } else if (menu instanceof Dessert) {
            System.out.println(", Dessert Flavor: " + ((Dessert) menu).getFlavour());
        } else if (menu instanceof Snack) {
            System.out.println(", Snack Size: " + ((Snack) menu).getSize());
        }
    }
}
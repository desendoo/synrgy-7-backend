package synrgy.challengethree.repository;

import lombok.Getter;
import synrgy.challengethree.model.Cart;
import synrgy.challengethree.model.ChoosenMenu;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
public class CartRepository {
    private Cart cart;

    public CartRepository(String idCart) {
        this.cart = new Cart(idCart, Optional.empty());
    }

    public void addMenu(ChoosenMenu menu) {
        this.cart.getMenus().ifPresentOrElse(menus -> menus.add(menu), () -> {
            List<ChoosenMenu> menus = new ArrayList<>();
            menus.add(menu);
            this.cart.setMenus(Optional.of(menus));
        });

        // Conventional way to implement addMenu method
        /*
        if (this.cart.getMenus().isPresent()) {
            this.cart.getMenus().ifPresent(menus -> menus.add(menu));
        } else {
            List<ChoosenMenu> menus = new ArrayList<>();
            menus.add(menu);
            this.cart.setMenus(Optional.of(menus));
        }
        */
    }

    public Integer getMenuQuantityById(String idMenu) {
        return this.cart.getMenus().flatMap(menus -> menus.stream()
                .filter(menu -> menu.getMenu().getId().equals(idMenu))
                .findFirst()
                .map(ChoosenMenu::getQuantity)).orElse(null);
    }

    public ChoosenMenu getMenuById(String idMenu) {
        return this.cart.getMenus().flatMap(menus -> menus.stream()
                .filter(menu -> menu.getMenu().getId().equals(idMenu))
                .findFirst()).orElse(null);
    }

    public void updateMenuQuantityById(String idMenu, Integer quantity) {
        this.cart.getMenus().flatMap(menus -> menus.stream()
                .filter(menu -> menu.getMenu().getId().equals(idMenu))
                .findFirst()).ifPresent(menu -> menu.setQuantity(quantity));
    }

    public void deleteMenuById(String idMenu) {
        this.cart.getMenus().ifPresent(menus -> menus.removeIf(menu -> menu.getMenu().getId().equals(idMenu)));
    }
}
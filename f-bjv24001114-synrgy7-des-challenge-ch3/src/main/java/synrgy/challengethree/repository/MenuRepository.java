package synrgy.challengethree.repository;

import lombok.Getter;
import synrgy.challengethree.model.MenuAbstract;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class MenuRepository {
    private Set<MenuAbstract> menus;

    public MenuRepository() {
        this.menus = new LinkedHashSet<>();
    }

    public Set<MenuAbstract> getMenus() {
        return menus;
    }

    public void addMenu(MenuAbstract menu) {
        this.menus.add(menu);
    }

    public MenuAbstract getMenuById(String id) {
        return menus.stream().filter(menu -> menu.getId().equals(id)).findFirst().orElse(null);

        // Conventional way to iterate through the menus
        /*for (MenuAbstract menu : menus) {
            if (menu.getId().equals(id)) {
                return menu;
            }
        }
        return null;*/
    }

    public Boolean deleteMenuById(String id) {
        return menus.removeIf(menu -> menu.getId().equals(id));

        // Conventional way to iterate through the menus
        /*for (MenuAbstract menu : menus) {
            if (menu.getId().equals(id)) {
                menus.remove(menu);
                return true;
            }
        }
        return false;*/
    }

    public Boolean updateMenuById(MenuAbstract menu, String id) {
        return menus.stream().filter(m -> m.getId().equals(id)).findFirst().map(m -> {
            m.setName(menu.getName());
            m.setId(menu.getId());
            m.setPrice(menu.getPrice());
            return true;
        }).orElse(false);

        // Conventional way to iterate through the menus
        /*for (MenuAbstract m : menus) {
            if (m.getId().equals(id)) {
                m.setName(menu.getName());
                m.setId(menu.getId());
                m.setPrice(menu.getPrice());
                return true;
            }
        }
        return false;*/
    }

    public Set<MenuAbstract> getMenusByType(Class<? extends MenuAbstract> menuType) {
        return menus.stream().filter(menuType::isInstance).collect(Collectors.toSet());

        // Another way to collect the menus
        // return menus.stream().filter(menuType::isInstance).collect(LinkedHashSet::new, Set::add, Set::addAll);

        // Conventional way to iterate through the menus
        /*Set<MenuAbstract> menusByFoodType = new LinkedHashSet<>();
        for (MenuAbstract menu : menus) {
            if (menuType.isInstance(menu)) {
                menusByFoodType.add(menu);
            }
        }
        return menusByFoodType;*/
    }
}
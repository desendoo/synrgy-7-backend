package synrgy.challengethree.repository;

import lombok.Data;
import lombok.Getter;
import synrgy.challengethree.model.Cart;
import synrgy.challengethree.model.ChoosenMenu;
import synrgy.challengethree.model.MenuAbstract;
import synrgy.challengethree.model.Order;

import java.util.ArrayList;
import java.util.List;

@Getter
public class OrderRepository {
    private List<Order> orders;

    public OrderRepository() {
        this.orders = new ArrayList<>();
    }

    public Boolean createOrder(String idOrder, Cart cart) {
        return this.orders.add(new Order(idOrder, cart));
    }

    public Order getOrderById(String id) {
        return orders.stream()
                .filter(order -> order.getIdOrder().equals(id))
                .findFirst()
                .orElse(null);

        // Conventional way to iterate through the orders
        /*for (Order order : orders) {
            if (order.getIdOrder().equals(id)) {
                return order;
            }
        }
        return null;*/
    }

    public Boolean deleteOrderById(String id) {
        return orders.removeIf(order -> order.getIdOrder().equals(id));

        // Conventional way to iterate through the orders
        /*for (Order order : orders) {
            if (order.getIdOrder().equals(id)) {
                orders.remove(order);
                return;
            }
        }*/
    }

    public List<ChoosenMenu> getChoosenMenusByMenuType(Class<? extends MenuAbstract> menuType) {
        return orders.stream()
                .flatMap(order -> order.getMenus().stream().filter(
                        choosenMenu -> menuType.isInstance(choosenMenu.getMenu()))).toList();

        // Conventional way to iterate through the orders
        /*List<ChoosenMenu> choosenMenusByMenuType = new ArrayList<>();
        for (Order order : orders) {
            for (ChoosenMenu choosenMenu : order.getMenus()) {
                if (menuType.isInstance(choosenMenu.getMenu())) {
                    choosenMenusByMenuType.add(choosenMenu);
                }
            }
        }
        return choosenMenusByMenuType;*/
    }
}

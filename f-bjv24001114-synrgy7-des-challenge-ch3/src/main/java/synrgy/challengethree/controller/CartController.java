package synrgy.challengethree.controller;

import synrgy.challengethree.model.Cart;
import synrgy.challengethree.repository.CartRepository;
import synrgy.challengethree.model.ChoosenMenu;
import synrgy.challengethree.model.MenuAbstract;

public class CartController {
    private final CartRepository cartRepository;

    public CartController(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    public void addMenu(MenuAbstract menuAbstract, Integer quantity) {
        ChoosenMenu existingMenu = cartRepository.getMenuById(menuAbstract.getId());
        /*
         * Update the quantity if the menu is already in the cart,
         * otherwise create a new ChoosenMenu object and add it to the cart
         * */
        if (existingMenu != null) {
            cartRepository.updateMenuQuantityById(menuAbstract.getId(), existingMenu.getQuantity() + quantity);
        } else {
            // Build the selected menu
            ChoosenMenu newChoosenMenu = new ChoosenMenu(menuAbstract, quantity);
            this.cartRepository.addMenu(newChoosenMenu);
        }
    }

    public Cart getCart() {
        return this.cartRepository.getCart();
    }

    public Integer getMenuQuantityById(String idMenu) {
        return this.cartRepository.getMenuQuantityById(idMenu);
    }

    public void deleteMenuById(String idMenu) {
        this.cartRepository.deleteMenuById(idMenu);
    }
}

package synrgy.challengethree.controller;

import synrgy.challengethree.model.Cart;
import synrgy.challengethree.model.ChoosenMenu;
import synrgy.challengethree.model.MenuAbstract;
import synrgy.challengethree.model.Order;
import synrgy.challengethree.repository.OrderRepository;

import java.util.List;

public class OrderController {
    private final OrderRepository orderRepository;

    public OrderController(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public Boolean createOrder(String idOrder, Cart cart) {
        return this.orderRepository.createOrder(idOrder, cart);
    }

    public List<Order> getOrders() {
        return this.orderRepository.getOrders();
    }

    public Order getOrderById(String id) {
        return this.orderRepository.getOrderById(id);
    }

    public Boolean deleteOrderById(String id) {
        return this.orderRepository.deleteOrderById(id);
    }

    public List<ChoosenMenu> getChoosenMenusByMenuType(Class<? extends MenuAbstract> orderType) {
        return this.orderRepository.getChoosenMenusByMenuType(orderType);
    }
}
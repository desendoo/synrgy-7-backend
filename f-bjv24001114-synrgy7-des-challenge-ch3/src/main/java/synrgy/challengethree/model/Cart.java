package synrgy.challengethree.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.Optional;

@Data
@AllArgsConstructor
public class Cart {
    private String idCart;
    // List of menu
    private Optional<List<ChoosenMenu>> menus;
}

package synrgy.challengethree.model;


import lombok.Getter;
import lombok.Setter;
import synrgy.challengethree.model.types.SnackSize;

@Getter
@Setter
public class Snack extends MenuAbstract {
    // Size of the snack
    private SnackSize size;

    public Snack(String id, String name, double price,  SnackSize size) {
        super(id, name, price);
        this.size = size;
    }
}

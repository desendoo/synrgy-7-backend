package synrgy.challengethree.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Setter
@Getter
@NoArgsConstructor
public abstract class MenuAbstract {
    private String id;
    private String name;
    private double price;

    protected MenuAbstract(String id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    // Return the type of the menu
    public String getType() {
        return this.getClass().getSimpleName();
    }

}

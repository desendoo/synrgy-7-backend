package synrgy.challengethree.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.List;

@Data
public class Order {
    private String idOrder;
    // List of menu
    private List<ChoosenMenu> menus;
    private ZonedDateTime timeStamp;

    public Order(String idOrder, Cart cart) {
        this.idOrder = idOrder;
        this.menus = cart.getMenus().orElseThrow();
        this.timeStamp = ZonedDateTime.now();
    }

    public double getTotalPrice() {
        return this.menus.stream()
                .mapToDouble(menu -> menu.getMenu().getPrice() * menu.getQuantity())
                .sum();
    }
}

package synrgy.challengethree.model;

import lombok.Getter;
import lombok.Setter;
import synrgy.challengethree.model.types.DrinkSize;

@Getter
@Setter
public class Drink extends MenuAbstract {
    // Size of the drink
    private DrinkSize size;

    public Drink(String id, String name, double price, DrinkSize size) {
        super(id, name, price);
        this.size = size;
    }
}

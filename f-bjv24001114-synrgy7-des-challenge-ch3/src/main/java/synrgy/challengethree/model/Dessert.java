package synrgy.challengethree.model;

import lombok.Getter;
import lombok.Setter;
import synrgy.challengethree.model.types.Flavour;

@Getter
@Setter
public class Dessert extends MenuAbstract {
    // Flavour of the dessert
    private Flavour flavour;

    public Dessert(String id, String name, double price, Flavour flavour) {
        super(id, name, price);
        this.flavour = flavour;
    }
}

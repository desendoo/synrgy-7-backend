-- Users table
CREATE TABLE Users (
    id UUID PRIMARY KEY,
    username VARCHAR(255),
    email_address VARCHAR(255),
    password VARCHAR(255)
);

-- Merchant table
CREATE TABLE Merchant (
    id UUID PRIMARY KEY,
    merchant_name VARCHAR(255),
    merchant_location VARCHAR(255),
    open BOOLEAN
);

-- Product table
CREATE TABLE Product (
    id UUID PRIMARY KEY,
    product_name VARCHAR(255),
    price DECIMAL(10, 2),
    merchant_id UUID,
    FOREIGN KEY (merchant_id) REFERENCES Merchant(id)
);

-- Orders table
CREATE TABLE Orders (
    id UUID PRIMARY KEY,
    order_time TIMESTAMP,
    destination_address VARCHAR(255),
    user_id UUID,
    completed BOOLEAN,
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Order Detail table
CREATE TABLE Order_Detail (
    id UUID PRIMARY KEY,
    order_id UUID,
    product_id UUID,
    quantity INTEGER,
    total_price DECIMAL(10, 2),
    FOREIGN KEY (order_id) REFERENCES Orders(id),
    FOREIGN KEY (product_id) REFERENCES Product(id)
);

-- Insert data into Users
INSERT INTO Users (id, username, email_address, password) VALUES
(gen_random_uuid(), 'user1', 'user1@example.com', 'password1'),
(gen_random_uuid(), 'user2', 'user2@example.com', 'password2'),
(gen_random_uuid(), 'user3', 'user3@example.com', 'password3'),
(gen_random_uuid(), 'user4', 'user4@example.com', 'password4'),
(gen_random_uuid(), 'user5', 'user5@example.com', 'password5');

-- Insert data into Merchant
INSERT INTO Merchant (id, merchant_name, merchant_location, open) VALUES
(gen_random_uuid(), 'Merchant1', 'Location1', TRUE),
(gen_random_uuid(), 'Merchant2', 'Location2', TRUE),
(gen_random_uuid(), 'Merchant3', 'Location3', FALSE),
(gen_random_uuid(), 'Merchant4', 'Location4', TRUE),
(gen_random_uuid(), 'Merchant5', 'Location5', FALSE);

-- Insert data into Product
INSERT INTO Product (id, product_name, price, merchant_id) VALUES
(gen_random_uuid(), 'Product1', 10.99, (SELECT id FROM Merchant WHERE merchant_name = 'Merchant1')),
(gen_random_uuid(), 'Product2', 20.99, (SELECT id FROM Merchant WHERE merchant_name = 'Merchant2')),
(gen_random_uuid(), 'Product3', 30.99, (SELECT id FROM Merchant WHERE merchant_name = 'Merchant3')),
(gen_random_uuid(), 'Product4', 40.99, (SELECT id FROM Merchant WHERE merchant_name = 'Merchant4')),
(gen_random_uuid(), 'Product5', 50.99, (SELECT id FROM Merchant WHERE merchant_name = 'Merchant5'));

-- Insert data into Orders
INSERT INTO Orders (id, order_time, destination_address, user_id, completed) VALUES
(gen_random_uuid(), NOW(), 'Address1', (SELECT id FROM Users WHERE username = 'user1'), TRUE),
(gen_random_uuid(), NOW(), 'Address2', (SELECT id FROM Users WHERE username = 'user2'), FALSE),
(gen_random_uuid(), NOW(), 'Address3', (SELECT id FROM Users WHERE username = 'user3'), TRUE),
(gen_random_uuid(), NOW(), 'Address4', (SELECT id FROM Users WHERE username = 'user4'), FALSE),
(gen_random_uuid(), NOW(), 'Address5', (SELECT id FROM Users WHERE username = 'user5'), TRUE);

-- Insert data into Order_Detail
INSERT INTO Order_Detail (id, order_id, product_id, quantity, total_price) VALUES
(gen_random_uuid(), (SELECT id FROM Orders WHERE destination_address = 'Address1'), (SELECT id FROM Product WHERE product_name = 'Product1'), 2, 21.98),
(gen_random_uuid(), (SELECT id FROM Orders WHERE destination_address = 'Address2'), (SELECT id FROM Product WHERE product_name = 'Product2'), 3, 62.97),
(gen_random_uuid(), (SELECT id FROM Orders WHERE destination_address = 'Address3'), (SELECT id FROM Product WHERE product_name = 'Product3'), 1, 30.99),
(gen_random_uuid(), (SELECT id FROM Orders WHERE destination_address = 'Address4'), (SELECT id FROM Product WHERE product_name = 'Product4'), 5, 204.95),
(gen_random_uuid(), (SELECT id FROM Orders WHERE destination_address = 'Address5'), (SELECT id FROM Product WHERE product_name = 'Product5'), 4, 203.96);
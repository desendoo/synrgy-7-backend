package synrgy.challengethree.model;

import org.junit.jupiter.api.Test;
import synrgy.challengethree.model.Dessert;
import synrgy.challengethree.model.types.Flavour;

import static org.junit.jupiter.api.Assertions.*;

class DessertTest {

    @Test
    void getType_returnsCorrectType() {
        Dessert dessert = new Dessert("1", "Ice Cream", 40000.0, Flavour.STRAWBERRY);
        assertEquals("Dessert", dessert.getType());
    }

    @Test
    void getId_returnsCorrectId() {
        Dessert dessert = new Dessert("1", "Ice Cream", 40000.0, Flavour.STRAWBERRY);
        assertEquals("1", dessert.getId());
    }

    @Test
    void getName_returnsCorrectName() {
        Dessert dessert = new Dessert("1", "Ice Cream", 40000.0, Flavour.STRAWBERRY);
        assertEquals("Ice Cream", dessert.getName());
    }

    @Test
    void getPrice_returnsCorrectPrice() {
        Dessert dessert = new Dessert("1", "Ice Cream", 40000.0, Flavour.STRAWBERRY);
        assertEquals(40000.0, dessert.getPrice());
    }

    @Test
    void getFlavour_returnsCorrectFlavour() {
        Dessert dessert = new Dessert("1", "Ice Cream", 40000.0, Flavour.STRAWBERRY);
        assertEquals(Flavour.STRAWBERRY, dessert.getFlavour());
    }

    @Test
    void setName_updatesName() {
        Dessert dessert = new Dessert("1", "Ice Cream", 40000.0, Flavour.STRAWBERRY);
        dessert.setName("Pie");
        assertEquals("Pie", dessert.getName());
    }

    @Test
    void setPrice_updatesPrice() {
        Dessert dessert = new Dessert("1", "Ice Cream", 40000.0, Flavour.STRAWBERRY);
        dessert.setPrice(60000.0);
        assertEquals(60000.0, dessert.getPrice());
    }

    @Test
    void setFlavour_updatesFlavour() {
        Dessert dessert = new Dessert("1", "Ice Cream", 40000.0, Flavour.STRAWBERRY);
        dessert.setFlavour(Flavour.VANILLA);
        assertEquals(Flavour.VANILLA, dessert.getFlavour());
    }
}
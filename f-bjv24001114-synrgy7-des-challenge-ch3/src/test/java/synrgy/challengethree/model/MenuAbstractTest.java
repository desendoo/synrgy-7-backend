package synrgy.challengethree.model;

import org.junit.jupiter.api.Test;
import synrgy.challengethree.model.MenuAbstract;

import static org.junit.jupiter.api.Assertions.*;

class MenuAbstractTest {

    @Test
    void getType_returnsCorrectTypeForSubclass() {
        class SubMenuTest extends MenuAbstract {
            SubMenuTest(String id, String name, double price) {
                super(id, name, price);
            }
        }
        SubMenuTest subMenu = new SubMenuTest("1", "Pizza", 120000.0);
        assertEquals("SubMenuTest", subMenu.getType());
    }

    @Test
    void getId_returnsCorrectId() {
        Food food = new Food("1", "Pizza", 120000.0);
        assertEquals("1", food.getId());
    }

    @Test
    void getName_returnsCorrectName() {
        Food food = new Food("1", "Pizza", 120000.0);
        assertEquals("Pizza", food.getName());
    }

    @Test
    void getPrice_returnsCorrectPrice() {
        Food food = new Food("1", "Pizza", 120000.0);
        assertEquals(120000.0, food.getPrice());
    }

    @Test
    void setId_updatesId() {
        Food food = new Food("1", "Pizza", 120000.0);
        food.setId("2");
        assertEquals("2", food.getId());
    }

    @Test
    void setName_updatesName() {
        Food food = new Food("1", "Pizza", 120000.0);
        food.setName("Burger");
        assertEquals("Burger", food.getName());
    }

    @Test
    void setPrice_updatesPrice() {
        Food food = new Food("1", "Pizza", 120000.0);
        food.setPrice(15.0);
        assertEquals(15.0, food.getPrice());
    }
}
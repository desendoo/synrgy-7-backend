package synrgy.challengethree.model;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class CartTest {

    @Test
    void getIdCart_returnsCorrectIdCart() {
        Cart cart = new Cart("1", Optional.of(new ArrayList<>()));
        assertEquals("1", cart.getIdCart());
    }

    @Test
    void getMenus_returnsEmptyWhenNoMenus() {
        Cart cart = new Cart("1", Optional.empty());
        assertTrue(cart.getMenus().isEmpty());
    }

    @Test
    void getMenus_returnsCorrectMenus() {
        ChoosenMenu menu = new ChoosenMenu(new Food("1", "Pizza", 120000.0), 2);
        List<ChoosenMenu> menus = Arrays.asList(menu);
        Cart cart = new Cart("1", Optional.of(menus));

        assertEquals(menus, cart.getMenus().get());
    }

    @Test
    void getMenus_returnsEmptyOptionalWhenNoMenus() {
        Cart cart = new Cart("1", Optional.empty());
        assertFalse(cart.getMenus().isPresent());
    }

}
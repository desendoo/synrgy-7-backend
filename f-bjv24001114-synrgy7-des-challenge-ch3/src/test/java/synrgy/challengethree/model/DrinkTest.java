package synrgy.challengethree.model;

import org.junit.jupiter.api.Test;
import synrgy.challengethree.model.types.DrinkSize;

import static org.junit.jupiter.api.Assertions.*;

class DrinkTest {

    @Test
    void getSize_returnsCorrectSize() {
        Drink drink = new Drink("1", "Coke", 12000.0, DrinkSize.LARGE);
        assertEquals(DrinkSize.LARGE, drink.getSize());
    }

    @Test
    void getType_returnsCorrectType() {
        Drink drink = new Drink("1", "Coke", 12000.0, DrinkSize.LARGE);
        assertEquals("Drink", drink.getType());
    }

    @Test
    void getId_returnsCorrectId() {
        Drink drink = new Drink("1", "Coke", 12000.0, DrinkSize.LARGE);
        assertEquals("1", drink.getId());
    }

    @Test
    void getName_returnsCorrectName() {
        Drink drink = new Drink("1", "Coke", 12000.0, DrinkSize.LARGE);
        assertEquals("Coke", drink.getName());
    }

    @Test
    void getPrice_returnsCorrectPrice() {
        Drink drink = new Drink("1", "Coke", 12000.0, DrinkSize.LARGE);
        assertEquals(12000.0, drink.getPrice());
    }

    @Test
    void setSize_updatesSize() {
        Drink drink = new Drink("1", "Coke", 12000.0, DrinkSize.LARGE);
        drink.setSize(DrinkSize.SMALL);
        assertEquals(DrinkSize.SMALL, drink.getSize());
    }
}
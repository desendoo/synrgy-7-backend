package synrgy.challengethree.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import synrgy.challengethree.repository.OrderRepository;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    private Cart cart;

    @BeforeEach
    void setUp() {
        // Add a list of ChoosenMenus to the Cart
        Optional<List<ChoosenMenu>> choosenMenus = Optional.of(Arrays.asList(
                new ChoosenMenu(new Food("1", "Pizza", 120000.0), 2),
                new ChoosenMenu(new Food("2", "Burger", 50000.0), 1)));
        cart = new Cart("1", choosenMenus);
    }

    @Test
    void getIdOrder_returnsCorrectIdOrder() {
        Order order = new Order("1", cart);

        assertEquals("1", order.getIdOrder());
    }

    @Test
    void getMenus_returnsCorrectMenus() {
        Order order = new Order("1", cart);

        assertEquals(cart.getMenus().get(), order.getMenus());
    }

    @Test
    void getTotalPrice_returnsZeroWhenNoMenus() {
        cart.setMenus(Optional.of(new ArrayList<>()));
        Order order = new Order("1", cart);

        assertEquals(0.0, order.getTotalPrice());
    }

    @Test
    void getTotalPrice_returnsCorrectTotalPriceWithMultipleMenus() {
        Order order = new Order("1", cart);

        assertEquals(290000.0, order.getTotalPrice());
    }
}
package synrgy.challengethree.model;

import org.junit.jupiter.api.Test;
import synrgy.challengethree.model.Snack;
import synrgy.challengethree.model.types.SnackSize;

import static org.junit.jupiter.api.Assertions.*;

class SnackTest {
    @Test
    void getSize_returnsCorrectSize() {
        Snack snack = new Snack("1", "Chips", 5000.0, SnackSize.LARGE);
        assertEquals(SnackSize.LARGE, snack.getSize());
    }

    @Test
    void getType_returnsCorrectType() {
        Snack snack = new Snack("1", "Chips", 5000.0, SnackSize.LARGE);
        assertEquals("Snack", snack.getType());
    }

    @Test
    void getId_returnsCorrectId() {
        Snack snack = new Snack("1", "Chips", 5000.0, SnackSize.LARGE);
        assertEquals("1", snack.getId());
    }

    @Test
    void getName_returnsCorrectName() {
        Snack snack = new Snack("1", "Chips", 5000.0, SnackSize.LARGE);
        assertEquals("Chips", snack.getName());
    }

    @Test
    void getPrice_returnsCorrectPrice() {
        Snack snack = new Snack("1", "Chips", 5000.0, SnackSize.LARGE);
        assertEquals(5000.0, snack.getPrice());
    }

    @Test
    void setSize_updatesSize() {
        Snack snack = new Snack("1", "Chips", 5000.0, SnackSize.LARGE);
        snack.setSize(SnackSize.SMALL);
        assertEquals(SnackSize.SMALL, snack.getSize());
    }
}
package synrgy.challengethree.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import synrgy.challengethree.model.Cart;
import synrgy.challengethree.model.Food;
import synrgy.challengethree.model.MenuAbstract;
import synrgy.challengethree.repository.CartRepository;

import static org.junit.jupiter.api.Assertions.*;

class CartControllerTest {
    private CartController cartController;
    private MenuAbstract menu;

    @Test
    void coba() {
        Food pizza = new Food("1", "Pizza", 120000.0);
        System.out.println("PIZZA ENAK " + pizza);
        assertEquals("oke", "oke");
        assertNotEquals(new Food("1", "Pizza", 120000.0), new Food("1", "Pizza", 120000.0));
        assertEquals(pizza, new Food("1", "Pizza", 120000.0));
        assertTrue("oke".equals("oke"));
    }

    @BeforeEach
    void setUp() {
        cartController = new CartController(new CartRepository("CART-1"));
        menu = new Food("1", "Pizza", 120000.0);
    }

    @Test
    void addMenu_addsNewMenuToCart() {
        cartController.addMenu(menu, 2);
        assertEquals(2, cartController.getMenuQuantityById("1"));
    }

    @Test
    void addMenu_updatesQuantityOfExistingMenuInCart() {
        cartController.addMenu(menu, 2);
        cartController.addMenu(menu, 3);
        assertEquals(5, cartController.getMenuQuantityById("1"));
    }

    @Test
    void getCart_returnsCorrectCart() {
        cartController.addMenu(menu, 2);
        Cart cart = cartController.getCart();
        assertEquals(1, cart.getMenus().get().size());
    }

    @Test
    void getMenuQuantityById_returnsCorrectQuantity() {
        cartController.addMenu(menu, 2);
        assertEquals(2, cartController.getMenuQuantityById("1"));
    }

    @Test
    void deleteMenuById_removesMenuFromCart() {
        cartController.addMenu(menu, 2);
        cartController.deleteMenuById("1");
        assertNull(cartController.getMenuQuantityById("123"));
    }

    @Test
    void deleteMenuById_doesNotRemoveMenuWhenIdDoesNotExist() {
        MenuAbstract menu = new Food("1", "Pizza", 120000.0);
        cartController.addMenu(menu, 2);
        cartController.deleteMenuById("2");

        assertEquals(2, cartController.getMenuQuantityById("1"));
    }

    @Test
    void getMenuQuantityById_returnsZeroWhenIdDoesNotExist() {
        assertEquals(0, cartController.getMenuQuantityById("1"));
    }
}
package synrgy.challengethree.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import synrgy.challengethree.model.*;
import synrgy.challengethree.model.types.DrinkSize;
import synrgy.challengethree.model.types.Flavour;
import synrgy.challengethree.model.types.SnackSize;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class OrderRepositoryTest {

    private OrderRepository orderRepository;
    private Cart cart;

    @BeforeEach
    void setUp() {
        orderRepository = new OrderRepository();

        // Add a list of ChoosenMenus to the Cart
        Optional<List<ChoosenMenu>> choosenMenus = Optional.of(Arrays.asList(
                new ChoosenMenu(new Food("1", "Pizza", 120000.0), 2),
                new ChoosenMenu(new Food("2", "Burger", 150000.0), 1)));
        cart = new Cart("1", choosenMenus);
    }

    @Test
    void addOrder_addsOrderToRepository() {
        orderRepository.createOrder("1", cart);
        // Ensure that an Order is only made once
        assertEquals(1, orderRepository.getOrders().size());
    }

    @Test
    void getOrderById_returnsCorrectOrder() {
        orderRepository.createOrder("1", cart);
        assertEquals("1", orderRepository.getOrderById("1").getIdOrder());
    }

    @Test
    void getOrderById_returnsNullForNonExistingOrder() {
        assertNull(orderRepository.getOrderById("1"));
    }

    @Test
    void deleteOrderById_deletesOrder() {
        orderRepository.createOrder("1", cart);
        orderRepository.deleteOrderById("1");
        assertEquals(0, orderRepository.getOrders().size());
    }

    @Test
    void getOrdersByType_returnsCorrectOrders() {
        Food foodMenu = new Food("1", "Pizza", 120000.0);
        Drink drinkMenu = new Drink("2", "Coke", 12000.0, DrinkSize.MEDIUM);
        Drink drinkMenu2 = new Drink("3", "Coke", 12000.0, DrinkSize.SMALL);
        Dessert dessertMenu = new Dessert("4", "Ice Cream", 40000.0, Flavour.CHOCOLATE);
        Dessert dessertMenu2 = new Dessert("5", "Ice Cream", 40000.0, Flavour.STRAWBERRY);
        Dessert dessertMenu3 = new Dessert("6", "Ice Cream", 40000.0, Flavour.VANILLA);
        // Snack snackMenu = new Snack("7", "Chips", 5000.0, SnackSize.EXTRA_LARGE);
        ArrayList<ChoosenMenu> updatedChoosenMenus = new ArrayList<>(Arrays.asList(
                new ChoosenMenu(foodMenu, 2),
                new ChoosenMenu(drinkMenu, 1),
                new ChoosenMenu(drinkMenu2, 1),
                new ChoosenMenu(dessertMenu, 1),
                new ChoosenMenu(dessertMenu2, 1),
                new ChoosenMenu(dessertMenu3, 1)
                // new ChoosenMenu(snackMenu, 1)
        ));
        cart = new Cart("1", Optional.of(updatedChoosenMenus));

        orderRepository.createOrder("1", cart);
        assertEquals(1, orderRepository.getChoosenMenusByMenuType(Food.class).size());
        assertEquals(3, orderRepository.getChoosenMenusByMenuType(Dessert.class).size());
        assertEquals(2, orderRepository.getChoosenMenusByMenuType(Drink.class).size());
        assertEquals(0, orderRepository.getChoosenMenusByMenuType(Snack.class).size());
    }
}
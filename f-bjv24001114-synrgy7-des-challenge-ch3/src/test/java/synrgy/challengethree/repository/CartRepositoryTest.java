package synrgy.challengethree.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import synrgy.challengethree.model.ChoosenMenu;
import synrgy.challengethree.model.Food;

import static org.junit.jupiter.api.Assertions.*;

class CartRepositoryTest {

    private CartRepository cartRepository;

    @BeforeEach
    void setUp() {
        cartRepository = new CartRepository("1");
    }

    @Test
    void addMenu_addsMenuToCart() {
        ChoosenMenu menu = new ChoosenMenu(new Food("1", "Pizza", 120000.0), 2);
        cartRepository.addMenu(menu);
        assertEquals(1, cartRepository.getCart().getMenus().isPresent() ?
                cartRepository.getCart().getMenus().get().size() : 0);
    }

    @Test
    void getMenuQuantityById_returnsCorrectQuantity() {
        ChoosenMenu menu = new ChoosenMenu(new Food("1", "Pizza", 120000.0), 2);
        cartRepository.addMenu(menu);
        assertEquals(2, cartRepository.getMenuQuantityById("1"));
    }

    @Test
    void getMenuQuantityById_returnsZeroForNonExistingMenu() {
        assertEquals(0, cartRepository.getMenuQuantityById("1"));
    }

    @Test
    void getMenuById_returnsCorrectMenu() {
        ChoosenMenu menu = new ChoosenMenu(new Food("1", "Pizza", 120000.0), 2);
        cartRepository.addMenu(menu);
        assertEquals(menu, cartRepository.getMenuById("1"));
    }

    @Test
    void getMenuById_returnsNullForNonExistingMenu() {
        assertNull(cartRepository.getMenuById("1"));
    }

    @Test
    void updateMenuQuantityById_updatesQuantity() {
        ChoosenMenu menu = new ChoosenMenu(new Food("1", "Pizza", 120000.0), 2);
        cartRepository.addMenu(menu);
        cartRepository.updateMenuQuantityById("1", 3);
        assertEquals(3, cartRepository.getMenuQuantityById("1"));
    }

    @Test
    void deleteMenuById_deletesMenu() {
        ChoosenMenu menu = new ChoosenMenu(new Food("1", "Pizza", 120000.0), 2);
        cartRepository.addMenu(menu);
        cartRepository.deleteMenuById("1");
        assertEquals(0, cartRepository.getCart().getMenus().isPresent() ?
                cartRepository.getCart().getMenus().get().size() : 0);
    }
}
package synrgy.challengethree.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import synrgy.challengethree.model.*;
import synrgy.challengethree.model.types.DrinkSize;
import synrgy.challengethree.model.types.Flavour;
import synrgy.challengethree.model.types.SnackSize;

import static org.junit.jupiter.api.Assertions.*;

class MenuRepositoryTest {

    private MenuRepository menuRepository;

    @BeforeEach
    void setUp() {
        menuRepository = new MenuRepository();
    }

    @Test
    void addMenu_addsMenuToRepository() {
        MenuAbstract menu = new Food("1", "Pizza", 120000.0);
        menuRepository.addMenu(menu);
        assertEquals(1, menuRepository.getMenus().size());
    }

    @Test
    void getMenuById_returnsCorrectMenu() {
        MenuAbstract menu = new Food("1", "Pizza", 120000.0);
        menuRepository.addMenu(menu);
        assertEquals(menu, menuRepository.getMenuById("1"));
    }

    @Test
    void getMenuById_returnsNullForNonExistingMenu() {
        assertNull(menuRepository.getMenuById("1"));
    }

    @Test
    void deleteMenuById_deletesMenu() {
        MenuAbstract menu = new Food("1", "Pizza", 120000.0);
        menuRepository.addMenu(menu);
        menuRepository.deleteMenuById("1");
        assertEquals(0, menuRepository.getMenus().size());
    }

    @Test
    void updateMenuById_updatesMenu() {
        MenuAbstract menu = new Food("1", "Pizza", 120000.0);
        menuRepository.addMenu(menu);
        MenuAbstract updatedMenu = new Food("1", "Burger", 150000.0);
        menuRepository.updateMenuById(updatedMenu, "1");
        assertEquals("Burger", menuRepository.getMenuById("1").getName());
        assertEquals(150000.0, menuRepository.getMenuById("1").getPrice());
    }

    @Test
    void getMenusByType_returnsCorrectMenus() {
        Food foodMenu = new Food("1", "Pizza", 120000.0);
        Drink drinkMenu = new Drink("7", "Coke", 12000.0, DrinkSize.MEDIUM);
        Dessert dessertMenu = new Dessert("8", "Ice Cream", 40000.0, Flavour.CHOCOLATE);
        Dessert dessertMenu2 = new Dessert("9", "Ice Cream", 40000.0, Flavour.STRAWBERRY);
        Dessert dessertMenu3 = new Dessert("10", "Ice Cream", 40000.0, Flavour.VANILLA);
        Snack snackMenu = new Snack("11", "Chips", 5000.0, SnackSize.EXTRA_LARGE);

        menuRepository.addMenu(foodMenu);
        menuRepository.addMenu(drinkMenu);
        menuRepository.addMenu(dessertMenu);
        menuRepository.addMenu(dessertMenu2);
        menuRepository.addMenu(dessertMenu3);
        menuRepository.addMenu(snackMenu);
        assertEquals(3, menuRepository.getMenusByType(Dessert.class).size());
    }
}
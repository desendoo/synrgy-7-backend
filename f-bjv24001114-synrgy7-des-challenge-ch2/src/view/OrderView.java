package view;

import model.Order;

import java.io.*;
import java.time.format.DateTimeFormatter;

public class OrderView {

    public static void printOrderDetail(Order order) {
        // Format the date time
        String formmatedDateTime = order.getTimeStamp().format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss"));
        String fileName = "receipt_" + formmatedDateTime + ".txt";

        try {
            // Redirect output to file
            PrintStream originalOut = System.out;
            FileOutputStream fileOut = new FileOutputStream(fileName);
            PrintStream fileStream = new PrintStream(fileOut);
            System.setOut(fileStream);

            // Print the receipt
            printReceipt(order);

            // Close the file stream
            fileStream.close();
            // Reset output stream
            System.setOut(originalOut);

            // Read the receipt file and print it back to the console
            readFromFile(fileName);

            System.out.println("The receipt has been printed in file: " + fileName);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static void printReceipt(Order order) {
        // Print order detail
        System.out.println("=======================================================");
        System.out.println("=                   Order Detail                      =");
        System.out.println("=======================================================");
        System.out.printf("%-25s %-15s %-10s%n", "Menu", "Price (IDR)", "Quantity");
        System.out.println("-------------------------------------------------------");

        order.getMenus().forEach(menu -> System.out.printf("%-25s %-15.2f %-10s%n",
                menu.getMenu().getName(),
                menu.getMenu().getPrice() * menu.getQuantity(),
                " x " + menu.getQuantity()));

        System.out.println("-------------------------------------------------------");
        System.out.printf("%-25s %-15s %-10s%n", "", "Total", order.getTotalPrice());
        System.out.println("-------------------------------------------------------");

        /* Example output:
        =======================================================
        =                   Order Detail                      =
        =======================================================
        Menu                      Price (IDR)     Quantity
        -------------------------------------------------------
        Pizza                     100000.00        x 1
        Ayam Goreng               60000.00         x 4
        Coke                      48000.00         x 4
        Chips                     40000.00         x 8
        Ice Cream                 200000.00        x 5
        -------------------------------------------------------
                                  Total           448000.0
        -------------------------------------------------------
        */
    }

    public static void readFromFile(String fileName) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

import controller.CartController;
import controller.MenuController;
import controller.OrderController;
import model.*;
import model.types.DrinkSize;
import model.types.Flavour;
import model.types.SnackSize;
import view.MenuView;
import view.OrderView;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        binarFudApp();
        // testBinarFud();
    }

    public static void testBinarFud() {
        MenuController menuController = buildMenuController();

        // Get and print a menu by its id
        MenuAbstract menu = menuController.getMenuById("1");
        MenuView.printMenuDetail(menu);

        // Update a menu by its id
        Food updatedFood = new Food("1", "Burger", 8.0);
        menuController.updateMenuById(updatedFood, "1");

        // Delete a menu by its id
        menuController.deleteMenuById("2");

        // List all menus
        menuController.getMenus().forEach(MenuView::printMenuDetail);

        // Get and print menus by type
        Set<MenuAbstract> foodMenus = menuController.getMenusByType(Snack.class);
        for (MenuAbstract m : foodMenus) {
            MenuView.printMenuDetail(m);
        }
    }

    public static void binarFudApp() {
        Scanner input = new Scanner(System.in);
        String idCart = "CART-01";
        String idOrder = "ORDER-01";
        MenuController menuController = buildMenuController();
        OrderController orderController = buildOrderController();
        CartController cartController = buildCartController(idCart);

        Set<MenuAbstract> menus = menuController.getMenus();

        boolean isRunning = true;

        do {
            MenuView.printMenus(menus);
            // Select a menu
            String selectedMenuId = selectMenu(input);
            // Get the selected menu details
            MenuAbstract selectedMenu = menuController.getMenuById(selectedMenuId);
            // Enter quantity
            Integer quantity = enterQuantity(input);
            // Add the selected menu to the cart.
            cartController.addMenu(selectedMenu, quantity);
            // Continue choosing menu or not
            isRunning = isChoosingMenuContinue(input);
            if (!isRunning) { // If not continue choosing menu
                // Move the choosen menu(s) from cart to order
                Order order = orderController.createOrder(idOrder, cartController.getCart());
                // Print the order
                OrderView.printOrderDetail(order);
                // End the program
                isRunning = false;
            }
        } while (isRunning);

        input.close();
    }

    private static String selectMenu(Scanner input) {
        String selectedMenu;
        do {
            try {
                System.out.println("Please choose a menu by entering the menu ID: ");
                System.out.print(" => ");
                selectedMenu = input.next();

                // Break the loop if the input is a valid String
                if (selectedMenu.matches("^[a-zA-Z0-9]*$")) {
                    break;
                }

                throw new IllegalArgumentException("[WARNING] Please enter a valid menu ID (String)! ");
            } catch (IllegalArgumentException e) {
                System.err.println(e.getMessage());
            }
        } while (true);

        return selectedMenu;
    }

    private static Integer enterQuantity(Scanner input) {
        Integer quantity;
        do {
            try {
                System.out.println("Please enter the desired menu quantity: ");
                System.out.print(" => ");
                quantity = input.nextInt();

                // Break the loop if the input is a valid positive integer
                if (quantity > 0) {
                    break;
                }

                throw new IllegalArgumentException("[WARNING] Please enter a valid quantity (positive integer)! ");
            } catch (InputMismatchException e) {
                System.err.println("[WARNING] Please enter a valid integer number!");
                input.next();
            } catch (IllegalArgumentException e) {
                System.err.println(e.getMessage());
            }
        } while (true);

        return quantity;
    }

    private static boolean isChoosingMenuContinue(Scanner input) {
        String answer;
        do {
            try {
                System.out.println("Do you want to continue choosing menu? (Y=Continue / N=Pay)");
                System.out.print(" => ");
                answer = input.next();

                // Break the loop if the input is a valid response
                if (answer.equalsIgnoreCase("Y") || answer.equalsIgnoreCase("N")) {
                    break;
                }

                throw new IllegalArgumentException("[WARNING] Please enter a valid response (Y/N)! ");
            } catch (IllegalArgumentException e) {
                System.err.println(e.getMessage());
            }
        } while (true);

        // Only recognize Y=true, otherwise N=false
        return answer.equalsIgnoreCase("Y");
    }

    private static OrderController buildOrderController() {
        OrderRepository orderRepository = new OrderRepository();

        return new OrderController(orderRepository);
    }

    private static CartController buildCartController(String idCart) {
        CartRepository cartRepository = new CartRepository(idCart);

        return new CartController(cartRepository);
    }

    private static MenuController buildMenuController() {
        MenuRepository menuRepository = new MenuRepository();
        MenuController menuController = new MenuController(menuRepository);

        // Create instances of Food, Drink, Dessert, and Snack classes
        List<Food> foods = Arrays.asList(
                new Food("1", "Pizza", 100000.0),
                new Food("2", "Ayam Goreng", 15000.0),
                new Food("3", "Nasi Goreng", 18000.0),
                new Food("4", "Nasi Goreng Spesial", 22000.0),
                new Food("5", "Bakmi Goreng", 20000.0),
                new Food("6", "Bakmi Goreng Spesial", 24000.0)
        );
        Drink drink = new Drink("7", "Coke", 12000.0, DrinkSize.MEDIUM);
        Dessert dessert = new Dessert("8", "Ice Cream", 40000.0, Flavour.CHOCOLATE);
        Snack snack = new Snack("9", "Chips", 5000.0, SnackSize.EXTRA_LARGE);

        // Add these instances to the menu
        foods.forEach(menuController::addMenu);
        menuController.addMenu(drink);
        menuController.addMenu(dessert);
        menuController.addMenu(snack);

        return menuController;
    }
}
package model;

public class Food extends MenuAbstract {
    public Food(String id, String name, double price) {
        super(id, name, price);
    }
}
package model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class OrderRepository {
    private List<Order> orders;

    public OrderRepository() {
        this.orders = new ArrayList<>();
    }

    public void addOrder(Order order) {
        this.orders.add(order);
    }

    public Order getOrderById(String id) {
        for (Order order : orders) {
            if (order.getIdOrder().equals(id)) {
                return order;
            }
        }
        return null;
    }

    public void deleteOrderById(String id) {
        for (Order order : orders) {
            if (order.getIdOrder().equals(id)) {
                orders.remove(order);
                return;
            }
        }
    }

    public void updateOrderById(Order order, String id) {
        for (Order ord : orders) {
            if (ord.getIdOrder().equals(id)) {
                ord.setIdOrder(order.getIdOrder());
                ord.setMenus(order.getMenus());
                break;
            }
        }
    }

    public List<Order> getOrdersByType(Class<? extends Order> orderType) {
        List<Order> ordersByType = new ArrayList<>();
        for (Order order : orders) {
            if (orderType.isInstance(order)) {
                ordersByType.add(order);
            }
        }
        return ordersByType;
    }
}

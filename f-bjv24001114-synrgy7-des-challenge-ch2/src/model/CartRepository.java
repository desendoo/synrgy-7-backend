package model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
public class CartRepository {
    private Cart cart;

    public CartRepository(String idCart) {
        this.cart = new Cart(idCart, new ArrayList<>());
    }

    public void addMenu(ChoosenMenu menu) {
        this.cart.getMenus().add(menu);
    }

    public Integer getMenuQuantityById(String idMenu) {
        return this.cart.getMenus().stream()
                .filter(menu -> menu.getMenu().getId().equals(idMenu))
                .findFirst()
                .map(ChoosenMenu::getQuantity)
                .orElse(0);
    }

    // Example of Optional.flatMap use case
    public String getMenuNameInUppercase(String idMenu) {
        return this.cart.getMenus().stream()
                .filter(menu -> menu.getMenu().getId().equals(idMenu))
                .findFirst()
                .flatMap(menu -> Optional.of(menu.getMenu().getName().toUpperCase()))
                .orElse("Menu not found");
    }

    // Example of Optional.orElse use case
    public List<String> getMenuNames() {
        return this.cart.getMenus().stream()
                .map(menu -> menu.getMenu().getName())
                .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
    }
    // Example of Optional.orElse use case
    public List<String> getMenuNamesInLowercase() {
        return this.cart.getMenus().stream()
                .map(menu -> menu.getMenu().getName().toLowerCase())
                .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
    }

    public ChoosenMenu getMenuById(String idMenu) {
        return this.cart.getMenus().stream()
                .filter(menu -> menu.getMenu().getId().equals(idMenu))
                .findFirst()
                .orElse(null);
    }

    public void updateMenuQuantityById(String idMenu, Integer quantity) {
        this.cart.getMenus().stream()
                .filter(menu -> menu.getMenu().getId().equals(idMenu))
                .findFirst()
                .ifPresent(menu -> menu.setQuantity(quantity));
    }

    public void deleteMenuById(String idMenu) {
        this.cart.getMenus().removeIf(menu -> menu.getMenu().getId().equals(idMenu));
    }
}
package model;

import lombok.Data;

import java.util.Objects;

@Data
public abstract class MenuAbstract {
    private String id;
    private String name;
    private double price;

    protected MenuAbstract() {}

    protected MenuAbstract(String id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    // Return the type of the menu
    public String getType() {
        return this.getClass().getSimpleName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MenuAbstract that = (MenuAbstract) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}

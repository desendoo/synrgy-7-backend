package model.types;

public enum SnackSize {
    // Size of the snack
    //      Example: SMALL, MEDIUM, LARGE, EXTRA_LARGE
    SMALL, MEDIUM, LARGE, EXTRA_LARGE
}

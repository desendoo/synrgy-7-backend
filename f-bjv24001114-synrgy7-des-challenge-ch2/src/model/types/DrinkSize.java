package model.types;

public enum DrinkSize {
    // Size of the drink
    //      Example: SMALL, MEDIUM, LARGE
    SMALL, MEDIUM, LARGE
}

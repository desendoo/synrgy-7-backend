package model.types;

public enum Flavour {
    // Flavour of the dessert
    //      Example: CHOCOLATE, VANILLA, STRAWBERRY
    CHOCOLATE, VANILLA, STRAWBERRY
}

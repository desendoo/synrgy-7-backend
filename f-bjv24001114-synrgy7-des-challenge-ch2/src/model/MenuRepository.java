package model;

import lombok.Data;

import java.util.LinkedHashSet;
import java.util.Set;

@Data
public class MenuRepository {
    private Set<MenuAbstract> menus;

    public MenuRepository() {
        this.menus = new LinkedHashSet<>();
    }

    public void addMenu(MenuAbstract menu) {
        this.menus.add(menu);
    }

    public MenuAbstract getMenuById(String id) {
        for (MenuAbstract menu : menus) {
            if (menu.getId().equals(id)) {
                return menu;
            }
        }
        return null;
    }

    public boolean deleteMenuById(String id) {
        for (MenuAbstract menu : menus) {
            if (menu.getId().equals(id)) {
                menus.remove(menu);
                return true;
            }
        }
        return false;
    }

    public boolean updateMenuById(MenuAbstract menu, String id) {
        for (MenuAbstract m : menus) {
            if (m.getId().equals(id)) {
                m.setName(menu.getName());
                m.setId(menu.getId());
                m.setPrice(menu.getPrice());
                return true;
            }
        }
        return false;
    }

    public Set<MenuAbstract> getMenusByType(Class<? extends MenuAbstract> menuType) {
        Set<MenuAbstract> menusByFoodType = new LinkedHashSet<>();
        for (MenuAbstract menu : menus) {
            if (menuType.isInstance(menu)) {
                menusByFoodType.add(menu);
            }
        }
        return menusByFoodType;
    }
}
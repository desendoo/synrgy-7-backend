package model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ChoosenMenu {
    private MenuAbstract menu;
    private Integer quantity;
}

package model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import model.types.SnackSize;

@EqualsAndHashCode(callSuper = true)
@Data
public class Snack extends MenuAbstract {
    // Size of the snack
    private SnackSize size;

    public Snack(String id, String name, double price,  SnackSize size) {
        super(id, name, price);
        this.size = size;
    }
}

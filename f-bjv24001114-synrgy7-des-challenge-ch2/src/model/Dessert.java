package model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import model.types.Flavour;

@EqualsAndHashCode(callSuper = true)
@Data
public class Dessert extends MenuAbstract {
    // Flavour of the dessert
    private Flavour flavour;

    public Dessert(String id, String name, double price, Flavour flavour) {
        super(id, name, price);
        this.flavour = flavour;
    }
}

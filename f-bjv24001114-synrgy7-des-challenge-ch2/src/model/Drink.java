package model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import model.types.DrinkSize;

@EqualsAndHashCode(callSuper = true)
@Data
public class Drink extends MenuAbstract {
    // Size of the drink
    private DrinkSize size;

    public Drink(String id, String name, double price, DrinkSize size) {
        super(id, name, price);
        this.size = size;
    }
}

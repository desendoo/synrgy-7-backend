package model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Cart {
    private String idCart;
    // List of menu
    private List<ChoosenMenu> menus;
}

package controller;

import model.Cart;
import model.Order;
import model.OrderRepository;

import java.util.List;

public class OrderController {
    private final OrderRepository orderRepository;

    public OrderController(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    // Create an order
    public Order createOrder(String idOrder, Cart cart) {
        return new Order(idOrder, cart);
    }

    public List<Order> getOrders() {
        return this.orderRepository.getOrders();
    }

    public void addOrder(Order order) {
        this.orderRepository.addOrder(order);
    }

    public Order getOrderById(String id) {
        return this.orderRepository.getOrderById(id);
    }

    public void deleteOrderById(String id) {
        this.orderRepository.deleteOrderById(id);
    }

    public void updateOrderById(Order order, String id) {
        this.orderRepository.updateOrderById(order, id);
    }

    public List<Order> getOrdersByType(Class<? extends Order> orderType) {
        return this.orderRepository.getOrdersByType(orderType);
    }
}
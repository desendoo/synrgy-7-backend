package controller;

import model.MenuAbstract;
import model.MenuRepository;

import java.util.Set;

public class MenuController {
    private final MenuRepository menuRepository;

    public MenuController(MenuRepository menuRepository) {
        this.menuRepository = menuRepository;
    }

    public Set<MenuAbstract> getMenus() {
        return this.menuRepository.getMenus();
    }

    public void addMenu(MenuAbstract menu) {
        this.menuRepository.addMenu(menu);
    }

    public MenuAbstract getMenuById(String id) {
        return this.menuRepository.getMenuById(id);
    }

    public boolean deleteMenuById(String id) {
        return this.menuRepository.deleteMenuById(id);
    }

    public boolean updateMenuById(MenuAbstract menu, String id) {
        return this.menuRepository.updateMenuById(menu, id);
    }

    public Set<MenuAbstract> getMenusByType(Class<? extends MenuAbstract> menuType) {
        return this.menuRepository.getMenusByType(menuType);
    }
}
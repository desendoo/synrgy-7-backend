package synrgy.challengefour;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@OpenAPIDefinition
@SpringBootApplication
public class ChallengefourApplication {

	private static final Logger logger = LoggerFactory.getLogger(ChallengefourApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ChallengefourApplication.class, args);
	}

}

package synrgy.challengefour.controller;

import jakarta.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import synrgy.challengefour.model.entity.Product;
import synrgy.challengefour.model.request.product.ProductCreateModel;
import synrgy.challengefour.model.standard.StandardResponseModel;
import synrgy.challengefour.service.ProductService;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/v1")
public class ProductController {

    private final ProductService<Product> productService;

    public ProductController(ProductService<Product> productService) {
        this.productService = productService;
    }

    @GetMapping("/products")
    public ResponseEntity<StandardResponseModel> getProducts(
            @RequestParam(required = false, name = "name") String name,
            @PageableDefault(page = 0, size = 5, sort = {"id"}) Pageable pageable
            // @RequestParam(value = "page", defaultValue = "0") int page,
            // @RequestParam(value = "size", defaultValue = "5") int size
    ) {
        StandardResponseModel response = productService.list(name, pageable);

        return ResponseEntity
                .status(HttpStatus.valueOf(response.getStatusCode()))
                .body(response);
    }

    @PostMapping("/products")
    public ResponseEntity<StandardResponseModel> saveProduct(
            @Valid @RequestBody ProductCreateModel request
            , BindingResult bindingResult
    ) {
        StandardResponseModel response = new StandardResponseModel();

        if (bindingResult.hasErrors()) {
            Map<String, String> errors = new HashMap<>();
            bindingResult.getFieldErrors().forEach(error -> errors.put(error.getField(), error.getDefaultMessage()));

            errors.forEach(response::putError);
            response.setStatusCode(HttpStatus.BAD_REQUEST.value());
        } else {
            response = productService.save(request);
        }

        return ResponseEntity
                .status(response.getStatusCode())
                .body(response);
    }

}

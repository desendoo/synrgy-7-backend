package synrgy.challengefour.controller;

import jakarta.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import synrgy.challengefour.model.entity.Merchant;
import synrgy.challengefour.model.request.merchant.MerchantCreateModel;
import synrgy.challengefour.model.standard.StandardResponseModel;
import synrgy.challengefour.service.MerchantService;

@RestController
@RequestMapping("/v1")
public class MerchantController {

    private final MerchantService<Merchant> merchantService;

    public MerchantController(MerchantService<Merchant> merchantService) {
        this.merchantService = merchantService;
    }

    @GetMapping("/merchants")
    public ResponseEntity<StandardResponseModel> getMerchants(
            @RequestParam(required = false, name = "open") Boolean open,
            @PageableDefault(page = 0, size = 5, sort = {"id"}) Pageable pageable
    ) {
        StandardResponseModel response = merchantService.list(open, pageable);

        return ResponseEntity
                .status(response.getStatusCode())
                .body(response);
    }

    @PostMapping("/merchants")
    public ResponseEntity<StandardResponseModel> saveMerchant(
            @Valid @RequestBody MerchantCreateModel request
            ) {
        StandardResponseModel response = merchantService.save(request);

        return ResponseEntity
                .status(response.getStatusCode())
                .body(response);
    }

}

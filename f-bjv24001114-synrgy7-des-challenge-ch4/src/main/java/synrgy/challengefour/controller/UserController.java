package synrgy.challengefour.controller;

import org.json.JSONArray;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import synrgy.challengefour.model.standard.StandardResponseModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class UserController {

    private RestTemplate restTemplate = new RestTemplate();

    @GetMapping("/v1/objects")
    public ResponseEntity<StandardResponseModel> index() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");

        String url = "https://api.restful-api.dev/objects?id=2";
        // Call with string query parameter
        Object restResponse = restTemplate.exchange(
                url,
                HttpMethod.GET,
                new HttpEntity<>(headers),
                Object.class).getBody();

        JSONArray jsonArray = new JSONArray(restResponse.toString());

        StandardResponseModel response = new StandardResponseModel();
        response.setStatusCode(HttpStatus.OK.value());
        response.putData("data", restResponse);

        return ResponseEntity
                .status(HttpStatus.valueOf(response.getStatusCode()))
                .body(response);
    }

    @GetMapping("/v1/users")
    public ResponseEntity<StandardResponseModel> getUsers() {
        Map<String, String> user = new HashMap<>();
        user.put("name", "John Doe");
        user.put("email", "johndoe@mail.com");
        Map<String, String> user2 = new HashMap<>();
        user2.put("name", "Will Smith");
        user2.put("email", "willsmith@mail.com");

        List<Map<String, String>> users = new ArrayList<>();
        users.add(user);
        users.add(user2);

        StandardResponseModel response = new StandardResponseModel();
        response.setStatusCode(HttpStatus.OK.value());
        response.putData("users", users);

        return ResponseEntity
                .status(HttpStatus.valueOf(response.getStatusCode()))
                .body(response);
    }

    @PostMapping("/v1/users")
    public ResponseEntity<StandardResponseModel> createUser() {
        Map<String, Object> user = new HashMap<>();
        user.put("name", "John Doe");
        user.put("email", "johndoe@mail.com");

        StandardResponseModel response = new StandardResponseModel();
        response.setStatusCode(HttpStatus.CREATED.value());
        response.setData(user);

        return ResponseEntity
                .status(HttpStatus.valueOf(response.getStatusCode()))
                .body(response);
    }

}

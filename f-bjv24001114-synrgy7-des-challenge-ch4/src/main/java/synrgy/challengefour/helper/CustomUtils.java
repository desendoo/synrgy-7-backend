package synrgy.challengefour.helper;

import org.springframework.data.domain.Page;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;

public final class CustomUtils {

    public static String generateRandomString(int length) {
        SecureRandom secureRandom = new SecureRandom();
        byte[] randomBytes = new byte[length];
        secureRandom.nextBytes(randomBytes);

        // Convert random bytes to a Base64-encoded string
        String secureString = Base64.getUrlEncoder().withoutPadding().encodeToString(randomBytes);

        // Ensure the string has the desired length
        return secureString.substring(0, length);
    }

    public static Long generateId() {
        return (long) (new Random().nextLong() * (Long.MAX_VALUE - 1));
    }

    public static Date getCurrentDate() {
        return new Date();
    }

    public static ZonedDateTime getCurrentTimeUtc() {
        return ZonedDateTime.now(ZoneOffset.UTC);
    }

    public static ZonedDateTime getCurrentTimeAsiaJakarta() {
        ZoneId targetTimeZone = ZoneId.of("Asia/Jakarta");
        return ZonedDateTime.now(targetTimeZone);
    }

    public static String getDateInString() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("ddMyyyyhhmmss");
        String strDate = formatter.format(date);

        return strDate;
    }

    public static int generate6DigitNumber() {
        SecureRandom random = new SecureRandom();
        // Generate a random 6-digit number
        return 100000 + random.nextInt(900000);
    }

    public static String generate6DigitNumberInString() {
        return Integer.toString(generate6DigitNumber());
    }

}
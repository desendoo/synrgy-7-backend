package synrgy.challengefour.helper;

import org.springframework.data.domain.Page;
import synrgy.challengefour.model.dto.*;
import synrgy.challengefour.model.entity.*;

import java.util.HashMap;
import java.util.Map;

public final class CustomMapper {

    public static OrderDetailDTO toOrderDetailDTO(OrderDetail orderDetail) {
        return OrderDetailDTO.builder()
                .id(orderDetail.getId())
                .quantity(orderDetail.getQuantity())
                .order(toOrderDTO(orderDetail.getIdOrder()))
                .product(toProductDTO(orderDetail.getIdProduct()))
                .build();
    }

    public static OrderDTO toOrderDTO(Order order) {
        return OrderDTO.builder()
                .id(order.getId())
                .orderTime(order.getOrderTime())
                .destinationAddress(order.getDestinationAddress())
                .completed(order.getCompleted())
                .idUser(order.getIdUser().getId())
                .build();
    }

    public static UserDTO toUserDTO(User user) {
        return UserDTO.builder()
                .id(user.getId())
                .username(user.getUsername())
                .emailAddress(user.getEmailAddress())
                .build();
    }

    public static ProductDTO toProductDTO(Product product) {
        return ProductDTO.builder()
                .id(product.getId())
                .name(product.getName())
                .price(product.getPrice())
                .idMerchant(product.getIdMerchant().getId())
                .build();
    }

    public static MerchantDTO toMerchantDTO(Merchant merchant) {
        return MerchantDTO.builder()
                .id(merchant.getId())
                .name(merchant.getName())
                .location(merchant.getLocation())
                .open(merchant.getOpen())
                .build();
    }

    public static Map<String, Object> pageableToMap(Page<?> page) {
        Map<String, Object> pageableMap = new HashMap<>();
        pageableMap.put("pageable", page.getPageable());
        pageableMap.put("totalPages", page.getTotalPages());
        pageableMap.put("totalElements", page.getTotalElements());
        pageableMap.put("last", page.isLast());
        pageableMap.put("size", page.getSize());
        pageableMap.put("number", page.getNumber());
        pageableMap.put("sort", page.getSort());
        pageableMap.put("numberOfElements", page.getNumberOfElements());
        pageableMap.put("first", page.isFirst());
        pageableMap.put("empty", page.isEmpty());

        return pageableMap;
    }
}
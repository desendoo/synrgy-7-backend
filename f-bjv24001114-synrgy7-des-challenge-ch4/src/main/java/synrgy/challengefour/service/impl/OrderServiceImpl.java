package synrgy.challengefour.service.impl;

import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import synrgy.challengefour.error.exception.ExtendedEntityNotFoundException;
import synrgy.challengefour.error.exception.InternalServerErrorException;
import synrgy.challengefour.helper.CustomMapper;
import synrgy.challengefour.model.dto.ProductDTO;
import synrgy.challengefour.model.entity.Order;
import synrgy.challengefour.model.entity.OrderDetail;
import synrgy.challengefour.model.entity.Product;
import synrgy.challengefour.model.entity.User;
import synrgy.challengefour.model.request.order.OrderCreateModel;
import synrgy.challengefour.model.request.order.OrderDetailCreateModel;
import synrgy.challengefour.model.standard.StandardResponseModel;
import synrgy.challengefour.repository.OrderDetailRepository;
import synrgy.challengefour.repository.OrderRepository;
import synrgy.challengefour.repository.UserRepository;
import synrgy.challengefour.service.OrderDetailService;
import synrgy.challengefour.service.OrderService;
import synrgy.challengefour.service.ProductService;

import java.util.Date;

@Service
public class OrderServiceImpl<T> implements OrderService<T>, OrderDetailService<T> {

    private final OrderRepository orderRepository;
    private final UserRepository userRepository;
    private final OrderDetailRepository orderDetailRepository;
    private final ProductService<Product> productService;

    public OrderServiceImpl(OrderRepository orderRepository, UserRepository userRepository, OrderDetailRepository orderDetailRepository, ProductService<Product> productService) {
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
        this.orderDetailRepository = orderDetailRepository;
        this.productService = productService;
    }

    @Override
    public StandardResponseModel list(Pageable pageable) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            Page<Order> orders = orderRepository.findAll(pageable);

            response.putData("orders", orders.getContent().stream().map(CustomMapper::toOrderDTO));
            response.setPageableData(orders);
            response.success();

            return response;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @Override
    @Transactional
    public StandardResponseModel save(OrderCreateModel request) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            if (!userRepository.existsById(request.getIdUser())) {
                throw new ExtendedEntityNotFoundException(User.class);
            }

            Order order = Order.builder()
                    .orderTime(new Date())
                    .destinationAddress(request.getDestinationAddress())
                    .idUser(userRepository.getReferenceById(request.getIdUser()))
                    .build();

            Order result = orderRepository.save(order);

            response.putData("order", CustomMapper.toOrderDTO(result));
            response.putMessageStatus("Success save order");
            response.created();

            return response;
        } catch (EntityNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @Override
    @Transactional
    public StandardResponseModel save(OrderDetailCreateModel request) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            if (!orderRepository.existsById(request.getIdOrder())) {
                throw new ExtendedEntityNotFoundException(Order.class);
            }

            Product fetchedProduct = productService.fetchProductById(request.getIdProduct());
            if (fetchedProduct == null) {
                throw new ExtendedEntityNotFoundException(Product.class);
            }

            double totalPrice = request.getQuantity() * fetchedProduct.getPrice();
            OrderDetail orderDetail = OrderDetail.builder()
                    .idOrder(orderRepository.getReferenceById(request.getIdOrder()))
                    .idProduct(fetchedProduct)
                    .quantity(request.getQuantity())
                    .totalPrice(totalPrice)
                    .build();

            OrderDetail result = orderDetailRepository.save(orderDetail);

            response.putData("orderDetail", CustomMapper.toOrderDetailDTO(result));
            response.putMessageStatus("Success save order detail");
            response.created();

            return response;
        } catch (EntityNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

}
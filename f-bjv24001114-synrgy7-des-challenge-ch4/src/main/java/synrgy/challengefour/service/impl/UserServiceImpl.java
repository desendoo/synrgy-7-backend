package synrgy.challengefour.service.impl;

import jakarta.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import synrgy.challengefour.error.exception.ExtendedEntityNotFoundException;
import synrgy.challengefour.error.exception.InternalServerErrorException;
import synrgy.challengefour.helper.CustomMapper;
import synrgy.challengefour.model.entity.User;
import synrgy.challengefour.model.request.user.UserCreateModel;
import synrgy.challengefour.model.request.user.UserEditRequestModel;
import synrgy.challengefour.model.standard.StandardResponseModel;
import synrgy.challengefour.repository.UserRepository;
import synrgy.challengefour.service.UserService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService<User> {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public StandardResponseModel list(Pageable pageable) {
        StandardResponseModel response = new StandardResponseModel();
        try {
            Page<User> users = userRepository.findAll(pageable);

            response.putData("users", users.getContent().stream().map(CustomMapper::toUserDTO));
            response.setPageableData(users);
            response.success();

            return response;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @Override
    @Transactional
    public StandardResponseModel save(UserCreateModel request) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            User user = User.builder()
                    .username(request.getUsername())
                    .emailAddress(request.getEmailAddress())
                    .password(request.getPassword())
                    .build();

            User result = userRepository.save(user);

            response.putData("user", CustomMapper.toUserDTO(result));
            response.created();

            return response;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @Override
    public StandardResponseModel edit(UserEditRequestModel request) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            Optional<User> foundUser = userRepository.findById(request.getIdUser());

            foundUser.ifPresentOrElse(user -> {
                user.setUsername(request.getUsername());
                user.setEmailAddress(request.getEmailAddress());
                user.setPassword(request.getPassword());

                User result = userRepository.save(user);

                response.putData("user", CustomMapper.toUserDTO(result));
                response.success();
            }, () -> {
                throw new ExtendedEntityNotFoundException(User.class);
            });

            return response;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @Override
    @Transactional
    public StandardResponseModel delete(UUID idUser) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            userRepository.findById(idUser).ifPresentOrElse(user -> {
                userRepository.delete(user);

                response.success();
                response.putMessageStatus("Success delete user");
            }, () -> {
                throw new ExtendedEntityNotFoundException(User.class);
            });

            return response;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }
}
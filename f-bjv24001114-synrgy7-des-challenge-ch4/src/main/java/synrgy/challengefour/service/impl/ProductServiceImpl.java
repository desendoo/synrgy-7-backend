package synrgy.challengefour.service.impl;

import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import synrgy.challengefour.error.exception.ExtendedEntityNotFoundException;
import synrgy.challengefour.error.exception.InternalServerErrorException;
import synrgy.challengefour.helper.CustomMapper;
import synrgy.challengefour.model.dto.ProductDTO;
import synrgy.challengefour.model.entity.Merchant;
import synrgy.challengefour.model.entity.Product;
import synrgy.challengefour.model.request.product.ProductCreateModel;
import synrgy.challengefour.model.request.product.ProductEditRequestModel;
import synrgy.challengefour.model.standard.StandardResponseModel;
import synrgy.challengefour.repository.MerchantRepository;
import synrgy.challengefour.repository.ProductRepository;
import synrgy.challengefour.service.ProductService;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductServiceImpl implements ProductService<Product> {

    private final ProductRepository productRepository;
    private final MerchantRepository merchantRepository;

    public ProductServiceImpl(ProductRepository productRepository, MerchantRepository merchantRepository) {
        this.productRepository = productRepository;
        this.merchantRepository = merchantRepository;
    }

    @Override
    public StandardResponseModel list(String name, Pageable pageable) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            Specification<Product> specification = (root, query, criteriaBuilder) -> {
                if (name != null) {
                    return criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + name.toLowerCase() + "%");
                }
                return null;
            };

            Page<Product> products = productRepository.findAll(specification, pageable);

            response.putData("products", products.getContent().stream().map(CustomMapper::toProductDTO));
            response.setPageableData(products);
            response.success(HttpStatus.OK.value());

            return response;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @Override
    @Transactional
    public StandardResponseModel save(ProductCreateModel request) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            if (!merchantRepository.existsById(request.getIdMerchant())) {
                throw new ExtendedEntityNotFoundException(Merchant.class);
            }

            Product product = Product.builder()
                    .name(request.getName())
                    .price(request.getPrice())
                    .idMerchant(merchantRepository.getReferenceById(request.getIdMerchant()))
                    .build();

            Product result = productRepository.save(product);

            response.putData("product", CustomMapper.toProductDTO(result));
            response.putMessageStatus("Success save product");
            response.created();

            return response;
        } catch (EntityNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @Override
    @Transactional
    public StandardResponseModel edit(ProductEditRequestModel request) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            Optional<Product> foundProduct = productRepository.findById(request.getIdProduct());

            foundProduct.ifPresentOrElse(product -> {
                product.setName(request.getName());
                product.setPrice(request.getPrice());
                product.setUpdatedDate(new Date());

                Product result = productRepository.save(product);

                response.putData("product", CustomMapper.toProductDTO(result));
                response.success();
                response.putMessageStatus("Success edit product");
            }, () -> {
                throw new ExtendedEntityNotFoundException(Product.class);
            });

            return response;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @Override
    @Transactional
    public StandardResponseModel delete(UUID idProduct) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            Optional<Product> foundProduct = productRepository.findById(idProduct);

            foundProduct.ifPresentOrElse(product -> {
                productRepository.delete(product);

                response.success();
                response.putMessageStatus("Success delete product");
            }, () -> {
                throw new ExtendedEntityNotFoundException(Product.class);
            });

            return response;
        } catch (ExtendedEntityNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @Override
    public Product fetchProductById(UUID idProduct) {
        return productRepository.findById(idProduct).orElse(null);
    }

}

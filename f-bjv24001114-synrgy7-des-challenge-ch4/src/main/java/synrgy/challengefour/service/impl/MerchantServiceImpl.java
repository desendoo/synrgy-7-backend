package synrgy.challengefour.service.impl;

import jakarta.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import synrgy.challengefour.error.exception.ExtendedEntityNotFoundException;
import synrgy.challengefour.error.exception.InternalServerErrorException;
import synrgy.challengefour.helper.CustomMapper;
import synrgy.challengefour.model.entity.Merchant;
import synrgy.challengefour.model.request.merchant.MerchantCreateModel;
import synrgy.challengefour.model.standard.StandardResponseModel;
import synrgy.challengefour.repository.MerchantRepository;
import synrgy.challengefour.service.MerchantService;

import java.util.Optional;
import java.util.UUID;

@Service
public class MerchantServiceImpl implements MerchantService<Merchant> {

    private final MerchantRepository merchantRepository;

    public MerchantServiceImpl(MerchantRepository merchantRepository) {
        this.merchantRepository = merchantRepository;
    }

    @Override
    public StandardResponseModel list(Boolean open, Pageable pageable) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            Specification<Merchant> specification = (root, query, criteriaBuilder) -> {
                if (open != null) {
                    return criteriaBuilder.equal(root.get("open"), open);
                }
                return null;
            };

            Page<Merchant> merchants = merchantRepository.findAll(specification, pageable);

            response.putData("merchants", merchants.getContent().stream().map(CustomMapper::toMerchantDTO));
            response.setPageableData(merchants);
            response.success(HttpStatus.OK.value());

            return response;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @Override
    @Transactional
    public StandardResponseModel save(MerchantCreateModel request) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            Merchant merchant = Merchant.builder()
                    .name(request.getName())
                    .location(request.getLocation())
                    .open(request.getOpen())
                    .build();

            Merchant result = merchantRepository.save(merchant);

            response.putData("merchant", CustomMapper.toMerchantDTO(result));
            response.success(HttpStatus.CREATED.value());
            response.putMessageStatus("Success save merchant");

            return response;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @Override
    public StandardResponseModel editOpen(UUID idMerchant, boolean openState) {
        StandardResponseModel response = new StandardResponseModel();

        try {
            Optional<Merchant> foundMerchant = merchantRepository.findById(idMerchant);

            foundMerchant.ifPresentOrElse(merchant -> {
                merchant.setOpen(openState);
                Merchant result = merchantRepository.save(merchant);

                response.putData("merchant", CustomMapper.toMerchantDTO(result));
                response.success(HttpStatus.OK.value());
                response.putMessageStatus("Success edit merchant open state");
            }, () -> {
                throw new ExtendedEntityNotFoundException(Merchant.class);
            });

            return response;
        } catch (ExtendedEntityNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalServerErrorException(e.getMessage());
        }
    }
}

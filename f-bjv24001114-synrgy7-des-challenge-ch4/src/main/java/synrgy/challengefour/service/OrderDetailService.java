package synrgy.challengefour.service;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import synrgy.challengefour.model.request.order.OrderCreateModel;
import synrgy.challengefour.model.request.order.OrderDetailCreateModel;
import synrgy.challengefour.model.standard.StandardResponseModel;

@Component
public interface OrderDetailService<T> {

    StandardResponseModel save(OrderDetailCreateModel request);

}
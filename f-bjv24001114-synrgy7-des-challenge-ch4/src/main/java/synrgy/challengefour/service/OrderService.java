package synrgy.challengefour.service;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import synrgy.challengefour.model.request.order.OrderCreateModel;
import synrgy.challengefour.model.standard.StandardResponseModel;

@Component
public interface OrderService<T> {

    StandardResponseModel list(Pageable pageable);
    StandardResponseModel save(OrderCreateModel request);

}
package synrgy.challengefour.service;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import synrgy.challengefour.model.entity.Product;
import synrgy.challengefour.model.request.product.ProductCreateModel;
import synrgy.challengefour.model.request.product.ProductEditRequestModel;
import synrgy.challengefour.model.standard.StandardResponseModel;

import java.util.UUID;

@Component
public interface ProductService<T> {

    StandardResponseModel list(String name, Pageable pageable);
    StandardResponseModel save(ProductCreateModel request);
    StandardResponseModel edit(ProductEditRequestModel request);
    StandardResponseModel delete(UUID idProduct);

    Product fetchProductById(UUID idProduct);

}

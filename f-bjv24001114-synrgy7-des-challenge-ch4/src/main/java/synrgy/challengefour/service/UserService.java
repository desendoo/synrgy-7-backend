package synrgy.challengefour.service;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import synrgy.challengefour.model.request.user.UserCreateModel;
import synrgy.challengefour.model.request.user.UserEditRequestModel;
import synrgy.challengefour.model.standard.StandardResponseModel;

import java.util.UUID;

@Component
public interface UserService<T> {

    StandardResponseModel list(Pageable pageable);
    StandardResponseModel save(UserCreateModel request);
    StandardResponseModel edit(UserEditRequestModel request);
    StandardResponseModel delete(UUID idUser);

}
package synrgy.challengefour.service;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import synrgy.challengefour.model.request.merchant.MerchantCreateModel;
import synrgy.challengefour.model.standard.StandardResponseModel;

import java.util.UUID;

@Component
public interface MerchantService<T> {

     StandardResponseModel list(Boolean open, Pageable pageable);
     StandardResponseModel save(MerchantCreateModel request);
     StandardResponseModel editOpen(UUID idMerchant, boolean openState);

}

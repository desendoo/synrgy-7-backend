package synrgy.challengefour.model.request.product;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.UUID;

@Data
public class ProductEditRequestModel {

    @NotBlank(message = "Name cannot be blank")
    @NotNull(message = "Name cannot be null")
    @NotEmpty(message = "Name cannot be empty")
    private String name;

    @NotBlank(message = "Price cannot be blank")
    @NotNull(message = "Price cannot be null")
    @NotEmpty(message = "Price cannot be empty")
    private Double price;

    @NotBlank(message = "Product cannot be blank")
    @NotNull(message = "Product cannot be null")
    @NotEmpty(message = "Product cannot be empty")
    private UUID idProduct;

}
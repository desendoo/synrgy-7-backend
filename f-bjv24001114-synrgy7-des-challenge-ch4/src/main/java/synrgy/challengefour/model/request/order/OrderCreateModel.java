package synrgy.challengefour.model.request.order;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.UUID;

@Data
public class OrderCreateModel {

    @NotBlank(message = "Destination address cannot be blank")
    @NotNull(message = "Destination address cannot be null")
    @NotEmpty(message = "Destination address cannot be empty")
    @Size(min = 3, max = 255, message = "Destination address must be between 3 and 255 characters")
    private String destinationAddress;

    @NotBlank(message = "User cannot be blank")
    @NotNull(message = "User cannot be null")
    @NotEmpty(message = "User cannot be empty")
    private UUID idUser;

}
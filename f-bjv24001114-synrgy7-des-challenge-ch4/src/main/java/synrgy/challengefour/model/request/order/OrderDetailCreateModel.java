package synrgy.challengefour.model.request.order;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.UUID;

@Data
public class OrderDetailCreateModel {

    @NotNull(message = "Quantity cannot be null")
    private Integer quantity;

    @NotNull(message = "Order cannot be null")
    private UUID idOrder;

    @NotNull(message = "Product cannot be null")
    private UUID idProduct;

}
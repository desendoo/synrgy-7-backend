package synrgy.challengefour.model.dto;

import lombok.Builder;
import lombok.Data;
import synrgy.challengefour.model.entity.User;

import java.util.Date;
import java.util.UUID;

@Data
@Builder
public class OrderDTO {

    private UUID id;
    private Date orderTime;
    private String destinationAddress;
    private Boolean completed;
    private UUID idUser;

}
package synrgy.challengefour.model.dto;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class ProductDTO {

    private UUID id;
    private String name;
    private Double price;
    private UUID idMerchant;

}
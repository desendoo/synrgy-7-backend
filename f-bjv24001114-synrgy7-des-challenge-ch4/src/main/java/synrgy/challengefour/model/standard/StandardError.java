package synrgy.challengefour.model.standard;

import java.util.ArrayList;
import java.util.Map;

public interface StandardError {

    public void putError(String key, Object value);
    public void appendErrorFromMap(Map<String, Object> error);
    public void appendErrors(ArrayList<Map<String, Object>> errors);

}

package synrgy.challengefour.model.standard;

import java.util.Map;

public interface StandardMessage {

    public void putMessage(String key, Object value);
    public void appendMessages(Map<String, Object> message);
    public void putMessageStatus(String messageStatus);

}

package synrgy.challengefour.model.standard;

import java.util.Map;

public interface StandardData {

    public void putData(String key, Object value);
    public void appendData(Map<String, Object> data);

}

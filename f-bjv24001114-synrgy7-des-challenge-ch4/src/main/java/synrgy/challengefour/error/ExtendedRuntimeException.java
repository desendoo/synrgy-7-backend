package synrgy.challengefour.error;

import synrgy.challengefour.helper.CustomUtils;
import synrgy.challengefour.model.standard.HttpMessage;

import java.time.ZonedDateTime;

public class ExtendedRuntimeException extends RuntimeException {

    private final ZonedDateTime timestamp = CustomUtils.getCurrentTimeAsiaJakarta();

    public ExtendedRuntimeException(HttpMessage ex) {
        super(ex.getExceptionMessage());
    }

    public ExtendedRuntimeException(HttpMessage ex, String customMessage) {
        super(ex.getExceptionMessage() + " Reason: " + customMessage);
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

}
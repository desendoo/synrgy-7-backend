package synrgy.challengefour.error.exception;

import lombok.Getter;
import lombok.Setter;
import synrgy.challengefour.error.ExtendedRuntimeException;
import synrgy.challengefour.model.response.FileUploadResponse;
import synrgy.challengefour.model.standard.HttpMessage;

@Setter
@Getter
public class FileStorageException extends ExtendedRuntimeException {

    FileUploadResponse fileUploadResponse;

    public FileStorageException() {
        super(HttpMessage.FILE_STORAGE_ERROR);
    }

    public FileStorageException(FileUploadResponse fileUploadResponse, String message) {
        super(HttpMessage.FILE_STORAGE_ERROR, message);
        this.fileUploadResponse = fileUploadResponse;
    }

    public FileStorageException(String message) {
        super(HttpMessage.FILE_STORAGE_ERROR, message);
    }
    public FileStorageException(Throwable cause) {
        super(HttpMessage.FILE_STORAGE_ERROR);
    }

    public FileStorageException(String message, Throwable cause) {
        super(HttpMessage.FILE_STORAGE_ERROR, message);
    }

}
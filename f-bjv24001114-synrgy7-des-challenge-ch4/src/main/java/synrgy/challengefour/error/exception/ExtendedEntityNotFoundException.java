package synrgy.challengefour.error.exception;

import jakarta.persistence.EntityNotFoundException;
import synrgy.challengefour.error.ExtendedRuntimeException;
import synrgy.challengefour.helper.CustomUtils;
import synrgy.challengefour.model.AbstractDate;
import synrgy.challengefour.model.standard.HttpMessage;

import java.time.ZonedDateTime;

public class ExtendedEntityNotFoundException extends EntityNotFoundException {

    private final ZonedDateTime timestamp = CustomUtils.getCurrentTimeAsiaJakarta();

    public ExtendedEntityNotFoundException(HttpMessage ex) {
        super(ex.getExceptionMessage());
    }

    public ExtendedEntityNotFoundException(HttpMessage ex, String customMessage) {
        super(ex.getExceptionMessage() + " Reason: " + customMessage);
    }

    public ExtendedEntityNotFoundException() {
        super(HttpMessage.ENTITY_IS_NOT_FOUND.getExceptionMessage());
    }

    public ExtendedEntityNotFoundException(String message) {
        super(HttpMessage.ENTITY_IS_NOT_FOUND.getExceptionMessage()+ " Reason: " + message);
    }

    public ExtendedEntityNotFoundException(Class<? extends AbstractDate> entity) {
        this(HttpMessage.ENTITY_IS_NOT_FOUND, String.format("[%1$s]", entity.getSimpleName()));
    }

    public ExtendedEntityNotFoundException(Class<? extends AbstractDate> entity, String message) {
        this(HttpMessage.ENTITY_IS_NOT_FOUND, String.format("[%1$s]%2$s", entity.getSimpleName(), message));
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

}

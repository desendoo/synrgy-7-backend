package synrgy.challengefour.error.exception;

import synrgy.challengefour.error.ExtendedRuntimeException;
import synrgy.challengefour.model.standard.HttpMessage;

public class InternalServerErrorException extends ExtendedRuntimeException {

    public InternalServerErrorException() {
        super(HttpMessage.INTERNAL_SERVER_ERROR);
    }

    public InternalServerErrorException(String message) {
        super(HttpMessage.INTERNAL_SERVER_ERROR, message);
    }

}
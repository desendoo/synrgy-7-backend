package synrgy.challengefour.error.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import synrgy.challengefour.error.exception.InternalServerErrorException;
import synrgy.challengefour.model.standard.StandardResponseModel;

@RestControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<StandardResponseModel> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        StandardResponseModel response = new StandardResponseModel();
        e.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();

            response.putError(fieldName, errorMessage);
        });
        logger.error(e.getMessage(), e);

        return ResponseEntity
                .status(response.getStatusCode())
                .body(response);
    }

    @ExceptionHandler(InternalServerErrorException.class)
    public ResponseEntity<StandardResponseModel> handleInternalServerErrorException(InternalServerErrorException e) {
        StandardResponseModel response = new StandardResponseModel();
        response.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), e);
        logger.error(e.getMessage(), e);

        return ResponseEntity
                .status(response.getStatusCode())
                .body(response);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<StandardResponseModel> handleException(Exception e) {
        StandardResponseModel response = new StandardResponseModel();
        response.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), e);
        logger.error(e.getMessage(), e);

        return ResponseEntity
                .status(response.getStatusCode())
                .body(response);
    }


}

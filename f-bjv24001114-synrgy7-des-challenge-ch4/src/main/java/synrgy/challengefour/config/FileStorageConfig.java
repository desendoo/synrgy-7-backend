package synrgy.challengefour.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "file")
public class FileStorageConfig {

    @Value("${file.upload-dir}")
    private String pathUpload;

    public String getUploadDir() {
        return pathUpload;
    }

    public void setUploadDir(String pathUpload) {
        this.pathUpload = pathUpload;
    }

}
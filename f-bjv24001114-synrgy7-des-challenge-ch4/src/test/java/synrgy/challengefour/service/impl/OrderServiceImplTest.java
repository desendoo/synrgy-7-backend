package synrgy.challengefour.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import synrgy.challengefour.model.entity.*;
import synrgy.challengefour.model.request.order.OrderDetailCreateModel;
import synrgy.challengefour.model.standard.StandardResponseModel;
import synrgy.challengefour.repository.OrderDetailRepository;
import synrgy.challengefour.repository.OrderRepository;
import synrgy.challengefour.repository.UserRepository;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class OrderServiceImplTest {

    @Mock
    private OrderRepository orderRepository;
    @Mock
    private UserRepository userRepository;
    @Mock
    private OrderDetailRepository orderDetailRepository;

    @Mock
    private ProductServiceImpl productService;

    @InjectMocks
    private OrderServiceImpl<Order> orderService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("OrderDetail created successfully")
    void orderDetailCreatedSuccessfully() {
        User user = UserTest.setupEntity();
        Order order = OrderTest.setupEntity(user);
        Merchant merchant = MerchantTest.setupEntity();
        Product product = ProductTest.setupEntity(merchant);
        OrderDetail orderDetail = OrderDetailTest.setupEntity(order, product);

        OrderDetailCreateModel orderDetailCreateModel = new OrderDetailCreateModel();
        orderDetailCreateModel.setQuantity(orderDetail.getQuantity());
        orderDetailCreateModel.setIdOrder(order.getId());
        orderDetailCreateModel.setIdProduct(product.getId());

        when(orderRepository.existsById(orderDetailCreateModel.getIdOrder())).thenReturn(true);
        when(productService.fetchProductById(orderDetailCreateModel.getIdProduct())).thenReturn(product);
        when(orderRepository.getReferenceById(orderDetailCreateModel.getIdOrder())).thenReturn(order);
        when(orderDetailRepository.save(any(OrderDetail.class))).thenReturn(orderDetail);
        StandardResponseModel response = orderService.save(orderDetailCreateModel);

        assertNotNull(response);
        assertEquals(HttpStatus.CREATED.value(), response.getStatusCode());
        assertNotNull(response.getData());
        assertNotNull(response.getData().get("orderDetail"));
    }

}
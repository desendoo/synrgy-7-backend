package synrgy.challengefour.service.impl;

import jakarta.persistence.criteria.Predicate;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import synrgy.challengefour.error.exception.InternalServerErrorException;
import synrgy.challengefour.model.entity.Merchant;
import synrgy.challengefour.model.entity.MerchantTest;
import synrgy.challengefour.model.entity.Product;
import synrgy.challengefour.model.entity.ProductTest;
import synrgy.challengefour.model.request.product.ProductCreateModel;
import synrgy.challengefour.model.request.product.ProductEditRequestModel;
import synrgy.challengefour.model.standard.StandardResponseModel;
import synrgy.challengefour.repository.MerchantRepository;
import synrgy.challengefour.repository.ProductRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

class ProductServiceImplTest {

    @Mock
    private MerchantRepository merchantRepository;
    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductServiceImpl productService;

    private Specification<Product> specification;
    private Pageable pageable;
    private Merchant merchant;
    private Product product;

    private Specification<Product> buildSpecification(String name) {
        return ((root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + name.toLowerCase() + "%"));

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        });
    }

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        specification = buildSpecification("Super Product");
        pageable = PageRequest.of(0, 5);
        merchant = MerchantTest.setupEntity();
        product = ProductTest.setupEntity(merchant);
    }

    @Test
    @DisplayName("List products successfully")
    void listProductsSuccessfully() {
        List<Product> productsList = Arrays.asList(
                Product.builder().name("Super Product").build(),
                new Product());
        Page<Product> products = new PageImpl<>(productsList, pageable, productsList.size());

        when(productRepository.findAll(any(Specification.class), eq(pageable))).thenReturn(products);

        StandardResponseModel response = productService.list("Super Product", pageable);

        assertNotNull(response);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
        assertNotNull(response.getData());
        assertNotNull(response.getData().get("products"));
        assertNotNull(response.getData().get("pageable"));
    }

    @Test
    @DisplayName("List products throws InternalServerErrorException")
    void listProductsThrowsInternalServerErrorException() {
        when(productRepository.findAll()).thenThrow(new RuntimeException());

        assertThrows(InternalServerErrorException.class, () -> productService.list("Super Product", pageable));
    }

    @Test
    @DisplayName("Save product successfully")
    void saveProductSuccessfully() {
        ProductCreateModel request = new ProductCreateModel();
        request.setIdMerchant(merchant.getId());
        request.setName(product.getName());
        request.setPrice(product.getPrice());

        when(merchantRepository.existsById(merchant.getId())).thenReturn(true);
        when(merchantRepository.getReferenceById(merchant.getId())).thenReturn(merchant);
        when(productRepository.save(any(Product.class))).thenReturn(product);
        StandardResponseModel response = productService.save(request);

        assertNotNull(response);
        assertEquals(HttpStatus.CREATED.value(), response.getStatusCode());
        assertNotNull(response.getData());
        assertNotNull(response.getData().get("product"));
    }

    @Test
    @DisplayName("Save product throws InternalServerErrorException")
    void saveProductThrowsInternalServerErrorException() {
        ProductCreateModel request = new ProductCreateModel();
        request.setIdMerchant(merchant.getId());
        request.setName(product.getName());
        request.setPrice(product.getPrice());

        when(merchantRepository.existsById(merchant.getId())).thenReturn(true);
        when(merchantRepository.getReferenceById(merchant.getId())).thenReturn(merchant);
        when(productRepository.save(any(Product.class))).thenThrow(new RuntimeException());

        assertThrows(InternalServerErrorException.class, () -> productService.save(request));
    }

    @Test
    @DisplayName("Edit product name successfully")
    void editProductSuccessfully() {
        ProductEditRequestModel request = new ProductEditRequestModel();
        request.setIdProduct(product.getId());
        request.setName("Super Product 2");
        request.setPrice(20000.0);

        when(productRepository.findById(product.getId())).thenReturn(java.util.Optional.of(product));
        when(productRepository.save(any(Product.class))).thenReturn(product);
        StandardResponseModel response = productService.edit(request);
        JSONObject responseJson = new JSONObject(response);

        assertNotNull(response);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
        assertNotNull(response.getData());
        assertEquals(request.getName(), responseJson.getJSONObject("data").getJSONObject("product").getString("name"));
    }

}
package synrgy.challengefour.service.impl;

import jakarta.persistence.criteria.Predicate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import synrgy.challengefour.error.exception.ExtendedEntityNotFoundException;
import synrgy.challengefour.error.exception.InternalServerErrorException;
import synrgy.challengefour.model.entity.Merchant;
import synrgy.challengefour.model.request.merchant.MerchantCreateModel;
import synrgy.challengefour.model.standard.StandardResponseModel;
import synrgy.challengefour.repository.MerchantRepository;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class MerchantServiceImplTest {

    @Mock
    private MerchantRepository merchantRepository;

    @InjectMocks
    private MerchantServiceImpl merchantService;

    private Specification<Merchant> specification;
    private Pageable pageable;

    private Specification<Merchant> buildSpecification(boolean open) {
        return ((root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            predicates.add(criteriaBuilder.equal(root.get("open"), open));

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        });
    }

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        specification = buildSpecification(true);
        pageable = PageRequest.of(0, 5);
    }

    @Test
    @DisplayName("List merchants successfully")
    void listMerchantsSuccessfully() {
        List<Merchant> merchantsList = Arrays.asList(new Merchant(), new Merchant());
        Page<Merchant> merchants = new PageImpl<>(merchantsList, pageable, merchantsList.size());
        when(merchantRepository.findAll(any(Specification.class), eq(pageable))).thenReturn(merchants);

        StandardResponseModel response = merchantService.list(null, pageable);

        assertNotNull(response);
        assertEquals(200, response.getStatusCode());
    }

    @Test
    @DisplayName("List merchants throws InternalServerErrorException")
    void listMerchantsThrowsInternalServerErrorException() {
        when(merchantRepository.findAll()).thenThrow(new RuntimeException());

        assertThrows(InternalServerErrorException.class, () -> merchantService.list(null, pageable));
    }

    @Test
    @DisplayName("Save merchant successfully")
    void saveMerchantSuccessfully() {
        Merchant merchant = new Merchant();
        MerchantCreateModel request = new MerchantCreateModel();

        when(merchantRepository.save(any(Merchant.class))).thenReturn(merchant);
        StandardResponseModel response = merchantService.save(request);

        assertNotNull(response);
        assertEquals(201, response.getStatusCode());
        assertNotNull(response.getData());
        assertNotNull(response.getData().get("merchant"));
    }

    @Test
    @DisplayName("Save merchant throws InternalServerErrorException")
    void saveMerchantThrowsInternalServerErrorException() {
        Merchant merchant = new Merchant();
        MerchantCreateModel request = new MerchantCreateModel();

        when(merchantRepository.save(any(Merchant.class))).thenThrow(new RuntimeException());

        assertThrows(InternalServerErrorException.class, () -> merchantService.save(request));
    }

    @Test
    @DisplayName("Edit merchant open state successfully")
    void editMerchantOpenStateSuccessfully() {
        UUID uuid = UUID.randomUUID();
        Merchant merchant = new Merchant();
        merchant.setId(uuid);

        when(merchantRepository.findById(uuid)).thenReturn(Optional.of(merchant));
        when(merchantRepository.save(any(Merchant.class))).thenReturn(merchant);

        StandardResponseModel response = merchantService.editOpen(uuid, true);

        assertNotNull(response);
        assertEquals(200, response.getStatusCode());
    }

    @Test
    @DisplayName("Edit merchant open state throws EntityNotFoundException")
    void editMerchantOpenStateThrowsEntityNotFoundException() {
        UUID uuid = UUID.randomUUID();

        when(merchantRepository.findById(uuid)).thenReturn(Optional.empty());

        assertThrows(ExtendedEntityNotFoundException.class, () -> merchantService.editOpen(uuid, true));
    }

    @Test
    @DisplayName("Edit merchant open state throws InternalServerErrorException")
    void editMerchantOpenStateThrowsInternalServerErrorException() {
        UUID uuid = UUID.randomUUID();

        when(merchantRepository.findById(uuid)).thenReturn(Optional.of(new Merchant()));
        when(merchantRepository.save(any(Merchant.class))).thenThrow(new RuntimeException());

        assertThrows(InternalServerErrorException.class, () -> merchantService.editOpen(uuid, true));
    }
}
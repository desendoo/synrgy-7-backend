package synrgy.challengefour.model.entity;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import synrgy.challengefour.model.entity.Product;
import synrgy.challengefour.repository.ProductRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestPropertySource("/application-test.properties")
public class ProductTest {

    @Autowired
    private ProductRepository productRepository;

    private static Merchant merchant;
    private Product sut;

    @BeforeAll
    static void init() {
        merchant = MerchantTest.setupEntity();
    }

    @BeforeEach
    void setUp() {
        resetRepository();
        merchant = MerchantTest.setupEntity();
        sut = setupEntity(merchant);
    }

    private void resetRepository() {
        productRepository.deleteAll();
    }

    public static Product setupEntity(Merchant merchant) {
        Product product = new Product();
        product.setId(UUID.randomUUID());
        product.setName("Super Product");
        product.setPrice(10000.0);
        product.setIdMerchant(merchant);

        return product;
    }

    @Test
    @DisplayName("Save product successfully")
    void saveProductSuccessfully() {
        Product result = productRepository.save(sut);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(sut.getName(), result.getName());
        assertEquals(sut.getPrice(), result.getPrice());
        assertEquals(sut.getIdMerchant(), result.getIdMerchant());
    }

    @Test
    @DisplayName("List product successfully")
    void listProductSuccessfully() {
        Product result = productRepository.save(sut);
        List<Product> products = productRepository.findAll();

        assertNotNull(products);
        assertEquals(1, products.size());
    }

    @Test
    @DisplayName("Edit product successfully")
    void editProductSuccessfully() {
        Product result = productRepository.save(sut);
        result.setName("Super Product 2");
        result.setPrice(20000.0);
        result.setIdMerchant(merchant);

        Product updated = productRepository.save(result);

        assertNotNull(updated);
        assertEquals(result.getName(), updated.getName());
        assertEquals(result.getPrice(), updated.getPrice());
        assertEquals(result.getIdMerchant().getId(), updated.getIdMerchant().getId());
    }

    @Test
    @DisplayName("Delete product successfully")
    void deleteProductSuccessfully() {
        Product result = productRepository.save(sut);
        productRepository.delete(result);

        List<Product> products = productRepository.findAll();

        assertNotNull(products);
        assertEquals(0, products.size());
    }

}
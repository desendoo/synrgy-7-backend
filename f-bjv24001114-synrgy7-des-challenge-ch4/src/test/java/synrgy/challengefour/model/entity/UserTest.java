package synrgy.challengefour.model.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import synrgy.challengefour.repository.UserRepository;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestPropertySource("/application-test.properties")
public class UserTest {

    @Autowired
    private UserRepository userRepository;

    private static User sut;

    @BeforeEach
    void  setUp() {
        resetRepository();
        sut = setupEntity();
    }

    private void resetRepository() {
        userRepository.deleteAll();
    }

    public static User setupEntity() {
        User merchant = User.builder()
                .username("example")
                .emailAddress("example@mail.com")
                .password("password")
                .build();

        return merchant;
    }

    @Test
    @DisplayName("Save user successfully")
    void saveUserSuccessfully() {
        User result = userRepository.save(sut);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(sut.getUsername(), result.getUsername());
        assertEquals(sut.getEmailAddress(), result.getEmailAddress());
        assertEquals(sut.getPassword(), result.getPassword());
    }

    @Test
    @DisplayName("List user successfully")
    void listUserSuccessfully() {
        userRepository.save(sut);
        userRepository.save(setupEntity());
        userRepository.save(setupEntity());
        userRepository.save(setupEntity());

        List<User> users = userRepository.findAll();

        assertFalse(users.isEmpty());
        assertEquals(4, users.size());
    }

    @Test
    @DisplayName("Delete user successfully")
    void deleteUserSuccessfully() {
        User result = userRepository.save(sut);
        assertTrue(userRepository.findById(result.getId()).isPresent());

        userRepository.delete(result);

        assertTrue(userRepository.findAll().isEmpty());
    }

}
package synrgy.challengefour.model.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.test.context.TestPropertySource;
import synrgy.challengefour.repository.MerchantRepository;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestPropertySource("/application-test.properties")
public class MerchantTest {

    @Autowired
    private MerchantRepository merchantRepository;

    private static Merchant sut;

    @BeforeEach
    void  setUp() {
        resetRepository();
       sut = setupEntity();
    }

    private void resetRepository() {
        merchantRepository.deleteAll();
    }

    public static Merchant setupEntity() {
        Merchant merchant = new Merchant();
        merchant.setId(UUID.randomUUID());
        merchant.setName("Merchant");
        merchant.setLocation("Yogyakarta");
        merchant.setOpen(true);

        return merchant;
    }

    @Test
    @DisplayName("Save merchant successfully")
    void saveMerchantSuccessfully() {
        Merchant result = merchantRepository.save(sut);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(sut.getName(), result.getName());
        assertEquals(sut.getLocation(), result.getLocation());
        assertEquals(sut.isOpen(), result.isOpen());
    }

    @Test
    @DisplayName("Edit merchant successfully")
    void editMerchantOpenSuccessfully() {
        Merchant result = merchantRepository.save(sut);
        assertTrue(result.isOpen());

        result.setOpen(false);
        Merchant updated = merchantRepository.save(result);

        assertFalse(updated.isOpen());
    }

    @Test
    @DisplayName("List merchants successfully")
    void listMerchantsSuccessfully() {
        merchantRepository.save(sut);
        merchantRepository.save(setupEntity());
        merchantRepository.save(setupEntity());
        merchantRepository.save(setupEntity());

        List<Merchant> merchants = merchantRepository.findAll();

        assertFalse(merchants.isEmpty());
        assertEquals(4, merchants.size());
    }

    @Test
    @DisplayName("Edit merchant open state successfully")
    void editOpenMerchantSuccessfully() {
        Merchant result = merchantRepository.save(sut);
        assertTrue(result.isOpen());

        result.setOpen(false);
        Merchant updated = merchantRepository.save(result);

        assertFalse(updated.isOpen());
    }

}
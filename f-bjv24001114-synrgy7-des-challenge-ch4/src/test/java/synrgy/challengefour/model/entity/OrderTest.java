package synrgy.challengefour.model.entity;

import java.util.Date;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class OrderTest {

    public static Order setupEntity(User user) {
        Order order = new Order();
        order.setId(UUID.randomUUID());
        order.setOrderTime(new Date());
        order.setDestinationAddress("Example Destination Address");
        order.setCompleted(true);
        order.setIdUser(user);

        return order;
    }

}
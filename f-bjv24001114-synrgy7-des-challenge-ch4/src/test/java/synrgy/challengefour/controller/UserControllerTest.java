package synrgy.challengefour.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestTemplate;
import synrgy.challengefour.model.standard.StandardResponseModel;

import static org.junit.jupiter.api.Assertions.*;

public class UserControllerTest {

    private RestTemplate restTemplate = new RestTemplate();

    @Test
    public void testObjectMapping() throws JsonProcessingException {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");

        String url = "https://api.restful-api.dev/objects";
        // String url = "https://api.restful-api.dev/objects?id=2";
        // Call with string query parameter
        Object restResponse = restTemplate.exchange(
                url,
                HttpMethod.GET,
                new HttpEntity<>(headers),
                Object.class).getBody();

        System.out.println(restResponse.toString());
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(restResponse);
        JSONArray jsonArray = new JSONArray(json);

        System.out.println(jsonArray
                        .getJSONObject(0)
                        .get("name"));
        System.out.println(jsonArray
                .getJSONObject(0)
                .getJSONObject("data")
                .get("capacity"));

        StandardResponseModel response = new StandardResponseModel();
        response.setStatusCode(HttpStatus.OK.value());
        response.putData("data", jsonArray.toList());

        System.out.println(response.getData());
    }

}
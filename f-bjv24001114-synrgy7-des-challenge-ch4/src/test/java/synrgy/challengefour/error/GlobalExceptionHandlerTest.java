package synrgy.challengefour.error;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import synrgy.challengefour.error.exception.InternalServerErrorException;
import synrgy.challengefour.error.handler.GlobalExceptionHandler;
import synrgy.challengefour.model.standard.StandardResponseModel;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class GlobalExceptionHandlerTest {

    @Mock
    private GlobalExceptionHandler globalExceptionHandler;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Handle generic exception")
    void handleGenericException() {
        Exception exception = new Exception("Test exception");
        when(globalExceptionHandler.handleException(exception)).thenCallRealMethod();

        ResponseEntity<StandardResponseModel> response = globalExceptionHandler.handleException(exception);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals("Test exception", response.getBody().getErrors().stream().filter(
                e -> e.containsValue("Test exception")).findFirst().get().get(exception.getClass().getSimpleName()));
        assertTrue(response.getBody().getErrors().stream().filter(
                e -> e.containsKey(exception.getClass().getSimpleName())).findFirst().get().get(exception.getClass().getSimpleName()).toString().contains("Test exception"));
    }

    @Test
    @DisplayName("Handle InternalServerErrorException")
    void handleInternalServerErrorException() {
        InternalServerErrorException exception = new InternalServerErrorException("Test exception");
        when(globalExceptionHandler.handleInternalServerErrorException(exception)).thenCallRealMethod();

        ResponseEntity<StandardResponseModel> response = globalExceptionHandler.handleInternalServerErrorException(exception);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertTrue(response.getBody().getErrors().stream().filter(
                e -> e.containsKey(exception.getClass().getSimpleName())).findFirst().get().get(exception.getClass().getSimpleName()).toString().contains("Test exception"));
    }

}
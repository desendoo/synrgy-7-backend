package test;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Scanner;

public class PemesananMakanan {

    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) throws Exception {
        /*
         * Pemesanan Makanan
         */
        String[] DAFTAR_MENU = {
            "Ayam Goreng",
            "Nasi Goreng",
            "Nasi Goreng Spesial",
            "Bakmi Goreng",
            "Bakmi Goreng Spesial"
        };
        int[] DAFTAR_HARGA = {
            15000,
            18000,
            22000,
            20000,
            24000
        };
        int jumlahPesananMaksimum = 100;
        int jumlahPesananSekarang = 0;
        // Array 2 dimensi untuk menyimpan pilihan menu dan kuantitas
        int[][] menuPilihanDanKuantitas = new int[jumlahPesananMaksimum][2];
        boolean isMasihMemilihMenu = true;

        /*
         * Menu Utama
         */
        System.out.println("=============================================");
        System.out.println("=  Selamat datang di Rumah Makan Sederhana  =");
        System.out.println("=============================================\n");
        // Cetak menu
        cetakMenu(DAFTAR_MENU, DAFTAR_HARGA);
        do {
            // Pilih menu
            menuPilihanDanKuantitas = pilihMenu(DAFTAR_MENU, DAFTAR_HARGA, menuPilihanDanKuantitas, jumlahPesananSekarang);
            jumlahPesananSekarang++;
            System.out.println("Apakah masih ingin memilih menu? [y=ya (default) / n=bayar]");
            System.out.print(" => ");
            String jawaban = input.next();

            if (jawaban.equals("n")) {
                isMasihMemilihMenu = false;
                // Cetak nota
                cetakNota(DAFTAR_MENU, DAFTAR_HARGA, menuPilihanDanKuantitas, jumlahPesananSekarang);
            }
        } while(isMasihMemilihMenu);

        input.close();
    }

    /*
     * Cetak Nota
     */
    public static void cetakNota(String[] DAFTAR_MENU, int[] DAFTAR_HARGA, int[][] menuPilihanDanKuantitas, int jumlahPesananSekarang) throws Exception {
        int totalHarga = 0;

        // Redirect output to file
        PrintStream originalOut = System.out;
        String namaFile = "nota.txt";
        FileOutputStream fileOut = new FileOutputStream(namaFile);
        PrintStream fileStream = new PrintStream(fileOut);
        System.setOut(fileStream);

        System.out.println("\n=============================================");
        System.out.println("=              Nota Pembayaran              =");
        System.out.println("=============================================");
        System.out.printf("%-25s %-12s %-10s\n", "   Menu", "Kuantitas", "Harga");
        System.out.println("---------------------------------------------");

        for (int i = 0; i < jumlahPesananSekarang; i++) {
            totalHarga += DAFTAR_HARGA[menuPilihanDanKuantitas[i][0]] * menuPilihanDanKuantitas[i][1];
            System.out.printf("%-30s %-5s %-10s\n",
                i+1 + ") " + DAFTAR_MENU[menuPilihanDanKuantitas[i][0]],
                " x " + menuPilihanDanKuantitas[i][1],
                " = " + DAFTAR_HARGA[menuPilihanDanKuantitas[i][0]] * menuPilihanDanKuantitas[i][1]);
        }
        System.out.println("---------------------------------------------");
        System.out.printf("%-25s %-12s %-10s\n", "", "Total", totalHarga);

        // Close the file stream
        fileStream.close();
        // Reset output stream
        System.setOut(originalOut);

        /*
         * Baca file nota dan cetak kembali ke console
         */
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(namaFile)));

        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }

        reader.close();

        System.out.println("\nNota pembayaran telah dicetak di file: " + namaFile);
    }

    /*
     * Pilih Menu
     */
    public static int[][] pilihMenu(String[] DAFTAR_MENU, int[] DAFTAR_HARGA, int[][] menuPilihanDanKuantitas, int jumlahPesananSekarang) {
        int menuPilihan;
        int menuKuantitas;
        boolean isInputValid = false;

        // Validasi input
        do {
            System.out.println("Silakan pilih menu: ");
            System.out.print(" => ");
            menuPilihan = input.nextInt();
            if (menuPilihan < 0 || menuPilihan > DAFTAR_MENU.length) {
                System.err.println("[WARNING] Input tidak valid, silakan pilih menu yang benar");
                continue;
            }
            isInputValid = true;
        } while (!isInputValid);
        
        // Validasi input
        isInputValid = false;
        do {
            System.out.println("Silakan tentukan kuantitas/jumlah pesanan: ");
            System.out.print(" => ");
            menuKuantitas = input.nextInt();
            if (menuKuantitas < 0) {
                System.err.println("[WARNING] Input tidak boleh minus, kuantitas harus bernilai positif");
                continue;
            }
            
            isInputValid = true;
        } while (!isInputValid);

        // Simpan pilihan menu
        menuPilihanDanKuantitas[jumlahPesananSekarang][0] = menuPilihan-1;
        // Simpan kuantitas
        menuPilihanDanKuantitas[jumlahPesananSekarang][1] = menuKuantitas;

        return menuPilihanDanKuantitas;
    }

    /*
     * Cetak Menu
     */
    public static void cetakMenu(String[] DAFTAR_MENU, int[] DAFTAR_HARGA) {
        System.out.println("Daftar Menu:");
        System.out.println("---------------------------------------------");
        System.out.printf("%-30s %-10s\n", "   Menu", "Harga");
        System.out.println("---------------------------------------------");
        for (int i = 0; i < DAFTAR_HARGA.length; i++) {
            System.out.printf("%-30s %-10s\n",
                (i + 1) + ". " + DAFTAR_MENU[i],
                DAFTAR_HARGA[i]);
        }
        System.out.println("---------------------------------------------");
    }

}

/*
Contoh output
 */
/*
=============================================
=  Selamat datang di Rumah Makan Sederhana  =
=============================================

Daftar Menu:
---------------------------------------------
   Menu                        Harga
---------------------------------------------
1. Ayam Goreng                 15000
2. Nasi Goreng                 18000
3. Nasi Goreng Spesial         22000
4. Bakmi Goreng                20000
5. Bakmi Goreng Spesial        24000
---------------------------------------------
Silakan pilih menu:
 => 1
Silakan tentukan kuantitas/jumlah pesanan:
 => 2
Apakah masih ingin memilih menu? [y=ya (default) / n=bayar]
 => y
Silakan pilih menu:
 => 3
Silakan tentukan kuantitas/jumlah pesanan:
 => 2
Apakah masih ingin memilih menu? [y=ya (default) / n=bayar]
 => y
Silakan pilih menu:
 => 5
Silakan tentukan kuantitas/jumlah pesanan:
 => 1
Apakah masih ingin memilih menu? [y=ya (default) / n=bayar]
 => n

=============================================
=              Nota Pembayaran              =
=============================================
   Menu                   Kuantitas    Harga
---------------------------------------------
1) Ayam Goreng                  x 2   = 30000
2) Nasi Goreng Spesial          x 2   = 44000
3) Bakmi Goreng Spesial         x 1   = 24000
---------------------------------------------
                          Total        98000

Nota pembayaran telah dicetak di file: nota.txt
 */